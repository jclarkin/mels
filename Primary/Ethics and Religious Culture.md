# Progression of Learning

# in Elementary School

# Ethics and Religious Culture

## August 20, 2010

## (Update: November 24, 2010)


## Ethics and Religious Culture

## Introduction

This document is complementary to the program and is designed to help teachers plan their teaching. It provides additional
information on the knowledge that students must acquire and use to support the development of the program's three
competencies and meet its objectives: the recognition of others and the pursuit of the common good. It is divided into three
sections. Each section is preceded by a short text that provides an overview of the learning to be acquired and focuses on
the main orientations of the program.

The approach selected for both the _ethics_ component and the _religious culture_ component starts from the immediate
realities of students. In Elementary Cycle One, we tackle situations related to family and school. In Cycle Two, we move on
to the broader environment of children, i.e. group life and the communities to which people belong, and finally, in Cycle
Three, we deal with the realities that affect societies and the world.

The learning in elementary school targets basic elements essential to ethical reflection, understanding the phenomenon of
religion and the practice of dialogue. The students progressively construct their understanding of the program concepts by
dealing with increasingly complex content. In this way, over the years, students gradually acquire the concepts related to
the _ethics_ component of the program, such as value, norm, reference or ethical question, as they tackle ethical issues. As
for the _religious culture_ component, by exploring various forms of religious expression in their cultural and social
environment, such as celebrations, stories, places of worship and religious norms, students construct their understanding
of this concept.

It is important to remember that it is through the practice of dialogue that the Ethics and Religious Culture program aims to
form citizens who are able to reflect on ethical questions and understand the phenomenon of religion in a spirit of
openness.


## Ethics and Religious Culture

## Reflects on ethical questions

At the elementary level, students learn to reflect on ethical questions using simple and familiar situations. Students are
asked to reflect on subjects that involve, for example, the needs of living beings, the advantages and disadvantages of
group life and the demands of life in society. From one cycle to the next, they apply their knowledge and develop their
competency in reflecting on ethical questions.

The following tables further describe the knowledge to be acquired for each compulsory theme in the _ethics_ component.
Students acquire this knowledge in order to identify a situation from an ethical point of view, to examine a few references
that support and enrich their reflection and to evaluate various options or possible actions. This knowledge is dealt with in
learning and evaluation situations that incorporate the practice of dialogue while proposing increasingly complex contexts
during a cycle and from one cycle to the next.

```
Knowledge related to ethics themes
```
```
Student constructs knowledge with teacher guidance.
```
```
Student applies knowledge by the end of the school year.
```
```
Student reinvests knowledge.
```
```
Elementary
```
```
Cycle
One
```
```
Cycle
Two
```
```
Cycle
Three
```
### A. The needs of humans and other living beings^1123456

```
Names the elements that contribute to the unique character of each human being
(e.g. personal history, preferences, talents, physical characteristics)
```
#### 1.

2. Names the needs shared by plants, animals and human beings (e.g. need for food)

```
Names the needs that are not shared by plants, animals and human beings (e.g. human
being's need for clothing, in contrast to animals and plants)
```
#### 3.

```
Gives examples of actions illustrating that living beings need each other (e.g. the farmer
grows the vegetables I eat)
```
#### 4.

```
Gives examples of actions illustrating that the members of a family need each other (e.g.
children need parents to give them shelter; parents need their children to cooperate to
create a pleasant living environment)
```
#### 5.

### Demands associated with the interdependence of humans and other living

### beings^2

### B.

```
1 re 2 e 3 e 4 e 5 e 6 e
```
```
Names the responsibilities that different family members may assume (e.g. parents are
responsible for feeding their children; a child might be responsible for walking the dog)
```
#### 1.

```
Names the responsibilities that different people at school may assume (e.g. a student is
responsible for cleaning the board; the principal organizes parent-teacher meetings; the
secretary records absences)
```
#### 2.

```
Names the values that guide behaviour in families and at school (e.g. sharing, attention to
others, safety, respect)
```
#### 3.

```
Names the norms that guide behaviour in families and at school (e.g. bedtime, rules of
conduct in class)
```
#### 4.

```
Gives examples of actions that may foster the well-being of living beings (e.g. consoling a
friend, sharing toys, feeding a pet)
```
#### 5.

```
Gives examples of actions that can harm living beings (e.g. saying hurtful things, hitting an
animal, ripping a plant out of the soil)
```
#### 6.

```
Names people or groups who take action to protect living beings (e.g. police officers,
school crossing guards, Society for the Prevention of Cruelty to Animals)
```
#### 7.

### C. Interpersonal relationships in groups^31 re 2 e 3 e 4 e 5 e 6 e

```
Names the groups to which people belong^4 (e.g. family, class, friends, sports team, drama
group, choir)
```
#### 1.

```
Makes connections between belonging to a group or groups and the development of
personal identity (e.g. influence of a group of friends on musical tastes, influence of family
on recreational activities, belonging to a scout troop and the ability to survive in a forest)
```
#### 2.

```
Gives examples of situations where needs are met in a group (e.g. by celebrating my
birthday, my friends and my family meet my need to be loved and be recognized)
```
#### 3.

```
Gives examples of situations illustrating the advantages of group life (e.g. through the
cooperation and solidarity of all students in the school, the school yard cleanup was done
quickly and everyone had more time to play)
```
#### 4.


```
Gives examples of situations illustrating the disadvantages of group life (e.g. since many
students want to play on the play structure, they all have to be patient and wait their turn,
make compromises and learn to share the space)
```
#### 5.

```
Names various types of relationships that can exist among the members of a group (e.g.
friendship, leadership and conflictual relationships can exist among classmates)
```
#### 6.

```
Explains how interpersonal relationships can contribute to or detract from the personal
development of group members (e.g. when the captain of a team encourages the players,
the players feel appreciated and this reinforces their self-esteem; a player who feels
rejected by the members of his/her team loses self-confidence)
```
#### 7.

### D. Demands of belonging to a group^51 re 2 e 3 e 4 e 5 e 6 e

```
Explains how behaviours, attitudes or actions can foster group life (e.g. by adopting a
calm attitude in class, everyone contributes to creating a pleasant atmosphere conducive
to work, and the whole group benefits)
```
#### 1.

```
Explains how behaviours, attitudes or actions can detract from group life (e.g. insulting a
friend, or talking behind his/her back creates tension in the group and can cause conflicts)
```
#### 2.

```
Explains how values guide group life (e.g. the value of respect encourages students to
wait their turn to speak and allows everyone to express an opinion without being
interrupted)
```
#### 3.

```
Explains how norms guide group life (e.g. in karate classes, the teacher asks everyone to
change teammates on a regular basis to avoid creating subgroups)
```
#### 4.

```
Gives examples of situations in which values or norms can be questioned, modified or
improved to foster community life (e.g. although keeping a secret is usually a good idea,
sometimes, to help a friend, a secret should be revealed to an adult)
```
#### 5.

```
Names the roles and responsibilities that members of a group may assume (e.g. in a
drama group, all actors have to memorize their parts)
```
#### 6.

```
Names the conditions that foster the personal well-being of the members of a group (e.g.
sharing material in the classroom, establishing a conflict-management procedure)
```
#### 7.

### E. Individuals as members of society^61 re 2 e 3 e 4 e 5 e 6 e

```
Explains how members of a society influence each other (e.g. more and more people use
reusable grocery bags, which creates a ripple effect)
```
#### 1.

```
Gives examples of situations in which the influence of members of a society has an impact
on self-assertiveness (e.g. a student refuses or decides to wear an article of clothing
widely advertised in the media and worn by many friends)
```
#### 2.

```
Explains how differences among individuals can be a source of enrichment (e.g. writing to
a pen pal in another country can lead to enriching discoveries: music, recipes, values,
ways of life)
```
#### 3.

```
Explains how the differences among individuals can be a source of conflict (e.g. the
competitive spirit can create conflict with those who only value participation; when people
do not speak the same language, they may not always understand each other very well)
```
#### 4.

```
Gives examples of prejudices, generalizations or stereotypes that are present in society
(Prejudice: he is certainly the one who did it because he is the leader of his group;
Generalization: my neighbour likes hunting, therefore all men like this activity; Stereotype:
children who come from this neighbourhood are more intelligent than children from other
neighbourhoods)
```
#### 5.

```
Names the possible effects of prejudices, generalizations and stereotypes (e.g. there
could be discrimination, rejection, injustice, categorization)
```
#### 6.

### F. Demands of life in society^71 re 2 e 3 e 4 e 5 e 6 e

```
Indicates what distinguishes an acceptable action from an unacceptable action (e.g. does
an action result in prejudice, harm, discomfort or injustice?)
```
#### 1.

```
Explains how actions or attitudes can foster life in society (e.g. denouncing an individual
caught in the act of theft contributes to neighbourhood safety)
```
#### 2.

```
Explains how actions and attitudes can detract from life in society (e.g. vandalism gives
rise to costs that all citizens have to pay)
```
#### 3.

```
Names sources of tension or conflict in society (e.g. discrimination, different
interpretations of a rule, social inequalities)
```
#### 4.

```
Explains how actions and attitudes can reduce tensions or conflicts in society (e.g.
tensions between motorists and cyclists can be reduced if everyone abides by road safety
rules and signals their intentions)
```
#### 5.

```
Explains how values or norms guide life in society (e.g. the value "saving the environment"
encourages reduced consumption of drinking water; the classification system for video
games guides parents and children in their choices)
```
#### 6.

```
Names the possible connections between a right and a responsibility (e.g. the right to
borrow a book at the library and the responsibility for taking care of it; the right to use a
municipal park and the responsibility for behaving like a good citizen)
```
#### 7.


### G. Learning common to all themes at the elementary level 1 re 2 e 3 e 4 e 5 e 6 e

```
Names references that support and enrich ethical reflection (e.g. charters, laws,
regulations, spiritual leaders, media)
```
#### 1.

2. Reformulates ethical questions (e.g. what distinguishes a need from a want?)
3. Formulates ethical questions (e.g. what is a just society, what does "being equal" mean?)
1. Ethics and Religious Culture Program, Elementary Education, p. 335
2. Ethics and Religious Culture Program, Elementary Education, p. 336
3. Ethics and Religious Culture Program, Elementary Education, p. 337
4. This is a reminder of learning achieved in Elementary Cycle One, Geography, History and Citizenship Education.
5. Ethics and Religious Culture Program, Elementary Education, p. 338
6. Ethics and Religious Culture Program, Elementary Education, p. 339
7. Ethics and Religious Culture Program, Elementary Education, p. 340


## Ethics and Religious Culture

## Demonstrates an understanding of the phenomenon of religion

To develop the competency _Demonstrates an understanding of the phenomenon of religion,_ students must acquire
knowledge about the religious traditions that are present in Québec and about representations of the world and human
beings that define the meaning and value of human experience outside the realm of religious beliefs and affiliations.

The following tables further describe the knowledge to be acquired for each compulsory theme of the _religious culture_
component. The students work with this knowledge in learning and evaluation situations that incorporate the practice of
dialogue. They use it to explore various forms of religious expression, to make connections between forms of religious
expression and the social and cultural environment as well as to consider various ways of thinking, being and acting.

The knowledge indicated in the following tables must be covered in accordance with the compulsory nature of the religious
elements of the program content:

```
Christianity (Catholicism and Protestantism) is covered throughout each year of a cycle
Judaism and Native spirituality are covered on a number of occasions in each year of a cycle
Islam, Buddhism and Hinduism are covered on a number of occasions over the course of a cycle
other religions may be covered over the course of a cycle, depending on the reality and the needs of the class
cultural expressions and expressions derived from representations of the world and of human beings that reflect the
meaning and value of human experience outside of religious beliefs and affiliation are addressed during the cycle
```
```
Knowledge related to themes of religious culture
```
```
Student constructs knowledge with teacher guidance.
```
```
Student applies knowledge by the end of the school year.
```
```
Student reinvests knowledge.
```
```
Elementary
```
```
Cycle
One
```
```
Cycle
Two
```
```
Cycle
Three
```
### A. Family celebrations^1123456

1. Names celebrations (e.g. Easter, Hanukkah, Mother's Day, Diwali, birthdays)

```
Describes ways of experiencing family celebrations: how the celebration is held, special
decorations, special menu, guests, etc.
```
#### 2.

3. Names rituals associated with birth^2 (e.g. baptism, walking out ceremony, planting a tree)^
    Describes rituals associated with birth: locations for the rituals, gestures, words, people
    present, etc.

#### 4.

```
Associates celebrations with their tradition of origin (e.g. Christmas and Christianity,
Passover and Judaism, powwow and Native traditions, the secular celebration of Mother's
Day)
```
#### 5.

```
Associates rituals associated with birth with their tradition of origin (e.g. baptism and
Christianity; whispering the call to prayer and Islam)
```
#### 6.

### B. Stories that have touched people^31 re 2 e 3 e 4 e 5 e 6 e

```
Names forms of religious expression that are associated with various stories (e.g. the
nativity scene, the galette des rois and the story of Jesus' birth, Native masks and stories)
```
#### 1.

```
Describes forms of religious expression associated with various stories: main aspects,
form, colour, location where they take place, use, etc.
```
#### 2.

```
Associates stories with celebrations or key figures (e.g. the story of the Three Wise Men
and the celebration of the Epiphany, the Exodus of the Jews from Egypt and the
celebration of Succoth)
```
#### 3.

```
Names the important aspects of a story associated with a celebration or an key figure
(e.g. the building of Noah’s ark, the animals, the rain, the dove, the rainbow in the story of
Noah and the Flood)
```
#### 4.

```
Associates stories with their religious tradition (e.g. the Nativity story and Christianity; the
story of Glouskap and the story of Beaver who steals fire and Native spirituality; the birth
of Gautama and Buddhism)
```
#### 5.

### C. Religious practices in the community^41 re 2 e 3 e 4 e 5 e 6 e

```
Names daily, weekly, annual or occasional religious celebrations (e.g. daily prayer,
Sunday mass, Seder meal, marriage, funeral)
```
#### 1.


```
Describes places of worship, objects or symbols connected with one or more religious
practices (e.g. form, furnishings, decorations in the place of worship; form, colour, use of
objects; form, origin, meaning of the symbols)
```
#### 2.

```
Describes religious celebrations experienced in the community: reasons for the
celebration, gestures, words recited, objects used, etc.
```
#### 3.

```
Associates spiritual guides with one or more religious practices (e.g. the priest and the
celebration of the Eucharist; the pastor and the assembly of prayer; the rabbi and the
Sabbath prayers; the shaman and Native spirituality)
```
#### 4.

```
Associates words and writings with their religious tradition (e.g. the Bible and Christianity;
the Torah and Judaism; Jesus for Christianity; the Great Spirit and Native spirituality;
Allah for Islam)
```
#### 5.

```
Describes practices of prayer and meditation: places, words recited, gestures, songs,
objects used
```
#### 6.

```
Associates elements of a celebration experienced in a community with the appropriate
religious tradition (e.g. the chalice and Christianity; Book of the Word and Protestantism;
the tam-tam and Native spirituality; the three crows and Buddhism; the Vedas and
Hinduism)
```
#### 7.

### D. Forms of religious expression in the young person's environment^51 re 2 e 3 e 4 e 5 e 6 e

```
Names elements of religious heritage present in the environment (e.g. church bell,
cemetery, names of certain streets)
```
#### 1.

```
Describes forms of religious expression observed in the environment: what it is, what it
represents, its use, appearance, origin, etc.
```
#### 2.

```
Names community or cultural works influenced by religion (e.g. stained glass, paintings,
sculptures as works of art; Saint Vincent de Paul Society as community work; media
holiday food drive as a cultural event)
```
#### 3.

```
Describes community or cultural works influenced by religion: characteristics, mission,
impact in the community, etc.
```
#### 4.

```
Associates symbols and images with various representations of the world's origins (e.g.
representations of earthly paradise, the turtle representing Mother Earth)
```
#### 5.

```
Associates forms of religious expression with their religious tradition (e.g. the wayside
cross and Christianity; a tombstone bearing the Star of David and Judaism; the
peace-pipe and Native spirituality)
```
#### 6.

### E. Religions in society and the world^61 re 2 e 3 e 4 e 5 e 6 e

```
Identifies the geographic distribution of the main religious traditions in the world (e.g.
making a geographical map showing the distribution of religions; identifying mainly
Catholic, Protestant, Muslim countries)
```
#### 1.

```
Identifies the population distribution of different belief groups (e.g. in the world, there are
nearly 1.13 billion Catholics, 800 million Protestants, 14 million Jews, 1.57 billion Muslims)
```
#### 2.

```
Names the place of origin of the religions practised in Québec (e.g. the Mediterranean
Basin for Christianity, Judaism and Islam; the Americas for Native spirituality; Europe for
Protestantism; India for Buddhism and Hinduism)
```
#### 3.

```
Relates events to the origins of religious traditions present in Québec (e.g. the death and
resurrection of Christ; the Protestant Reformation; Moses receiving the Ten
Commandments; the departure of Muhammad for Medina)
```
#### 4.

```
Names forms of religious expression associated with the founders of religious traditions
present in Québec (e.g. the crucifix and Jesus; Passover and Moses)
```
#### 5.

```
Explains the meaning of forms of religious expression associated with the founders of
religious traditions present in Québec (e.g. for Christians, the crucifix recalls the
resurrection of Jesus and his mission to save human beings; for Buddhists, Vaisakha
recalls the birth and enlightenment of Buddha)
```
#### 6.

```
Describes various representations or concepts of time (e.g. the origin of the Julian
calendar: the number and names of the months, the beginning and duration of the year;
linear and cyclical concepts of time)
```
#### 7.

### F. Religious values and norms^71 re 2 e 3 e 4 e 5 e 6 e

```
Names the values and norms related to various religious traditions (e.g. the Golden Rule
for many religious traditions; the Ten Commandments for Christianity and Judaism;
respect for nature for Native spirituality; compassion for Buddhism)
```
#### 1.

```
Makes connections between religious values or norms and the behaviour or attitudes of
certain believers (e.g. in several religious traditions, the value of sharing encourages
believers to engage in charity; in Native spirituality, the value of respect for nature means
only killing animals for food)
```
#### 2.


```
Explains how the work of an exemplary individual is inspired by values and beliefs (e.g. Le
bon Dieu dans la Rue, founded by Father Emmett Johns, known as “Pops,” helps
homeless young people; this work is based on the values of charity and sharing, important
to the founder)
```
#### 3.

```
Describes dietary practices based on religious values or norms: food preparation, meal
times, authorized and forbidden food, etc.
```
#### 4.

```
Explains the meaning of certain dietary practices: origin, reasons and values underlying
these practices, meaning given to the practice, symbolism, etc.
```
#### 5.

```
Describes practices related to clothing based on religious values or norms: obligation or
prohibition concerning the form of clothing, its colour, use, etc.
```
#### 6.

```
Explains the meaning of certain practices related to clothing: origin, reasons and values
as the motivation for following these practices, meaning attached to the practice,
symbolism, etc.
```
#### 7.

1. Ethics and Religious Culture Program, Elementary Education, p. 342
2. "Ritual associated with birth" means any ritual, ceremony or celebration that marks the welcoming of a child into a family
    or a religion.
3. Ethics and Religious Culture Program, Elementary Education, p. 342
4. Ethics and Religious Culture Program, Elementary Education, p. 343
5. Ethics and Religious Culture Program, Elementary Education, p. 345
6. Ethics and Religious Culture Program, Elementary Education, p. 346
7. Ethics and Religious Culture Program, Elementary Education, p. 347


## Ethics and Religious Culture

## Engages in dialogue

To support the development of the competencies _Reflects on ethical questions_ and _Demonstrates an understanding of the
phenomenon of religion_ , students must learn to engage in dialogue. To do so, they acquire knowledge related to the forms
of dialogue, the conditions that foster it and the means to be used to develop and examine a point of view. The students
learn to use this knowledge in a gradual way. That is why learning continues over more than one cycle at the elementary
and secondary levels.

The following tables present the knowledge that will allow students to organize their thinking, interact effectively with
others and develop a point of view in relation to an ethical reflection or to understanding an aspect of the phenomenon of
religion.

By reflecting on an ethical question or seeking to understand a form of religious expression, students acquire and use
simultaneously several elements of knowledge related to the competency involving dialogue.

```
Knowledge related to the practice of dialogue
```
```
Student constructs knowledge with teacher guidance.
```
```
Student applies knowledge by the end of the school year.
```
```
Student reinvests knowledge.
```
```
Elementary
```
```
Cycle
One
```
```
Cycle
Two
```
```
Cycle
Three
```
### A. Forms of dialogue^1123456

1. Explains, in his/her own words, the meaning of:

```
a. conversation, discussion, narration, deliberation
```
```
b. interview
```
```
c. debate
```
2. Uses, in a situation involving dialogue:

```
a. conversation, discussion, narration, deliberation
```
```
b. interview
```
```
c. debate
```
### B. Conditions that foster dialogue^21 re 2 e 3 e 4 e 5 e 6 e

1. Respects conditions that foster dialogue

```
a. observes rules for engaging in dialogue
```
```
b. correctly expresses his/her ideas
```
```
c. respects the right of others to speak
```
```
d. attentively listens to what another person has to say in order to grasp the meaning
```
```
e. all other paths for fostering dialogue, ERC program, p. 349
```
2. Contributes to establishing conditions that foster dialogue

```
a. suggests rules for engaging in dialogue
```
```
b. proposes ways of alleviating tensions
```
```
introduces nuances to his/her comments and recognizes the nuances introduced by
others
```
```
c.
```
```
d. is open to different ways of thinking
```

```
e. all other paths for fostering dialogue, ERC program, p. 349
```
### C. Means for developing a point of view^31 re 2 e 3 e 4 e 5 e 6 e

```
Explains, in his/her own words, what description is (e.g. giving a description means giving
several characteristics of something)
```
#### 1.

```
Uses description to enumerate the characteristics of the subject discussed^4 (e.g. in Cycle
One, words, objects or gestures for a ritual associated with birth; in Cycle Two, various
parts of a place of worship and objects, furnishings and symbols; in Cycle Three, origin,
division of time, main markers of time for a calendar)^5
```
#### 2.

```
Explains, in his/her own words, what comparison is (e.g. drawing a comparison means
finding differences and similarities between two things)
```
#### 3.

```
Uses comparison to highlight similarities and differences among the elements of the
subject discussed (e.g. in Cycle One, names two differences between the needs of
animals and those of a human being; in Cycle Two, compares an advantage and a
disadvantage of playing alone and in a group; in Cycle Three, compares how children's
rights are observed in various areas of the world)
```
#### 4.

```
Explains, in his/her own words, what synthesis is (e.g. making a synthesis means
summarizing what has been discussed)
```
#### 5.

```
Uses synthesis to provide a coherent summary of the elements of the subject discussed
(e.g. in Cycle Two, summarizes his/her discoveries about different ways of celebrating
civil marriage or religious marriage; in Cycle Three, summarizes his/her understanding of
the founding events of a religious tradition)
```
#### 6.

```
Describes, in his/her own words, what an explanation is (e.g. explaining means providing
accurate information to account for how I understand something)
```
#### 7.

```
Uses explanation to help others to know or understand the meaning of the subject
discussed (e.g. in Cycle Two, explains the negative effects of intimidation by referring to
what a community police officer said; in Cycle Three, explains the connections between
rights and responsibilities by referring to charters, city regulations and the school code)
```
#### 8.

```
Explains, in his/her own words, what a justification is (e.g. making a justification means
giving several reasons to demonstrate a point of view)
```
#### 9.

```
Uses justification to present, in a logical way, a few reasons and ideas that support a point
of view (e.g. presents some reasons that motivate a believer to observe a dietary rule)
```
#### 10.

### D. Means for examining a point of view^61 re 2 e 3 e 4 e 5 e 6 e

1. Types of judgments

```
1.1. Explains, in his/her own words the meaning of:
```
```
a. a judgment of preference, a judgment of prescription
```
```
b. a judgment of reality
```
```
c. a judgment of value
```
```
1.2. Recognizes, in a situation involving dialogue:
```
```
a judgment of preference (e.g. winter is my favourite season; it's the most beautiful
season)
```
```
a.
```
```
a judgment of prescription (e.g. the regulation prohibits running around the
swimming pool)
```
```
b.
```
```
a judgment of reality (e.g. the Easter celebration takes place in spring, but not on a
set date)
```
```
c.
```
```
d. a judgment of value (e.g. money creates happiness)
```
```
1.3. Examines, in a situation involving dialogue:
```
```
a. a judgment of preference (e.g. can you explain why you prefer winter?)
```
```
b. a judgment of prescription (e.g. why does this regulation exist?)
```
```
a judgment of reality (e.g. can you explain how the date for the Easter celebration is
decided?)
```
```
c.
```
```
d. a judgment of value (e.g. why do some people say that money leads to happiness?)
```
2. Processes that may hinder dialogue


```
2.1. Explains, in his/her own words:
```
```
a. a hasty generalization, a personal attack
```
```
b. an appeal to the people
```
```
an appeal to the crowd (bandwagon), an appeal to prejudice, an appeal to
stereotype, an argument from authority
```
```
c.
```
```
2.2. Recognizes, in a situation involving dialogue:
```
```
a hasty generalization (e.g. when my cat is home alone, he eats more; therefore cats
who are bored eat more)
```
```
a.
```
```
b. a personal attack (e.g. you're too much of a baby to understand this)
```
```
c. an appeal to the people (e.g. all my friends can stay out until 9:00 p.m., so I can too)
```
```
an appeal to the crowd (e.g. my new shoes are certainly the best, because the
salesperson told me they are the top sellers)
```
```
d.
```
```
an appeal to prejudice (e.g. my grandmother won't know how to use this electronic
device because she is too old)
```
```
e.
```
```
f. an appeal to stereotype (e.g. boys shouldn't cry)
```
```
an argument from authority (e.g. we should buy this brand of equipment because our
Olympic champion promotes it)
```
```
g.
```
```
2.3. Examines, in a situation involving dialogue:
```
```
a hasty generalization (e.g. do all cats eat more when they are alone or when they
are bored?)
```
```
a.
```
```
a personal attack (e.g. do you think you are helping the discussion by calling
someone a baby?)
```
```
b.
```
```
an appeal to the people (e.g. how does the fact that all your friends can stay out until
9:00 p.m. mean that you can too?)
```
```
c.
```
```
an appeal to the crowd (e.g. is something true simply because a large number of
people say it is?)
```
```
d.
```
```
e. an appeal to prejudice (e.g. is age linked to a person's abilities?)
```
```
f. an appeal to stereotype (e.g. can you explain to me why boys should not cry?)
```
```
an argument from authority (e.g. what criteria should we use for making choices
when buying sports equipment?)
```
```
g.
```
1. Ethics and Religious Culture Program, Elementary School, p. 349
2. Ethics and Religious Culture Program, Elementary School, p. 349
3. Ethics and Religious Culture Program, Elementary School, p. 351
4. The subject being discussed refers to the object of dialogue, i.e. the ethical situation or the form of religious expression
    dealt with in class.
5. The examples provided in this section alternate between the two components of the program (ethics and religious
    culture). All the means used to develop a point of view can, however, be used for both components.
6. Ethics and Religious Culture Program, Elementary School, p. 352


