# Progression des apprentissages

# Français, langue seconde

# (Programme d'immersion)

## 25 novembre 2009


## Table des matières

- Présentation
- Stratégies
- Vocabulaire utilisé à l’oral et à l’écrit
- Conventions de communication orale ou écrite
- Connaissances liées au texte oral ou écrit
- Connaissances liées à la phrase


## Français, langue seconde

## (Programme d'immersion)

## Présentation

```
Le présent document constitue un complément au programme d’immersion^1 de français, langue seconde (FLS). Il précise
les connaissances que les élèves doivent acquérir et mobiliser à chacune des années du primaire. Il est constitué de cinq
grandes sections qui regroupent les connaissances nécessaires au développement des compétences en FLS; ces
connaissances s’appliquent tant à l’oral qu’à l’écrit. Chaque section contient un court texte qui présente une vision globale
des apprentissages ainsi qu’un tableau illustrant leur progression. Ce document a pour but de soutenir les enseignantes et
les enseignants dans la planification de leur enseignement.
```
À leur entrée au primaire, les élèves qui entreprennent l’étude du français dans un programme d’immersion ont
généralement une connaissance limitée de la langue. Bien que ce ne soit pas une condition d’admission, ils ont souvent
fréquenté la maternelle en français et y ont acquis du vocabulaire de base du fait de leur exposition quotidienne à la
langue en classe. Dès la première année du primaire, ils seront amenés à développer leur vocabulaire général et à
acquérir celui qui se rattache plus spécifiquement aux autres disciplines enseignées en français. En effet, les élèves en
immersion apprennent la langue à la fois pour elle-même et pour être en mesure de l’utiliser dans l’apprentissage de
notions et de concepts propres à d’autres disciplines. Cette utilisation plurielle et régulière du français explique pour une
bonne part la rapidité avec laquelle ils se l’approprient.

Le vocabulaire et les stratégies constituent les éléments centraux des apprentissages de départ des élèves et la
compréhension de la langue en est la pierre angulaire, à l’oral comme à l’écrit. La familiarisation avec les réalités sonores
de la langue et l’apprivoisement de l’écrit se feront ainsi simultanément. Les élèves enrichiront graduellement leur
vocabulaire grâce à la lecture des divers textes auxquels ils seront régulièrement exposés, qu’il s’agisse de textes courants
ou littéraires ou encore de textes afférents aux autres disciplines. L’utilisation consciente de stratégies leur permettra de
devenir de plus en plus autonomes et de transférer au français divers apprentissages réalisés en anglais. Par ailleurs,

l’importance accordée à la littératie^2 et aux œuvres littéraires devrait contribuer de façon significative à l’ouverture des
élèves à l’égard de la culture francophone.

L’apprentissage de la langue se fait en spirale dans le cadre de tâches très variées et de complexité croissante tant en
français que dans certaines autres disciplines. Pour réaliser ces tâches, les élèves devront acquérir des connaissances
liées au texte, à la phrase et aux conventions de la communication. Il leur faudra également se familiariser avec le
métalangage^3 afin de pouvoir réfléchir au fonctionnement de la langue et aux difficultés parfois récurrentes qu’ils
éprouvent lors de la production d’énoncés. Ils s’approprieront ainsi des structures de base auxquelles s’ajouteront d’autres
éléments qui les amèneront peu à peu à développer leur compétence à communiquer à l’oral et à l’écrit.

Étudiant le français en tant qu’objet et s’en servant comme outil d’apprentissage, les élèves en immersion ont l’occasion
d’acquérir des connaissances et de les utiliser dans divers contextes, s’approchant en cela de la réalité scolaire des
locuteurs natifs. Ils devront par ailleurs les consolider, les enrichir et les réinvestir dans leur milieu, à l’extérieur de l’école,
afin d’atteindre un niveau proche du bilinguisme à la fin du secondaire.

1. La progression des apprentissages a été établie à partir du portrait d’un élève dont environ la moitié du temps de classe
    se déroule en français, tant pour l’apprentissage du français, langue seconde, que pour celui d’autres disciplines.
2. Selon Norma MacFarlane,la littératie désigne l’aptitude à comprendre une gamme de documents imprimés, électroniques
    et multimodaux ( _Réflexions sur la littératie_ , Saint-Lambert, Québec, Les Éditions Pearson Erpi, 2008, p. 41).
3. Signalons que la terminologie utilisée dans ce document pour présenter les connaissances liées au texte et à la phrase
    est généralement propre à la grammaire nouvelle, soit la même que celle adoptée dans différents pays francophones
    depuis déjà plusieurs années.


## Français, langue seconde

## (Programme d'immersion)

## Stratégies

En langue seconde, les stratégies d’apprentissage désignent « un ensemble d’opérations mises en œuvre par les
apprenants pour acquérir, intégrer et réutiliser la langue cible^1 ». Ces stratégies peuvent être d’ordre métacognitif (ex. :
identifier ses forces dans le but de les réinvestir), cognitif (ex. : organiser l’information) ou social et affectif (ex. : lire et
présenter son texte à ses pairs pour une rétroaction).

Devant apprendre la langue et l’utiliser pour apprendre, les élèves en immersion ont besoin de développer rapidement une
conscience stratégique pour parvenir à faire ce double apprentissage. Soutenus par leur enseignante ou leur enseignant,
ils apprendront graduellement à utiliser des stratégies à bon escient, selon les besoins de la situation de communication.
En général, ils reconnaîtront d’abord la stratégie modélisée par l’enseignante ou l’enseignant ainsi que son utilité. Ils se
l’approprieront ensuite progressivement dans le cadre de diverses activités. L’utilisation autonome d’une ou plusieurs
stratégies leur permettra alors de mieux interagir en français, de mieux comprendre et produire des textes en français et de
mieux évaluer leur démarche.

Le tableau qui suit présente des stratégies liées aux compétences du programme.

```
Stratégies
```
```
L’élève apprend à le faire avec l’intervention de l’enseignante ou de l’enseignant.
```
```
L’élève le fait par lui-même à la fin de l’année scolaire.
```
```
L’élève réutilise cette connaissance.
```
```
Primaire
```
```
1 er
cycle
```
```
2 e
cycle
```
```
3 e
cycle
```
```
A. Stratégies d’interaction 1 re 2 e 3 e 4 e 5 e 6 e
```
1. Prendre une posture d’écoute

```
a. en étant attentif à son interlocuteur
```
```
b. en maintenant son attention
```
2. Recourir au langage non verbal

```
a. pour demander la parole ou pour répondre à une question
```
```
b. pour remplacer un mot manquant
```
```
c. pour exprimer son incompréhension, son accord ou son désaccord
```
```
d. pour inciter l’interlocuteur à poursuivre
```
3. Solliciter l’aide de l’interlocuteur

```
a. en lui demandant de répéter
```
```
b. en lui demandant de ralentir son débit
```
```
c. en lui demandant de reformuler sa phrase ou d’expliquer
```
4. Utiliser divers moyens de dépannage tels que

```
a. la reformulation
```
```
b. l‘utilisation de paraphrases
```
```
c. l’explication
```
5. Participer activement


```
a. en saisissant les occasions de parler ou d’écrire en français
```
```
b. en reprenant les propos de son interlocuteur
```
```
c. en prenant des risques
```
```
d. en s’ouvrant à de nouvelles expériences
```
6. Adopter une attitude d’ouverture

```
a. à l’égard de ses pairs
```
```
b. à l’égard de la culture francophone
```
**B. Stratégies de compréhension**^21 **re 2 e 3 e 4 e 5 e 6 e**

1. Anticiper le contenu du texte oral, écrit ou visuel à partir de certains éléments

```
a. des illustrations
```
```
b. des éléments sonores
```
```
c. des éléments gestuels
```
```
d. des éléments prosodiques
```
```
e. du sujet annoncé
```
```
f. du titre
```
```
g. des intertitres
```
```
des tableaux, des schémas, des diagrammes, des légendes, des graphiques, des
bulles explicatives
```
```
h.
```
```
i. de ses connaissances antérieures d’ordre général ou linguistique
```
```
Recourir à son bagage d’expériences et de connaissances d’ordre général ou linguistique
ainsi qu’à ses nouvelles connaissances issues des autres disciplines pour établir des liens
avec les textes entendus, lus ou vus
```
### 2.

3. Recourir, pendant la lecture, l’écoute ou le visionnage de textes, à divers indices

```
d’ordre graphique (ex. : grosseur des lettres, caractères gras ou italiques,
soulignement)
```
```
a.
```
```
b. d’ordre sémantique (ex. : mots substituts, contexte des mots)
```
```
d’ordre morphologique (ex. : correspondance graphophonétique, marques du genre
et du nombre, terminaison des verbes, préfixes, suffixes)
```
```
c.
```
```
d’ordre syntaxique (ex. : ordre des mots, mots permettant de faire les enchaînements
dans une phrase)
```
```
d.
```
4. Se donner une représentation du contenu du texte^3^
5. Identifier les éléments d’information essentiels

```
a. en repérant les mots connus du texte
```
```
b. en reformulant des informations explicites dans ses propres mots
```
```
c. en repérant les signes qui délimitent la phrase (majuscule, point, pause, arrêt)
```
```
d. en organisant les informations à l’aide d’un outil (ex. : carte sémantique)
```
```
en faisant ressortir des informations implicites, à partir des indices du texte ou de ses
connaissances générales
```
```
e.
```
6. Recourir à divers moyens de dépannage tels que :

```
a. les questions à l’enseignant ou à ses pairs
```

```
b. la poursuite de l’écoute ou de la lecture malgré les incompréhensions
```
```
c. la seconde écoute ou la relecture
```
```
la banque de mots, l’abécédaire, les cartes de sons, les pictogrammes, les
dictionnaires, les illustrations, les textes connus, les phrases modèles
```
```
d.
```
```
e. le décodage graphophonétique
```
```
f. le partage de sa compréhension avec un pair
```
```
C. Stratégies de production 1 re 2 e 3 e 4 e 5 e 6 e
```
```
Activer ses connaissances pour générer du vocabulaire ou des idées (ex. : remue-
méninges, réseaux sémantiques)
```
### 1.

2. Sélectionner des informations ayant un lien avec

```
a. le sujet
```
```
b. l’intention
```
```
c. le destinataire
```
```
Organiser l’information sélectionnée à l’aide d’un outil (ex. : plan, réseau sémantique,
diagramme ou tableau)
```
### 3.

```
Reproduire la structure de certains modèles de textes, oraux et écrits, proposés par
l’enseignant
```
### 4.

5. S’inspirer de modèles pour créer un texte
6. Réutiliser des mots ou expressions entendus, lus ou vus

```
Réutiliser des notions grammaticales apprises en classe (ex. : le genre et le nombre, les
temps de verbes)
```
### 7.

```
Enrichir son texte en employant des synonymes, des antonymes, des périphrases, des
mots spécifiques^4
```
### 8.

9. Recourir à des ressources

```
a. humaines (ex. : l’enseignant, les pairs, la bibliothécaire)
```
```
linguistiques (ex. : des phrases modèles, des listes orthographiques, un dictionnaire,
des tableaux de conjugaison, une grammaire)
```
```
b.
```
```
c. technologiques (ex. : Internet, des logiciels, des cédéroms)
```
```
Utiliser des codes de correction et des outils de révision et laisser des traces de son
travail de révision
```
### 10.

```
Lire et présenter son texte à une ou plusieurs personnes afin d’obtenir des suggestions
pour en améliorer le contenu, la structure, les éléments prosodiques ou la présentation
visuelle
```
### 11.

```
D. Stratégies d’évaluation de sa démarche 1 re 2 e 3 e 4 e 5 e 6 e
```
1. Utiliser une liste de vérification pour revoir le déroulement de la tâche
2. Nommer les stratégies et les ressources utilisées pour interagir, comprendre et produire
3. Faire l’autoévaluation de ses apprentissages à l’aide d’outils prévus à cet effet

```
cibler un ou des éléments à modifier en vue d’améliorer ses interactions, sa
compréhension et ses productions
```
```
a.
```
```
b. s’interroger sur le choix des stratégies et les ressources utilisées
```
```
c. identifier ses forces dans le but de les réinvestir ultérieurement
```
1. Paul Cyr, _Le point sur les stratégies d’apprentissage d’une langue seconde_ , Montréal, CEC, 1996, p. 5.
2. D’autres stratégies ayant trait à la compréhension de mots ou d’expressions se trouvent à la fin de la section
    _Vocabulaire_.


3. Certains théoriciens parlent d’imagerie mentale ou de visualisation.
4. Un mot spécifique fait partie d’un ensemble de mots précisant un mot générique.


## Français, langue seconde

## (Programme d'immersion)

## Vocabulaire utilisé à l’oral et à l’écrit

Pour l’enfant qui commence son apprentissage du français en immersion, le vocabulaire constitue la porte d’entrée pour
comprendre, lire ou produire des textes et pour communiquer efficacement en français. Il lui permet également d’apprendre
en français des concepts propres à d’autres disciplines.

Dès leur arrivée au primaire, les élèves sont exposés à un vocabulaire riche provenant d’une diversité de textes, littéraires,
courants ou liés à d’autres disciplines. À l’aide de diverses ressources telles que des banques de mots, des cartes de
sons, des illustrations ou des dictionnaires, ils acquièrent graduellement un vocabulaire varié qui leur permet de mieux
cerner des concepts, de découvrir des repères culturels et de répondre à leurs divers besoins. Ils réinvestissent par la
suite ces mots dans le cadre d’activités signifiantes, à l’oral et à l’écrit. Ils explorent ainsi la langue dans toute sa richesse,
dans toute sa complexité et dans tous ses usages.

```
Le tableau qui suit contient des connaissances sur le vocabulaire de base, sur le vocabulaire lié à l’expression des
besoins d’ordre personnel, scolaire ou social des élèves, à différentes notions, à la description, aux repères culturels, à
différentes expressions, au métalangage ainsi que sur le vocabulaire relatif aux disciplines. Il présente également les
stratégies utiles au développement du vocabulaire.
```
```
Vocabulaire utilisé à l’oral et à l’écrit
```
```
L’élève apprend à le faire avec l’intervention de l’enseignante ou de l’enseignant.
```
```
L’élève le fait par lui-même à la fin de l’année scolaire.
```
```
L’élève réutilise cette connaissance.
```
```
Primaire
```
```
1 er
cycle
```
```
2 e
cycle
```
```
3 e
cycle
```
```
A. Vocabulaire de base 1 re 2 e 3 e 4 e 5 e 6 e
```
1. Reconnaître et comprendre le vocabulaire de base

```
pour nommer une personne (ex. : un pompier, une tante ) , un animal (ex. : une poule,
un tigre ) , un végétal ( ex. : une pomme, un érable ), un objet (ex. : un avion, une
montre )
```
```
a.
```
```
pour nommer un événement (ex. : un anniversaire, une célébration ), un repère
culturel^1 (ex. : un castor, une citrouille )
```
```
b.
```
```
pour nommer une activité (ex. : une randonnée , une partie de soccer , le cinéma ), un
service (ex. : la poste , le train ), un lieu^2 (ex. : un musée , un centre commercial , une
bibliothèque )
```
```
c.
```
2. Utiliser le vocabulaire de base

```
a. pour nommer une personne, un animal, un végétal, un objet
```
```
b. pour nommer un événement, un repère culturel
```
```
c. pour nommer une activité, un service, un lieu
```
```
Vocabulaire lié à l’expression de ses besoins d’ordre personnel, scolaire ou
social
```
```
B.
1 re 2 e 3 e 4 e 5 e 6 e
```
1. Reconnaître et comprendre le vocabulaire lié à l’expression de ses besoins d’ordre personnel, scolaire ou social

```
a. pour saluer (ex. : dire bonjour, prendre congé)
```
```
b. pour se présenter ou présenter quelqu’un
```
```
i. soi-même ou sa famille
```
```
ii. ses amis, des personnes de son entourage
```
```
c. pour répondre aux consignes (ex. : nommer, dessiner, lever la main, écouter )
```

```
d. pour exprimer
```
```
sa compréhension, son incompréhension, sa capacité ou son incapacité (ex. :
je sais, je ne comprends pas, je peux )
```
```
i.
```
```
ses goûts, ses préférences ou ses sentiments (ex. : j’aime, je déteste, je suis
contente )
```
```
ii.
```
```
iii. son acceptation, son refus (ex. : je veux, je suis d’accord, je refuse )
```
```
iv. son état physiologique (ex. : j’ai mal à la tête )
```
```
v. son appréciation (ex. : c’est bien, intéressant, beau, vrai, correct, pas du tout )
```
```
e. pour faire des demandes
```
```
i. d’aide, de permission
```
```
ii. de renseignement, de service
```
```
f. pour offrir de l’aide (ex. : Veux-tu mon crayon? )
```
```
pour présenter des remerciements, des félicitations, des encouragements, des
vœux, des souhaits ou des excuses (ex. : merci, bravo, je m’excuse )
```
```
g.
```
2. Utiliser le vocabulaire lié à l’expression de ses besoins d’ordre personnel, scolaire ou social

```
a. pour saluer
```
```
b. pour se présenter ou présenter quelqu’un
```
```
i. soi-même ou sa famille
```
```
ii. ses amis, des personnes de son entourage
```
```
c. pour donner des consignes à ses pairs
```
```
d. pour exprimer
```
```
i. sa compréhension, son incompréhension, sa capacité ou son incapacité
```
```
ii. ses goûts, ses préférences ou ses sentiments
```
```
iii. son acceptation, son refus
```
```
iv. son état physiologique
```
```
v. son appréciation
```
```
e. pour faire des demandes
```
```
i. d’aide, de permission
```
```
ii. de renseignement, de service
```
```
f. pour offrir de l’aide
```
```
pour présenter des remerciements, des félicitations, des encouragements, des
vœux, des souhaits ou des excuses
```
```
g.
```
**C. Vocabulaire relatif à certaines notions 1 re 2 e 3 e 4 e 5 e 6 e**

1. Reconnaître et comprendre le vocabulaire relatif à certaines notions

```
l’existence (ex. : il y a, il y a quelqu’un, il n’y a personne, il est présent, elle est
absente )
```
```
a.
```
```
le temps (ex. : au printemps, à Noël, en juin, lundi, après-midi, hier, à 9 h 29, cette
année )
```
```
b.
```
```
c. l’espace (ex. : sur, sous, à droite, à l’avant de, en face, à côté, derrière, à l’intérieur )
```

```
la qualité (ex. : mou, sec, dur, rond, haut, amer, doux, long, grand, bon, mauvais,
rude, soyeux, lisse )
```
```
d.
```
```
e. la quantité
```
```
i. les nombres (de 0 à 1 000 000)
```
```
ii. les collectifs (ex. : beaucoup de , trop de , assez de , combien de )
```
```
les comparatifs (ex.: plus... que, moins... que, autant... que, pareil à, comme,
meilleur que, pire que, aussi, différent )
```
```
iii.
```
2. Utiliser le vocabulaire relatif à certaines notions

```
a. l’existence
```
```
b. le temps
```
```
c. l’espace
```
```
d. la qualité
```
```
e. la quantité
```
```
i. les nombres
```
```
ii. les collectifs
```
```
iii. les comparatifs
```
**D. Vocabulaire lié à la description 1 re 2 e 3 e 4 e 5 e 6 e**

1. Reconnaître et comprendre le vocabulaire lié à la description

```
des caractéristiques d’une personne (ex. : la personnalité, l’habillement, la
nationalité), d’un animal (ex. : la race, les traits physiques), d’un végétal (ex. : l’utilité,
l’apparence), d’un objet (ex. : la fabrication, la provenance), d’un lieu
(ex. : l’emplacement, le paysage) ou d’un repère culturel (ex. : l’origine, la tradition)
```
```
a.
```
```
b. d’un fait ou d’un événement culturel ou social (ex. : la date, la programmation)
```
```
d’une activité (ex. : le matériel requis) ou d’un service de sa communauté (ex. : le
transport)
```
```
c.
```
2. Utiliser le vocabulaire lié à la description

```
des caractéristiques d’une personne, d’un animal, d’un végétal, d’un objet, d’un lieu
ou d’un repère culturel
```
```
a.
```
```
b. d’un fait ou d’un événement culturel ou social
```
```
c. d’une activité ou d’un service de sa communauté
```
**E. Vocabulaire lié aux repères culturels 1 re 2 e 3 e 4 e 5 e 6 e**

1. Reconnaître et comprendre le vocabulaire lié aux repères culturels

```
a. les célébrations culturelles (ex. : un festival, la Saint-Jean-Baptiste, le Nouvel An)
```
```
les produits culturels francophones (ex. : la chanson, le livre, l’émission télévisée, la
pièce de théâtre, le film, le cédérom, le site Internet, le jeu, la recette)
```
```
b.
```
2. Utiliser le vocabulaire lié aux repères culturels

```
a. les célébrations culturelles
```
```
b. les produits culturels francophones
```
```
Reconnaître et comprendre des termes appartenant au langage familier ou au langage
standard (ex. : patate-pomme de terre ; niaiser-perdre son temps , achaler-agacer )
```
### 3.

```
F. Vocabulaire relatif à différentes expressions 1 re 2 e 3 e 4 e 5 e 6 e
```

```
Reconnaître et comprendre certaines expressions courantes (ex. : il était une fois, tout à
coup, de temps en temps, après tout, par cœur )
```
### 1.

2. Utiliser certaines expressions courantes

```
Reconnaître et comprendre certaines expressions idiomatiques (ex. : donner sa langue au
chat, il pleut à boire debout )
```
### 3.

4. Utiliser certaines expressions idiomatiques

```
Reconnaître et comprendre des formules d’introduction et de conclusion liées à des
situations formelles (ex. : concours d’art oratoire : honorable juge , lettre : veuillez agréer )
```
### 5.

6. Utiliser des formules d’introduction et de conclusion liées à des situations formelles

**G. Vocabulaire relatif au métalangage 1 re 2 e 3 e 4 e 5 e 6 e**

```
Reconnaître et comprendre certains mots propres au métalangage (ex. : le nom, le verbe,
la virgule, l’accent, le genre, le radical et la terminaison, l’accord, le volume de la voix, le
débit )
```
### 1.

2. Utiliser certains mots propres au métalangage

```
H. Vocabulaire relatif aux disciplines^31 re 2 e 3 e 4 e 5 e 6 e
```
1. Reconnaître et comprendre des mots propres **à la mathématique**

```
a. arithmétique (ex. : nombres naturels, fractions, nombres décimaux )
```
```
b. géométrie (ex. : solides, figures planes, dallages )
```
```
c. mesure (ex. : longueurs, angles, surfaces, périmètre, aire )
```
```
statistique (ex. : collecte de données, diagramme à bandes, à pictogrammes et à
ligne brisée, moyenne arithmétique )
```
```
d.
```
```
probabilité (ex. : dénombrement, hasard, résultat certain [possible, impossible],
événement également probable )
```
```
e.
```
2. Reconnaître et comprendre des mots propres **à la science et à la technologie**

```
a. univers matériel (ex. : propriété, matériau, circuit électrique )
```
```
b. Terre et espace (ex. : énergie renouvelable, cycle de l’eau, rotation de la Terre )
```
```
c. univers vivant (ex. : croissance, anatomie, adaptation )
```
3. Reconnaître et comprendre des mots propres **à la géographie, à l’histoire et à l’éducation à la citoyenneté**

```
localisation de la société dans l’espace et le temps (ex. : les Grands Lacs, la vallée
du Saint-Laurent, la fondation de Montréal en 1642 )
```
```
a.
```
```
éléments de la société qui ont une incidence sur l’aménagement du territoire (ex. :
commerce des fourrures, fédération canadienne )
```
```
b.
```
```
atouts et contraintes du territoire occupé (ex. : hydrographie, relief, ressources du
territoire)
```
```
c.
```
```
influence de personnages et incidences d’événements sur l’organisation sociale et
territoriale (ex. : roi, gouverneur, évêque )
```
```
d.
```
```
e. éléments de continuité avec le présent (ex. : artéfacts et sites, noms de villes)
```
4. Reconnaître et comprendre des mots propres **à l’art dramatique**

```
a. langage dramatique (ex. : attitude, geste, mimique, mouvement, voix parlée, chœur )
```
```
techniques de jeu (ex. : concentration, détente, équilibre/déséquilibre,
mémorisation, projection, posture, respiration )
```
```
b.
```
```
techniques théâtrales (ex. : castelet, effets comiques, exagération, imitation,
manipulation de la marionnette, masque, position du corps )
```
```
c.
```
```
modes de théâtralisation (ex. : bruitage, costume, côté jardin/côté cour, éclairage,
espace, objet, répétition, théâtre )
```
```
d.
```
```
structure (ex. : action dramatique, canevas, dialogue, improvisation, monologue,
personnage )
```
```
e.
```

5. Reconnaître et comprendre des mots propres **aux arts plastiques**

```
gestes transformateurs et outils (ex. : dessiner, coller, imprimer, modelage,
peinture, crayon feutre, carton, pâte à modeler, ciseaux, brosse )
```
```
a.
```
```
langage plastique (ex. : forme angulaire, ligne large, cyan, tridimensionnelle,
énumération, juxtaposition, superposition )
```
```
b.
```
6. Reconnaître et comprendre des mots propres **à la danse et au langage de la danse**

```
a. corps (ex. : marcher, courir, chuter, rouler, gambader )
```
```
b. temps (ex. : pulsation, tempo lent, tempo rapide, arrêt )
```
```
c. espace (ex. : niveaux, directions, amplitudes )
```
```
d. énergie (ex. : effort soudain, effort soutenu, accélération )
```
```
e. relations entre partenaires (ex. : face à face, se rencontrer, se croiser, en file )
```
```
techniques du mouvement (ex. : relâchement, contraction, flexion, extension,
allongement )
```
```
f.
```
```
g. procédés de composition (ex. : répétition ou variation d’une phrase de mouvement )
```
```
h. structures (ex. : départ et finale, mouvements enchaînés, ronde, farandole )
```
7. Reconnaître et comprendre des mots propres **à la musique**

```
a. langage musical (ex. : doux, fort, court, long, soupir, grave, aigu, sec, résonnant )
```
```
représentation graphique (ex. : blanche, deux croches, noire, do, ré, mi, fa, sol, la,
si )
```
```
b.
```
```
moyens sonores (ex. : instrument de percussion, instrument à cordes, instrument à
vent, cuivres, synthétiseur )
```
```
c.
```
```
d. techniques instrumentales (ex. : souffler, gratter, frotter, secouer, frapper )
```
```
e. procédés de composition (ex. : ostinato, miroir et contraste )
```
```
f. structures (ex. : canon, forme A-B, lent, modéré, rapide, ascendant, descendant )
```
8. Reconnaître et comprendre des mots propres **à l’éducation physique et à la santé**

```
a. vocabulaire lié au matériel utilisé (ex. : ballon, bâton, banc, espalier )
```
```
vocabulaire lié aux actions motrices (ex. : marcher, courir, sauter, tourner, jongler,
attraper )
```
```
b.
```
```
vocabulaire lié aux activités physiques individuelles ou collectives (ex. :
gymnastique, ski de fond, jonglerie, danse aérobique, lutte, badminton, mini-volley,
mini-basket )
```
```
c.
```
```
vocabulaire lié aux principes d’action (ex. : feinter, faire circuler l’objet, attaquer la
cible )
```
```
d.
```
```
vocabulaire lié aux habitudes de vie (ex. : activité physique régulière, pratique
sécuritaire, endurance cardiovasculaire )
```
```
e.
```
9. Reconnaître et comprendre des mots propres **à l’éthique et à la culture religieuse**^4

```
a. célébrations en famille (ex. : fêtes, rituels de naissance)
```
```
récits marquants (ex. : récits de personnages importants, récits qui ont une grande
importance)
```
```
b.
```
```
pratiques religieuses en communauté (ex. : lieux de culte, des objets et des
symboles)
```
```
c.
```
```
expressions du religieux dans l’environnement (ex. : monuments, édifices,
toponymie)
```
```
d.
```
```
religions dans la société et dans le monde (ex. : fondateurs, géographie et
démographie des religions)
```
```
e.
```
```
valeurs et normes religieuses (ex. : personnes modèles, pratiques alimentaires et
vestimentaires)
```
```
f.
```

```
Utiliser dans ses productions ou ses interactions, des mots propres aux différentes
disciplines enseignées
```
### 10.

```
I. Stratégies utiles au développement du vocabulaire 1 re 2 e 3 e 4 e 5 e 6 e
```
1. Repérer des expressions ou des mots connus, à l’oral et à l’écrit
2. Inférer le sens d’un mot à partir des illustrations, des éléments gestuels ou du contexte

```
Recourir à un aide-mémoire, une liste orthographique, un abécédaire ou une banque de
mots
```
### 3.

```
Recourir à ses connaissances antérieures d’ordre linguistique (ex. : la comparaison de
mots apparentés en anglais et en français)
```
### 4.

```
Recourir à différentes techniques pour favoriser le développement et la rétention du
vocabulaire (ex. : la chanson, la comptine, le champ lexical)
```
### 5.

6. Activer ses connaissances pour générer du vocabulaire (ex. : le remue-méninges)

```
Repérer des mots de même famille à partir de mots connus (ex. : grand, grandeur, grandir,
agrandir )
```
### 7.

8. Recourir à des indices d’ordre morphologique pour comprendre le sens d’un mot

```
a. le préfixe (ex. : refaire, défaire )
```
```
b. le suffixe (ex. : fille, fillette )
```
9. Recourir au contexte ou à des ressources (ex. : dictionnaire) pour découvrir le sens d’un mot

```
a. sens propre (ex.: une manche de chandail et un manche de pelle )
```
```
b. sens figuré (ex. : la tête et la tête du peloton )
```
```
Recourir au contexte pour comprendre le sens des mots qui se prononcent de façon
identique, mais dont l’orthographe diffère (ex. : mer/mère/maire , faim/fin , son/sont )
```
### 10.

```
Recourir à la synonymie, l’antonymie, la périphrase, les mots spécifiques pour préciser ou
enrichir son vocabulaire
```
### 11.

1. Voir la sous-section _Vocabulaire lié aux repères culturels._
2. L’enseignant peut faire prendre conscience aux élèves de l’importance de parler français dans les lieux publics au
    Québec.
3. Le vocabulaire présenté dans cette sous-section est celui qui est incontournable et qui doit être généralement connu à la
    fin du primaire. L’enseignant devra toutefois se référer au programme de chacune des disciplines à l’étude en français
    pour planifier son enseignement.
4. Il est à noter que le vocabulaire relatif à la réflexion éthique et à la pratique du dialogue se trouve également dans
    d’autres sous-sections.


## Français, langue seconde

## (Programme d'immersion)

## Conventions de communication orale ou écrite

```
Très jeune, un enfant apprend à saluer poliment ses interlocuteurs, à les remercier ou à prendre la parole au moment
opportun. Ces connaissances sociolinguistiques ne sont pas propres à une langue. En immersion, l’élève pourra aisément
réinvestir ces acquis en français, avec l’aide de son enseignante ou de son enseignant, dans le cadre de diverses tâches
de production ou d’interaction.
```
```
Par ailleurs, certaines connaissances sociolinguistiques et conventions sont spécifiques du français. Par exemple, l’emploi
du vouvoiement et du conditionnel présent dans un contexte formel devra être appris, tout comme les conventions ayant
trait à la prosodie, à la phonétique et à l’orthographe. Les élèves d’immersion doivent donc acquérir ces connaissances,
```
essentielles pour l’apprentissage du français. Ils apprennent ainsi à développer rapidement leur conscience phonologique^1
et à se familiariser avec les réalités sonores de la langue, ce qui les aide sur le plan de la prononciation et de l’articulation.
À l’écrit, ils découvrent le système orthographique, et plus particulièrement ses régularités. Ce faisant, ils apprennent à
écrire correctement les mots auxquels ils sont le plus fréquemment exposés.

```
Le tableau qui suit présente les connaissances nécessaires pour mieux comprendre et utiliser les conventions essentielles
à une bonne communication, tant à l’oral qu’à l’écrit.
```
```
Conventions de communication orale ou écrite
```
```
L’élève apprend à le faire avec l’intervention de l’enseignante ou de l’enseignant.
```
```
L’élève le fait par lui-même à la fin de l’année scolaire.
```
```
L’élève réutilise cette connaissance.
```
```
Primaire
```
```
1 er
cycle
```
```
2 e
cycle
```
```
3 e
cycle
```
```
A. Éléments gestuels et non verbaux 1 re 2 e 3 e 4 e 5 e 6 e
```
```
Observer le langage non verbal d’un locuteur ou de son interlocuteur pour comprendre le
message (ex. : les gestes, les mimiques, les expressions faciales)
```
### 1.

2. Interpréter le langage non verbal pour réagir à un message
3. Utiliser le langage non verbal pour illustrer ses propos
4. Reconnaître l’importance du contact visuel

```
a. avec son interlocuteur
```
```
b. avec son auditoire
```
5. Maintenir le contact visuel avec son interlocuteur ou son auditoire

```
B. Éléments prosodiques 1 re 2 e 3 e 4 e 5 e 6 e
```
```
Répéter des mots, des comptines, des chansons et des expressions connus pour
développer son aisance à prononcer
```
### 1.

```
Observer et reconnaître des éléments prosodiques employés en contexte (ex. : intonation,
débit, rythme, volume de la voix, articulation)
```
### 2.

```
Reproduire certains éléments prosodiques (ex. : la récitation d’une série de mots en
respectant le rythme et en articulant clairement, la répétition de courtes phrases en
changeant l’intonation)
```
### 3.

4. Interpréter l’intention du message entendu

```
a. selon le volume (ex. : faible qui indique le partage d’un secret)
```
```
b. selon le débit (ex. : rapide qui indique l’urgence)
```
```
c. selon l’intonation (ex. : montante qui indique un questionnement)
```
```
Produire un message en respectant des éléments prosodiques (ex. : le débit, le volume de
la voix, l’articulation)
```
### 5.


```
Recourir à des éléments prosodiques pour rendre son message plus expressif (ex. :
l’intonation pour exprimer la surprise, le débit pour attirer l’attention)
```
### 6.

7. Observer et reconnaître les liaisons (ex. : _les amis, des enfants_ )
8. Effectuer les liaisons fréquemment entendues

```
C. Orthographe d’usage 1 re 2 e 3 e 4 e 5 e 6 e
```
1. Mémoriser et réciter l’alphabet
2. Repérer les lettres minuscules et majuscules de l’alphabet
3. Nommer les lettres minuscules et majuscules de l’alphabet
4. Nommer les voyelles et les consonnes
5. Reconnaître globalement à l’écrit une expression ou un mot appris
6. Repérer des mots en utilisant les lettres de l’alphabet

```
Observer la correspondance phonème-graphème^2 dans les mots appris (ex. : /s/ comme
dans ssss apin, /m/ comme dans mmmm aison, /R/ sonore comme dans rouge , /y/
comparé à /u/ comme dans bulle/boule )
```
### 7.

```
Produire les phonèmes correspondant à chacune des lettres de l’alphabet dans les mots
appris (ex. : la lettre c se prononce /k/ dans carotte )
```
### 8.

```
Produire les graphèmes correspondant aux phonèmes dans les mots appris (ex. : le
phonème /o/ s’écrit « eau » dans beau )
```
### 9.

```
Observer et reconnaître des similitudes et des différences dans la représentation phonémique ou graphique de mots
connus
```
### 10.

```
a. la rime et l’assonance (ex. : ballon et maison)
```
```
les lettres muettes (ex. : d muet comme dans gourmand, e caduc comme dans
maint’nant et maintenant, e muet comme dans vache )
```
```
b.
```
```
c. l’homophonie (ex. : Tes amis t’ont dit que ton chapeau est laid .)
```
11. Observer l’orthographe des mots fréquemment utilisés en classe
12. Orthographier correctement un répertoire de mots fréquemment utilisés en classe
13. Utiliser des stratégies pour l’acquisition de l’orthographe d’usage

```
formuler des hypothèses sur l’orthographe d’un mot (ex. : association
graphophonétique)
```
```
a.
```
```
utiliser des techniques de mémorisation (ex. : segmenter en syllabes, visualiser un
mot, repérer des détails orthographiques)
```
```
b.
```
```
identifier des différences orthographiques entre les mots qui se prononcent de façon
identique (ex. : l’a/la, pain/pin, amande/amende )
```
```
c.
```
```
D. Signes graphiques^31 re 2 e 3 e 4 e 5 e 6 e
Observer et reconnaître l’accent aigu, l’accent grave, l’accent circonflexe, la cédille, le
tréma et le trait d’union dans un mot appris
```
### 1.

```
Prononcer correctement des mots contenant un accent aigu, un accent grave, un accent
circonflexe, une cédille ou un tréma.
```
### 2.

3. Utiliser à l’écrit les accents, la cédille et le tréma dans les mots appris
4. Utiliser le trait d’union

```
a. pour former un mot composé
```
```
b. pour marquer l’inversion du verbe avec le sujet dans une phrase interrogative
```
```
c. pour couper un mot à la fin d’une ligne
```
5. Observer et reconnaître l’utilisation de l’apostrophe dans l’élision (ex. : _l’école, l’ami_ )


6. Utiliser l’apostrophe dans l’élision avec les mots appris en classe
7. Observer et reconnaître les règles d’emploi de la majuscule

```
a. au début d’une phrase
```
```
dans les noms propres (de personnes, d’animaux, de lieux, de peuples ou
d’événements)
```
```
b.
```
8. Utiliser correctement la majuscule

```
a. au début d’une phrase
```
```
dans les noms propres (de personnes, d’animaux, de lieux, de peuples ou
d’événements)
```
```
b.
```
9. Observer et reconnaître la ponctuation dans les modèles de phrases proposés par l’enseignant

```
a. le point final
```
```
b. le point d’interrogation et d’exclamation
```
```
c. la virgule dans les énumérations
```
```
d. les guillemets, le tiret, les deux-points, les parenthèses, les points de suspension
```
```
Associer des signes de ponctuation à une pause (virgule et point) ou à une intonation
(point d’interrogation et d’exclamation) lors d’une lecture à voix haute
```
### 10.

11. Utiliser la ponctuation dans ses productions orales ou écrites

```
a. le point final
```
```
b. le point d’interrogation et d’exclamation
```
```
la virgule dans les énumérations, les guillemets, le tiret, les deux-points, les
parenthèses et les points de suspension
```
```
c.
```
1. La _conscience phonologique_ fait référence à la connaissance consciente et explicite que les mots du langage sont
    formés d’unités plus petites, soit les syllabes et les phonèmes. Rappelons que le phonème est la plus petite unité de la
    langue orale correspondant à un son qui permet de distinguer des mots entre eux; il n’est pas porteur de sens en
    lui-même (ex. : /b//o/ = beau, /p//o/ = peau).
2. La correspondance graphophonétique permet de traduire un son par un graphème, c’est-à-dire une lettre ou un groupe
    de lettres correspondant à un phonème (ex. : o, ô, au, eau pour le son /o/).
3. Il est à noter que le terme _signe orthographique_ est utilisé dans la nouvelle grammaire.


## Français, langue seconde

## (Programme d'immersion)

## Connaissances liées au texte oral ou écrit

Dans le programme de français, langue seconde, la définition du mot _texte_ va au-delà de sa représentation habituelle et du
cadre de l’écrit. Un texte peut ainsi prendre la forme d’une image, d’un ou plusieurs mots, d’une ou plusieurs phrases, d’un
ou plusieurs paragraphes. Les élèves sont donc amenés à visionner, à lire ou à entendre des textes de diverses natures et
à en produire.

Pour leurs premiers pas en langue seconde, les élèves ont besoin d’un support visuel ou sonore afin de mieux
appréhender le sens d’un texte. En étant exposés régulièrement à des textes authentiques et signifiants, de différents
genres, littéraires et courants, ils peuvent en observer l’organisation et, par la suite, être en mesure d’en produire une
variété. Ils apprennent également à tenir compte du sujet, de l’intention et du destinataire ainsi qu’à organiser leurs idées
de façon cohérente. Ils ont ainsi l’occasion de tirer profit des connaissances acquises dans les textes entendus, lus ou vus
pour les réinvestir dans leurs interactions et leurs productions ultérieures.

Le tableau qui suit contient des connaissances relatives aux genres de textes, à l’organisation textuelle, visuelle et sonore
de l’information ainsi qu’aux éléments qui assurent la cohérence d’un texte.

```
Connaissances liées au texte oral ou écrit
```
```
L’élève apprend à le faire avec l’intervention de l’enseignante ou de l’enseignant.
```
```
L’élève le fait par lui-même à la fin de l’année scolaire.
```
```
L’élève réutilise cette connaissance.
```
```
Primaire
```
```
1 er
cycle
```
```
2 e
cycle
```
```
3 e
cycle
```
```
A. Genres^1 de textes^21 re 2 e 3 e 4 e 5 e 6 e
```
1. Observer globalement et reconnaître le genre de certains textes littéraires entendus, lus ou vus dont le but est

```
de raconter (ex. : album, conte, saynète, émission télévisée, roman jeunesse, fait
divers, récit historique, bande dessinée)
```
```
a.
```
```
b. de jouer avec les mots (ex. : comptine, devinette, chanson, poème, blague, énigme)
```
2. Observer globalement et reconnaître le genre de certains textes courants entendus, lus ou vus dont le but est

```
de donner des instructions (ex. : un mode d’emploi, des règles de jeux, une recette,
des consignes)
```
```
a.
```
```
de décrire (ex. : un ouvrage documentaire illustré, une biographie, un rapport
scientifique, une fiche d’identité, un compte rendu, une annonce classée, un article
sur le Web)
```
```
b.
```
```
c. d’expliquer (ex. : un article de journal, un reportage, une entrevue)
```
```
de convaincre ou d’inciter à agir (ex. : une affiche promotionnelle, un message
publicitaire à la radio, une invitation)
```
```
d.
```
3. Produire des textes, littéraires ou courants, à l’oral ou à l’écrit

```
Observer le support médiatique^3 utilisé pour la présentation du texte (ex. : diaporama,
maquette, dépliant, enregistrement vidéo, carte géographique)
```
### 4.

```
Choisir un support médiatique pour la présentation de sa production de texte, à l’oral ou à
l’écrit
```
### 5.

```
B. Organisation textuelle, visuelle et sonore de l’information 1 re 2 e 3 e 4 e 5 e 6 e
```
```
Observer et reconnaître les marques d’organisation textuelle, visuelle et sonore de l’information et reconnaître l’utilité
de certains éléments
```
### 1.

```
des éléments visuels (ex. : rôle de la couleur, des illustrations, des photos, de la
grosseur des lettres, du caractère gras, du soulignement)
```
```
a.
```
```
b. des éléments sonores (ex. : musique, bruit, son)
```
```
c. des titres
```
```
d. des intertitres
```

```
e. la table des matières, l’index et le glossaire
```
2. Utiliser les marques d’organisation textuelle, visuelle et sonore de l’information

```
a. les éléments visuels
```
```
b. les éléments sonores
```
```
c. les titres
```
```
d. les intertitres
```
```
e. la table des matières, l’index et le glossaire
```
3. Observer et reconnaître la structure du texte entendu, lu ou vu

```
a. l’organisation de certains textes littéraires
```
```
i. début, milieu, fin
```
```
situation initiale, élément déclencheur, péripéties, dénouement et situation
finale
```
```
ii.
```
```
b. l’organisation de certains textes courants : introduction, développement, conclusion
```
```
c. l’organisation des idées en paragraphes, en refrain, en couplets ou en strophes
```
4. Produire un texte structuré, à l’oral ou à l’écrit, qui contient certains éléments

```
a. un début, un milieu et une fin dans un texte littéraire
```
```
une situation initiale, un événement déclencheur, des péripéties, un dénouement et
une situation finale dans un texte littéraire
```
```
b.
```
```
c. une introduction, un développement et une conclusion dans un texte courant
```
```
d. des paragraphes, un refrain, des couplets ou des strophes
```
**C. Éléments de cohérence du texte 1 re 2 e 3 e 4 e 5 e 6 e**

1. Observer et reconnaître les éléments de la situation de communication présents dans un texte oral ou écrit

```
a. le sujet
```
```
b. l’intention de communication
```
```
c. le destinataire (interlocuteur, lecteur, auditeur)
```
2. Interagir ou produire un texte, à l’oral ou à l’écrit

```
a. en respectant le sujet
```
```
en respectant l’intention donnée (ex. : informer, inciter à agir ou à réagir, exprimer
des sentiments ou des émotions, divertir, se divertir, raconter)
```
```
b.
```
```
en tenant compte de certaines caractéristiques de son destinataire (ex. : l’âge, le
statut)
```
```
c.
```
```
Observer et reconnaître, à l’oral ou à l’écrit, les mots ou les groupes de mots qui assurent la cohérence entre
certaines parties d’un texte (les organisateurs textuels^4 ) et qui indiquent une valeur
```
### 3.

```
a. de temps (ex. : autrefois, aujourd’hui, demain, maintenant )
```
```
b. de séquence (ex. : d’abord, puis, après, ensuite, premièrement, finalement )
```
4. Utiliser des organisateurs textuels, en production ou en interaction orale ou écrite

```
a. qui indiquent le temps
```
```
b. qui indiquent la séquence
```

```
Observer et reconnaître l’ordre logique et chronologique des idées, des informations ou
des évènements dans un texte
```
### 5.

6. Produire un texte, à l’oral ou à l’écrit, en tenant compte de l’ordre logique et chronologique

```
Observer et reconnaître, à l’oral ou à l’écrit, les mots ou les groupes de mots utilisés pour reprendre de l’information
(les mots substituts) et assurer la cohérence
```
### 7.

```
a. les pronoms (ex. : Voici papa. Il est très gentil .)
```
```
les synonymes (ex. : Le gros chien jappe tout le temps. Cet énorme chien court
aussi très vite .)
```
```
b.
```
```
les mots génériques/les mots spécifiques (ex. : - Voici des fleurs pour toi maman. -
Qu’elles sont belles ces marguerites! )
```
```
c.
```
```
d. les groupes de mots (ex : la reine de la ruche ou l’abeille )
```
8. Utiliser, en production ou en interaction, orale ou écrite, les mots substituts dans un texte

```
a. des pronoms
```
```
b. des synonymes
```
```
c. des mots génériques/des mots spécifiques
```
```
d. des groupes de mots
```
1. Les catégories choisies reprennent celles présentées dans l’ouvrage suivant : Myriam Laporte et Ginette Rochon,
    _Grammaire Jeunesse_ , Anjou, CEC, 2004, p.157.
2. Pour d’autres exemples de textes littéraires et courants, se référer à la p. 132 du _Québec Education Program_ , sous la
    rubrique _Repères culturels_.
3. Pour plus de détails sur les technologies de l’information et de la communication, se référer à la p. 136 du _Québec_
    _Education Program_.
4. Dans le _Québec Education Program_ (p. 136), on parle d’organisateurs textuels en termes de vocabulaire servant à
    exprimer des relations entre les groupes de mots et les paragraphes.


## Français, langue seconde

## (Programme d'immersion)

## Connaissances liées à la phrase

Le fait d’être exposés de façon intensive à la langue permet aux élèves en immersion d’intérioriser des structures de
phrases et une grammaire implicite. Il s’avère toutefois important d’observer les constituants de la phrase pour bien
comprendre les modalités de fonctionnement propres à la langue française et pour percevoir celle-ci comme un système
organisé.

Afin de mieux saisir le sens de ce qu’ils entendent et lisent, les élèves doivent s’approprier diverses notions grammaticales
au moyen d’activités d’acquisition de connaissances dans le cadre de situations de communication. Ils mobilisent ces
notions pour préciser davantage leurs idées et pour formuler des phrases syntaxiquement correctes dans les textes qu’ils
produisent ou lors de leurs interactions.

Le tableau qui suit contient des connaissances relatives à la phrase et à ses divers éléments (groupe du nom, groupe du
verbe, mots invariables) ainsi qu’aux opérations syntaxiques.

```
Connaissances liées à la phrase
```
```
L’élève apprend à le faire avec l’intervention de l’enseignante ou de l’enseignant.
```
```
L’élève le fait par lui-même à la fin de l’année scolaire.
```
```
L’élève réutilise cette connaissance.
```
```
Primaire
```
```
1 er
cycle
```
```
2 e
cycle
```
```
3 e
cycle
```
```
A. Phrase en tant qu’unité de sens 1 re 2 e 3 e 4 e 5 e 6 e
```
```
Observer globalement et reconnaître les caractéristiques des phrases proposées par
l’enseignant, à l’oral ou à l’écrit (ex. : intonation, pause, majuscule, ponctuation, espace
entre les mots)
```
### 1.

2. Reproduire diverses phrases modélisées, à l’oral ou à l’écrit
3. Observer et reconnaître les différents types^1 et formes^2 de phrases entendus, lus et vus^
4. Produire différents types et formes de phrases pour

```
communiquer un fait, une information ou une opinion (ex. : Le loup dort dans une
tanière. )
```
```
a.
```
```
b. exprimer une émotion, un sentiment ou un jugement (ex. : Quel beau dessin! )
```
```
c. poser une question (ex. : À quelle heure te couches-tu le soir? )
```
```
formuler un ordre, une demande ou un conseil (ex. : N'oublie pas de faire tes
devoirs. )
```
```
d.
```
```
nier un fait, interdire ou refuser (ex. : Il n'est pas permis de mâcher de la gomme en
classe. )
```
```
e.
```
```
B. Éléments^3 de la phrase 1 re 2 e 3 e 4 e 5 e 6 e
Observer et reconnaître l’importance des espaces entre les mots dans une phrase pour
reconnaître chaque mot en tant qu’unité de sens
```
### 1.

2. Observer et reconnaître l’ordre usuel des mots dans une phrase^4^
    Observer et reconnaître certaines classes de mots (nom, verbe, déterminant, adjectif,
    pronom, adverbe, préposition, conjonction) dans une phrase

### 3.

```
Observer et reconnaître, à partir des modèles proposés par l’enseignant, les deux éléments obligatoires de la phrase
de base
```
### 4.

```
a. le groupe du nom (GN)^5^
```
```
b. le groupe du verbe (GV)
```
5. Observer et reconnaître, à partir de modèles proposés par l’enseignant, les fonctions du groupe du nom

```
a. sujet
```

```
b. complément de phrase
```
```
c. complément direct
```
```
Observer et reconnaître, à partir de modèles proposés par l’enseignant, la fonction du
groupe du verbe : prédicat
```
### 6.

```
Observer et reconnaître, à partir de modèles proposés par l’enseignant, les caractéristiques de l’élément facultatif de
la phrase de base : le groupe complément de phrase (GCdeP)
```
### 7.

```
a. précise la phrase de base
```
```
est formé d’un mot (ex. : aujourd’hui ) ou d’un groupe de mots (ex. : dès son arrivée à
l’école )
```
```
b.
```
```
c. est mobile (peut se placer au début, au milieu ou à la fin de la phrase)
```
8. Produire, à l’oral et à l’écrit, des phrases comportant

```
a. un GNS (groupe du nom sujet) et un GV (groupe du verbe)
```
```
b. un GNS, un GV et un GCdeP (groupe complément de phrase)
```
**C. Groupe du nom 1 re 2 e 3 e 4 e 5 e 6 e**

**Le nom**

1. Observer certaines caractéristiques du nom

```
a. désigne une personne, un animal, un objet, un lieu, un végétal
```
```
b. est commun ou propre
```
```
c. a un nombre (singulier ou pluriel)
```
```
d. a un genre (masculin ou féminin)
```
```
e. est donneur de genre et de nombre au déterminant et à l’adjectif
```
```
f. est toujours à la 3e personne du singulier ou du pluriel^
```
```
g. est donneur de personne et de nombre au verbe conjugué
```
2. Observer et reconnaître les règles de formation du pluriel des noms

```
a. ajout d’un -s
```
```
b. ajout d’un -x aux noms se terminant par - au, -eau, -eu et certains -ou
```
```
c. transformation de la finale (- al/-aux )
```
```
d. transformations particulières de certains noms (ex. : œil/yeux, madame/mesdames )
```
3. Observer et reconnaître les règles de formation du féminin des noms

```
a. ajout d’un -e
```
```
transformation (- er/-ère, -eau/-elle, -eux/-euse, -eur/-euse, -teur/-teuse, -teur/-trice,
-f/-ve )
```
```
b.
```
```
doublement de la consonne finale suivie d’un -e (-el/-elle, -an/-anne, -en/-enne,
-on/-onne, -at/-atte, -et/-ette, -ot/-otte )
```
```
c.
```
```
transformations particulières de certains noms (ex. : compagnon/compagne,
copain/copine )
```
```
d.
```
```
changement complet du nom (ex : frère/sœur, coq/poule, roi/reine, tante/oncle,
homme/femme, garçon/fille )
```
```
e.
```
4. Appliquer certaines règles de formation des noms

```
a. pour le pluriel
```

```
b. pour le féminin
```
**Le déterminant**

5. Observer certaines caractéristiques du déterminant

```
a. se place devant le nom
```
```
b. reçoit le genre et le nombre du nom
```
6. Observer et reconnaître les sortes de déterminants suivants

```
a. déterminant article
```
```
b. déterminant numéral
```
```
c. déterminant démonstratif
```
```
d. déterminant possessif
```
7. Utiliser un déterminant devant le nom dans les phrases produites à l’oral et à l’écrit
8. Accorder le déterminant avec le nom

**L’adjectif**

9. Observer certaines caractéristiques de l’adjectif

```
a. sert à décrire ou à préciser un nom ou un pronom
```
```
b. se place avant ou après le nom et après un verbe d’état^6^
```
```
c. reçoit le genre et le nombre du nom et du pronom
```
10. Observer et reconnaître les règles de formation du pluriel des adjectifs

a. ajout d’un _-s_ (^)
b. ajout d’un _-x_ aux adjectifs se terminant par _-eau, -au, -eu_
c. transformation de la finale ( _-al/-aux_ )

11. Observer et reconnaître les règles de formation du féminin des adjectifs

```
a. ajout d’un -e
```
```
transformation (- er/-ère, -eux/-euse, -eur/-euse, -teur/-teuse, -teur/-trice, -f/-ve,
-c/-che )
```
```
b.
```
```
doublement de la consonne finale suivie d’un -e (- el/-elle, -eil/-eille, -ul/-ulle, -il/-ille,
-en/-enne, -on/-onne, -ot/-otte, -et/-ette, ‑ s/-sse )
```
```
c.
```
```
transformations particulières de certains adjectifs (ex. : malin/maligne, vieux/vieille,
long/longue, fou/folle )
```
```
d.
```
12. Utiliser des adjectifs dans les phrases produites à l’oral et à l’écrit
13. Appliquer certaines règles d’accord des adjectifs

```
a. pour le pluriel
```
```
b. pour le féminin
```
**Le pronom**

14. Nommer les pronoms personnels sujets ( _je, tu, il, elle, nous, vous, ils, elles_ )
15. Observer certaines caractéristiques du pronom personnel sujet


```
a. remplace un nom ou un groupe du nom
```
```
b. se place devant le verbe dans une phrase déclarative
```
```
c. prend le genre et le nombre du nom qu’il remplace
```
```
d. se place après le verbe, relié par un trait d’union dans une phrase interrogative
```
16. Identifier le nom ou le groupe du nom que le pronom personnel sujet remplace
17. Observer et reconnaître la personne et le nombre des pronoms personnels sujets
18. Observer et reconnaître que le pronom personnel qui occupe la fonction sujet

```
a. donne sa personne et son nombre au verbe conjugué
```
```
donne son genre et son nombre au participe passé employé avec être et à l’adjectif
attribut du sujet
```
```
b.
```
19. Utiliser les pronoms personnels sujets dans les phrases produites à l’oral et à l’écrit

```
a. je , tu
```
```
b. il , elle , ils , elles
```
```
c. nous, vous (sens pluriel)
```
```
d. vous (forme de politesse^7 )^^
Observer et reconnaître d’autres pronoms personnels courants (ex. : on, eux, me, lui, le,
la, y )
```
### 20.

21. Utiliser des pronoms personnels courants dans les phrases produites à l’oral et à l’écrit

```
D. Groupe du verbe 1 re 2 e 3 e 4 e 5 e 6 e
```
**Le verbe**

1. Observer certaines caractéristiques du verbe

```
a. exprime une action ou un état
```
```
b. est précédé d’un pronom ou d’un groupe du nom
```
```
c. permet de situer un événement dans le temps
```
```
d. est formé d’un radical et d’une terminaison
```
```
e. reçoit la personne et le nombre du sujet
```
```
Mémoriser, sans découverte du système de conjugaison, les formes à l’indicatif présent
des verbes fréquemment utilisés (ex. : j’ai, tu es, il aime, nous allons, vous faites, ils
peuvent )
```
### 2.

3. Nommer le verbe à l’infinitif présent

```
a. pour trouver sa signification dans le dictionnaire
```
```
b. pour trouver son modèle de conjugaison
```
```
Observer et identifier certains temps de verbe utilisés pour exprimer le passé, le présent et
l’avenir
```
### 4.

```
Observer et reconnaître les terminaisons de différents temps de verbes réguliers en -er et en -ir (ayant un participe
présent en - issant ) fréquemment utilisés
```
### 5.

```
a. indicatif présent, passé composé
```
```
b. imparfait, futur proche, futur simple
```
```
c. conditionnel présent, impératif présent
```

6. Mémoriser les terminaisons propres aux différents temps de verbes réguliers

```
a. indicatif présent, passé composé
```
```
b. imparfait, futur proche, futur simple
```
```
c. conditionnel présent, impératif présent
```
```
Observer et reconnaître les variations du radical des verbes irréguliers fréquemment utilisés en classe (ex. : avoir,
être, aller, prendre, recevoir, partir, dire, faire, vouloir, mettre ) à différents temps de verbe
```
### 7.

```
a. indicatif présent, passé composé
```
```
b. imparfait, futur proche, futur simple
```
```
c. conditionnel présent, impératif présent
```
```
Mémoriser les terminaisons propres aux différents temps de verbes irréguliers fréquents (ex. : avoir, être, aller,
prendre, recevoir, partir, dire, faire, vouloir, mettre )
```
### 8.

```
a. indicatif présent, passé composé
```
```
b. imparfait, futur proche, futur simple
```
```
c. conditionnel présent, impératif présent
```
```
Observer et reconnaître que le verbe avoir est utilisé comme auxiliaire au passé composé
pour la plupart des verbes
```
### 9.

```
Observer et reconnaître que le verbe être est utilisé comme auxiliaire au passé composé
pour certains verbes (ex. : partir, sortir, venir )
```
### 10.

```
Observer et reconnaître le temps de conjugaison d’un verbe à partir de la terminaison
dans les textes lus, vus et entendus
```
### 11.

```
Identifier le sujet du verbe conjugué pour l’accorder en personne et en nombre (ex. : Pierre
et Marie parlent )
```
### 12.

13. Appliquer la règle d’accord du verbe avec le sujet dans ses productions à l’oral et à l’écrit

```
Appliquer, à partir des modèles connus, la conjugaison propre au temps de verbe utilisé
dans ses productions, à l’oral et à l’écrit
```
### 14.

```
E. Mots invariables 1 re 2 e 3 e 4 e 5 e 6 e
```
```
Observer que l’adverbe, la préposition et la conjonction sont invariables en genre et en
nombre
```
### 1.

```
Utiliser des adverbes, des prépositions et des conjonctions dans les phrases produites à
l’oral et à l’écrit
```
### 2.

```
F. Opérations syntaxiques 1 re 2 e 3 e 4 e 5 e 6 e
```
```
Observer l’enseignant qui transforme la phrase, à l’oral et à l’écrit, en encadrant, en effaçant, en ajoutant, en
remplaçant ou en déplaçant des mots ou des groupes de mots
```
### 1.

```
a. pour identifier les classes de mots dans une phrase
```
```
i. un nom commun (ajout d’un déterminant, ajout d’un adjectif)
```
```
un déterminant (remplacement d’un déterminant par un autre déterminant
connu, ajout d’un nom après le déterminant)
```
```
ii.
```
```
un adjectif (remplacement d’un adjectif par un autre, ajout de très devant un
adjectif)
```
```
iii.
```
```
un verbe (encadrement du verbe par n’/ne... pas , ajout d’un pronom de
conjugaison devant le verbe, remplacement du verbe par le même verbe
conjugué à un temps simple)
```
```
iv.
```
```
v. un adverbe (ex. : remplacement par un autre adverbe)
```
```
pour identifier le sujet (GNS) dans une phrase (ex. : encadrement du sujet par c’est...
qui )
```
```
b.
```
```
pour identifier le GCdeP dans une phrase (ex. : effacement ou déplacement du
GCdeP)
```
```
c.
```
```
pour enrichir ou simplifier une phrase (ex. : ajout d’un adjectif, déplacement du
GCdeP)
```
```
d.
```

```
pour transformer la phrase déclarative en d’autres types ou formes de phrases (ex. :
le trait d’union dans la phrase interrogative, le n’/ne... pas dans la phrase négative)
```
```
e.
```
2. Effectuer quelques manipulations

```
a. pour repérer les classes de mots
```
```
b. pour identifier un GNS ou un GCdeP
```
```
c. pour enrichir ou simplifier une phrase
```
```
d. pour transformer la phrase déclarative
```
1. Les types de phrases sont les suivants : déclaratif, interrogatif, impératif et exclamatif.
2. Les formes de phrases sont les suivantes : positive et négative.
3. Il est à noter que dans la nouvelle grammaire, on utilise l’expression c _onstituants de la phrase._
4. Dans le _Québec Education Program_ (p. 135), il est question de l’ordre usuel des mots ou des groupes de mots dans la
    phrase, dans la sous-section _Groupe du nom_.
5. Selon la nouvelle grammaire, le groupe du nom, lorsqu’il est sujet du groupe verbe, peut s’appeler _groupe nominal sujet_
    _(GNS)._
6. Il est à noter que dans la nouvelle grammaire, on utilise l’expression _verbe attributif_.
7. Il importe d’exposer les élèves le plus rapidement possible à l’utilisation appropriée du _vous_ de politesse et du _tu_ familier.
    De plus, il est fortement recommandé à l'enseignant d'employer le _vous_ collectif en s’adressant à l’ensemble des élèves
    dans la classe pour éviter qu’ils fassent un emploi erroné du _tu_ et du _vous_ dans d’autres contextes.


