# Progression of Learning

[MELS Source](http://www.education.gouv.qc.ca/fileadmin/site_web/documents/education/jeunes/pfeq/PDA_PFEQ_mathematique-primaire_2009_EN.pdf)

# Mathematics

## August 24, 2009

## Table of Contents

- Introduction
- Arithmetic
   - Understanding and writing numbers
   - Meaning of operations involving numbers
   - Operations inv olv ing numbers
- Geometry
- Measurement
- Statistics
- Probability
- Examples of strategies

## Introduction

Numeracy, which encompasses all of the mathematical knowledge and skills an individual needs in order to function in society, is a goal that all students should achieve, no matter what path they may choose to follow in school. It can be attained through effective, controlled use of all the mathematical concepts set forth in the Québec Education Program.

This document is complementary to the mathematics program. It provides additional information on the knowledge and skills students must acquire throughout elementary school with respect to arithmetic, geometry, measurement, statistics and probability. Each of these branches is dealt with in a separate section that covers, for every year of elementary school, the knowledge to be acquired as well as the actions to be performed in order for students to fully assimilate the concepts presented. Each section consists of an introduction, which provides an overview of the progression of learning, and content tables, which illustrate the mathematical symbols and vocabulary to be introduced as students progress in their learning. This document should therefore help teachers with their lesson planning.

Because mathematics is a science that involves abstract concepts and language, students develop their mathematical thinking gradually through personal experience and exchanges with peers. Their learning is based on situations that are often drawn from everyday life. Thus, by participating in learning activities that encourage them to reflect, manipulate, explore, construct, simulate, discuss, structure and practise, students assimilate concepts, processes and strategies. These activities allow students to use objects, manipulatives, references and various tools and instruments. They also enable students to rely on their intuition, sense of observation, manual skills and ability to express themselves, reflect and analyze—actions that are essential to the development of competencies. By making connections, visualizing mathematical objects in different ways and organizing them in their minds, students gradually develop their understanding of abstract mathematical concepts.

In this way, students build a set of tools that will allow them to communicate appropriately using mathematical language, reason effectively by making connections between mathematical concepts and processes, and solve situational problems. By using mathematical concepts and various strategies, students can make informed decisions in all areas of life. Combined with learning activities, the situations experienced by students promote the development of mathematical skills and attitudes that allow them to mobilize, consolidate and broaden their mathematical knowledge.

* [Homeschool Pop: Roman Numerals](https://www.youtube.com/watch?v=NrnXsKYpLJM&list=PLNmq_T1B9ljJcradU4Xhec5nU52cB_klf&index=8&t=0s)
* [Khan Academy: Math By Grade - K to 8](https://www.khanacademy.org/math/k-8-grades)


## Arithmetic

The concepts and processes to be acquired and mastered in arithmetic constitute the building blocks of mathematics, since they are applied in all other branches of this subject.

The learning content in arithmetic is divided into three sections: understanding and writing numbers, meaning of operations involving numbers, and operations involving numbers.

- Understanding and writing numbers
- Meaning of operations involving numbers
- Operations involving numbers

### Understanding and writing numbers

Number sense is a concept that is developed in early childhood and is refined as students progress through school. In elementary school, it is developed first by looking at natural numbers and then enriched by studying rational numbers.1

At the outset, counting rhymes, counting, constructions, representations, ordering and establishing relationships among numbers are essential in order for students to understand number systems. Using appropriate manipulatives, students first learn about counting groups (grouping) and gradually replace this concept with place value. However, care must be taken not to progress too quickly from one concept to another, as this could affect the way students understand operations or learn new numbers.

It is in elementary school that students acquire the basic tools for understanding and using fractions. Students must first understand concepts (meaning) before they can understand calculation processes (operations). This can be achieved by allowing students to systematically use concrete materials and pictorial representations when dealing with situations involving fractions.

The table below presents the learning content associated with understanding and writing numbers. The concepts and processes targeted will provide students with increasingly complex tools that will help them develop and use all three mathematics competencies.

1. Natural numbers less than...
    1. Counts or recites counting rhymes involving natural numbers
        1. counts forward from a given number
        2. counts forward or backward
        3. skip counts (e.g. by twos)
    2. Counts collections (using objects or drawings)
        1. matches the gesture to the corresponding number word; recognizes the cardinal aspect of a number and the conservation of number in various combinations
        1. counts from a given number
        1. counts a collection by grouping or regrouping
        1. counts a pre-grouped collection
    3. Reads and writes any natural number
    4. Represents natural numbers in different ways or associates a number with a set of objects or drawings
        1. emphasis on apparent, accessible groupings using objects, drawings or unstructured materials (e.g. tokens, nesting cubes, groups of ten objects placed inside a bag and ten of these bags placed inside another container)
        1. emphasis on exchanging apparent, non-accessible groupings, using structured materials (e.g. base ten blocks, number tables)
        1. emphasis on place value in non-apparent, non-accessible groupings, using materials for which groupings are symbolic (e.g. abacus, money)
    5. Composes and decomposes a natural number in a variety of ways
        ```
        (e.g. 123 = 100 + 23
        123 = 100 + 20 + 3
        123 = 50 + 50 + 20 + 3
        123 = 2 × 50 + 30 − 7
        123 = 2 × 60 + 3)
        ```
    6. Identifies equivalent expressions
        ```
        (e.g. 52 = 40 + 12, 25 + 27 = 40 + 12, 52 = 104 ÷ 2)
        ```
    7. Compares natural numbers
    8. Arranges natural numbers in increasing or decreasing order
    9. Describes number patterns, using his/her own words and appropriate mathematical vocabulary (e.g. even numbers, odd numbers, square numbers, triangular numbers, prime numbers, composite numbers)

    10. Locates natural numbers using different visual aids
        (e.g. hundreds chart, number strip, number line)

    11. Identifies properties of natural numbers
        1. odd or even numbers
        1. square, prime or composite numbers
    12. Classifies natural numbers in various ways, based on their properties
        (e.g. even numbers, composite numbers)

    13. Approximates a collection, using objects or drawings
        (e.g. estimate, round up/down to a given value)


    14. Represents the power of a natural number

        1. Vocabulary
        Grouping, digit, number, unit, tens place, hundreds place
        Natural number, even number, odd number
        Is equal to, is bigger than (is greater than) ; is smaller than (is less than )
        Increasing order, decreasing order
        Number line
        Symbols
        0 to 9, <, >, =, numbers written using digits
        1. Vocabulary
        Base ten, position, place value, thousand, thousands place, ten thousands
        Is not equal to ; is greater than ; is less than
        Square number, composite number, prime number
        Symbols
        ≠, numbers written using digits
        1. Vocabulary
        Hundred thousands, million
        Exponent, power, squared, cubed
        Parenthesis
        Symbols
        ( ), numbers written using digits, exponential notation
1. Fractions (using objects or drawings)
    1. Identifies fractions related to everyday items (using objects or drawings)
    1. Represents a fraction in a variety of ways, based on a whole or a collection of objects
    1. Matches a fraction to part of a whole (congruent or equivalent parts) or part of a group of objects, and vice versa
    4. Identifies the different meanings of fractions (sharing, division, ratio)
    5. Distinguishes a numerator from a denominator
    6. Reads and writes a fraction
    7. Compares a fraction to 0, ½ or 1
    8. Verifies whether two fractions are equivalent
    9. Matches a decimal or percentage to a fraction
    10. Orders fractions with the same denominator
    11. Orders fractions where one denominator is a multiple of the other(s)
    12. Orders fractions with the same numerator
    13. Locates fractions on a number line

    #### Vocabulary
    - Fraction, half, one third, one quarter
    - Numerator, denominator
    - Whole, equivalent part, equivalent fraction

    #### Symbol
    - Fractional notation

1. Decimals up to... `hundredths & thousandths`
    1. Represents decimals in a variety of ways (using objects or drawings)
    2. Identifies equivalent representations (using objects or drawings)
    3. Reads and writes numbers written in decimal notation
    4. Understands the role of the decimal point
    5. Composes and decomposes a decimal written in decimal notation

    1. Recognizes equivalent expressions
        (e.g. 12 tenths is equivalent to 1 unit and 2 tenths; 0.5 is equivalent to 0.50)
    7. Locates decimals on a number line
        1. between two consecutive natural numbers
        1. between two decimals
    8. Compares two decimals

    1. Approximates

        (e.g. estimates, rounds to a given value, truncates decimal places)
    10. Arranges decimals in increasing or decreasing order
    11. Matches
        1. a fraction to its decimal
        1. a fraction or percentage to its decimal

    ### Vocabulary
    Decimal, tenth, hundredth
    ### Symbol
    Decimal notation
    ### Vocabulary
    Thousandth
    ### Symbol
    Decimal notation

1. Integers
    1. Represents integers in a variety of ways (using objects or drawings)

        (e.g. tokens in two different colours, number line, thermometer, football field, elevator, hot air balloon)
    2. Reads and writes integers
    3. Locates integers on a number line or Cartesian plane
    4. Compares integers
    5. Arranges integers in increasing or decreasing order

    ### Vocabulary
    Integer
    Negative number, positive number
    ### Symbols
    Integer notation, +/– calculator key


### Meaning of operations involving numbers

In order to fully understand operations and their different meanings in various contexts, students must understand the relationships among data and among operations, and choose and perform the correct operations, taking into account the properties and order of operations. Students must also have a general idea of the result expected.

Students will thus be encouraged to use concrete, semi-concrete or symbolic means to mathematize a variety of situations illustrating different meanings. In these situations, students will learn to break problems down into simpler ones and identify the relationships among data that will help them to arrive at a solution. Since operation sense is developed at the same time as number sense, the two should be taught concurrently.

The table below presents the learning content associated with the meaning of operations involving numbers. The concepts and processes targeted will provide students with increasingly complex tools that will help them develop and use all three mathematics competencies.

1. Natural numbers less than...
    1. Determines the operation(s) to perform in a given situation

    1. Uses objects, diagrams or equations to represent a situation and conversely, describes a situation represented by objects, diagrams or equations (use of different meanings of addition and subtraction)

        1. transformation (adding, taking away), uniting, comparing
        1. composition of transformations: positive, negative
        1. composition of mixed transformations
    1. Uses objects, diagrams or equations to represent a situation and conversely, describes a situation represented by objects, diagrams or equations (use of different meanings of multiplication and division)
        1. rectangular arrays, repeated addition, Cartesian product, sharing, and number of times x goes into y (using objects and diagrams) a. rectangular arrays, repeated addition, Cartesian product, area, volume, repeated subtraction, sharing, number of times x goes into y, and comparisons (using objects, diagrams or equations) b.
    4. Establishes equality relations between numerical expressions (e.g. 3 + 2 = 6 – 1)
    5. Determines numerical equivalencies using relationships between
        1. operations (addition and subtraction) and the commutative property of addition
        1. operations (the four operations), the commutative property of addition and multiplication and the associative property
        1. operations (the four operations), the commutative property of addition and multiplication, the associative property and the distributive property of multiplication over addition or subtraction
    1. Translates a situation using a series of operations in accordance with the order of operations

    #### Vocabulary
    Plus, minus, less, more
    Addition, subtraction, sum, difference

    #### Symbols
    +, –
    #### Vocabulary
    At least, at most, term, missing term
    Multiplication, factor, product
    Division, divisor, dividend, quotient, remainder, sharing
    Equality, inequality, equation, inverse operation, multiple
    #### Symbols
    ×, ÷
1. Decimals up to... `hundredths thousandths`
    1. Uses objects, diagrams or equations to represent a situation and conversely, describes a situation represented by objects, diagrams or equations (use of different meanings of addition and subtraction)
        1. transformation (adding, taking away), uniting, comparing
        1. composition of transformations: positive, negative
        1. composition of mixed transformations
    1. Uses objects, diagrams or equations to represent a situation and conversely, describes a situation represented by objects, diagrams or equations (use of different meanings of multiplication and division: rectangular arrays, Cartesian product, area, volume, sharing, number of times x goes into y, and comparisons)

    3. Determines numerical equivalencies using
        1. the relationship between operations (addition and subtraction), the commutative property of addition and the associative property
        1. relationships between operations (the four operations), the commutative property of addition and multiplication, the associative property and the distributive property of multiplication over addition or subtraction
    1. Translates a situation into a series of operations in accordance with the order of operations

1. Fractions

    1. Uses objects, diagrams or equations to represent a situation and conversely, describes a situation represented by objects, diagrams or equations
    (use of different meanings of addition, subtraction and multiplication by a natural number)


### Operations involving numbers

As students gradually develop their number and operation sense, they will be called upon to develop their own processes and adopt conventional ones in order to perform various operations. They will learn to recognize equivalencies between these different processes and to develop certain automatic responses. Using these processes and the properties of operations, they will also learn to estimate results and obtain accurate results using mental and written computation.

The situations presented should involve numerical and non-numerical patterns (e.g. colours, shapes, sounds) to allow students to observe and describe various patterns and series of numbers and operations, such as a sequences of even numbers, multiples of 5 and triangular numbers. These situations will also require students to add terms to a series, state general rules or build models. Thus, students will learn to formulate or deduce definitions, properties and rules.

In all cycles, calculators may be used to good advantage as a calculation, verification and learning tool (e.g. in situations involving patterns, number decomposition, or the order of operations).

The table below presents the learning content associated with operations involving numbers. The concepts and processes targeted will provide students with increasingly complex tools that will help them develop and use all three mathematics competencies.

1. Natural numbers
    1. Approximates the result of
        1. an addition or subtraction involving natural numbers
        1. any of the four operations involving natural numbers
    2. Builds a repertoire of memorized addition and subtraction facts1

    1. Builds a memory of addition facts2 (0 + 0 to 10 + 10) and the corresponding subtraction facts, using objects, drawings, charts or tables
        1. Develops various strategies that promote mastery of number facts and relates them to the properties of addition
        1. Masters all addition facts (0 + 0 to 10 + 10) and the corresponding subtraction facts
    3. Develops processes for mental computation
        1. Uses his/her own processes to determine the sum or difference of two natural numbers
        1. Uses his/her own processes to determine the product or quotient of two natural numbers
    4. Develops processes for written computation (addition and subtraction)
        1. Uses his/her own processes as well as objects and drawings to determine the sum or difference of two natural numbers less than 1000
        1. Uses conventional processes to determine the sum of two natural numbers of up to four digits
        1. Uses conventional processes to determine the difference between two natural numbers of up to four digits whose result is greater than 0

    1. Determines the missing term in an equation (relationships between operations):

        `a + b = □, a + □ = c , □ + b = c, a – b = □, a – □ = c , □ – b = c`

    6. Builds a repertoire of memorized multiplication and division facts

        1. Builds a memory of multiplication facts (0 × 0 to 10 × 10) and the corresponding division facts, using objects, drawings, charts or tables
        1. Develops various strategies that promote mastery of number facts and relate them to the properties of multiplication
        1. Masters all multiplication facts (0 × 0 to 10 × 10) and the corresponding division facts
    7. Develops processes for written computation (multiplication and division)
        1. Uses his/her own processes as well as materials and drawings to determine the product or quotient of a three-digit natural number and a one-digit natural number, expresses the remainder of a division as a fraction, depending on the context
        1. Uses conventional processes to determine the product of a three-digit natural number and a two-digit natural number
        1. Uses conventional processes to determine the quotient of a four-digit natural number and a two-digit natural number, expresses the remainder of a division as a decimal that does not go beyond the second decimal place

    1. Determines the missing term in an equation (relationships between operations):
    `a × b = □, a × □ = c , □ × b = c, a ÷ b = □, a ÷ □ = c , □ ÷ b = c`
    9. Decomposes a number into prime factors
    10. Calculates the power of a number
    11. Determines the divisibility of a number by 2, 3, 4, 5, 6, 8, 9, 10
    12. Performs a series of operations in accordance with the order of operations
    13. Using his/her own words and mathematical language that is at an appropriate level for the cycle, describes

        1. non-numerical patterns (e.g. series of colours, shapes, sounds, gestures)
        1. numerical patterns (e.g. number rhymes, tables and charts)
        1. series of numbers and family of operations
    14. Adds new terms to a series when the first three terms or more are given
    15. Uses a calculator and

        1. becomes familiar with its basic functions (+, –, =, 0 to 9 number keys, all clear, clear)
        1. becomes familiar with its × and ÷ functions
        1. becomes familiar with memory keys and change of sign keys (+/–)
    #### Vocabulary
    Pattern, series
    #### Symbols
    Calculator keys
1. Fractions (using objects or diagrams)
    1. Generates a set of equivalent fractions
    2. Reduces a fraction to its simplest form (lowest terms)
    1. Adds and subtracts fractions when the denominator of one fraction is a multiple of the other fraction(s)
    4. Multiplies a natural number by a fraction

    #### Vocabulary
    Irreducible fraction

1. Decimals
    1. Approximates the result of

        1. an addition or a subtraction
        1. a multiplication or division
    2. Develops processes for mental computation

        1. adds and subtracts decimals
        1. performs operations involving decimals (multiplication, division by a natural number)
        1. multiplies and divides by 10, 100, 1000
    3. Develops processes for written computation
        1.  adds and subtracts decimals whose result does not go beyond the second decimal place
        1. multiplies decimals whose product does not go beyond the second decimal place
        1. divides a decimal by a natural number less than 11
    #### Symbols
    $, ¢

1. Using Numbers
    1. Expresses a decimal as a fraction, and vice versa
    2. Expresses a decimal as a percentage, and vice versa
    3. Expresses a fraction as a percentage, and vice versa
    4. Chooses an appropriate number form for a given context

## Geometry

Before they enter preschool, children explore the shapes of objects in their surroundings and begin to understand basic topological concepts such as inside-outside, above-below; they also acquire the rudiments of spatial sense. In preschool, they begin to organize space and establish relationships between objects by comparing, classifying and grouping them.

Throughout elementary school, by participating in activities and manipulating objects, students acquire the vocabulary of geometry and learn to get their bearings in space, identify plane figures and solids, describe categories of figures and observe their properties. Geometry in elementary school focuses on two-dimensional (plane) and three-dimensional figures and on key concepts, such as the ability to locate objects in space and observe their geometric and topological properties.

Knowledge of vocabulary is not enough; the words must be closely tied to precise concepts such as shape, similarity, dissimilarity, congruency and symmetry. Thus, the use of varied activities and a wide range of objects is essential for students to develop spatial sense and geometric thought. This will allow students to progress from the concrete to the abstract, first by manipulating and observing objects, then by making various representations, and finally by creating mental images of figures and their properties.

The ability to discern and recognize the properties of a geometric object or a category of objects must be developed before students can learn about the relationships among elements in a figure or among distinct figures. It is also required in order to develop the ability to identify new properties and use known or new properties in problem solving.

The table below presents the learning content associated with geometry. The concepts and processes targeted will provide students with increasingly complex tools that will help them develop and use all three mathematics competencies.

1. Space
    1. Gets his/her bearings and locates objects in space (spatial relationships)
    2. Locates objects in a plane
    3. Locates objects on an axis (based on the types of numbers studied)
    4. Locates points in a Cartesian plane

        1. in the first quadrant
        1. in all four quadrants

    #### Vocabulary
    Reference system, plane, Cartesian plane, ordered pair
    #### Symbols
    Writing ordered pairs ( a , b )

1. Solids
    1. Compares objects or parts of objects in the environment with solids
        (e.g. spheres, cones, cubes, cylinders, prisms, pyramids)

    1. Compares and constructs solids
        (e.g. spheres, cones, cubes, cylinders, prisms, pyramids)


    1. Identifies the main solids
        (e.g. spheres, cones, cubes, cylinders, prisms, pyramids)


    #### Vocabulary
        Solid, base of a solid, face, flat surface, curved surface
        Sphere, cone, cube, cylinder, prism, pyramid

    4. Identifies and represents the different faces of a prism or pyramid

    5. Describes prisms and pyramids in terms of faces, vertices and edges
    6. Classifies prisms and pyramids
    7. Constructs a net of a prism or pyramid
    8. Matches the net of
        1. a prism to the corresponding prism and vice versa
        1. a pyramid to the corresponding pyramid and vice versa
        1. a convex polyhedron to the corresponding convex polyhedron
    #### Vocabulary
        Vertex, edge, net of a solid

    9. Tests Euler’s theorem on convex polyhedrons

    #### Vocabulary
        Polyhedron, convex polyhedron

1. Plane figures
    1. Compares and constructs figures made with closed curved lines or closed straight lines
    2. Identifies plane figures (square, rectangle, triangle, rhombus and circle)
    3. Describes plane figures (square, rectangle, triangle and rhombus)

    #### Vocabulary
        Straight line, closed straight line, curved line
        Plane figure, side
        Square, circle, rectangle, triangle, rhombus

    4. Describes convex and nonconvex polygons
    5. Identifies and constructs parallel lines and perpendicular lines

    6. Describes quadrilaterals (e.g. parallel segments, perpendicular segments, right angles, acute angles, obtuse angles)

    7. Classifies quadrilaterals

    #### Vocabulary
        Quadrilateral, parallelogram, trapezoid, polygon
        Convex polygon, nonconvex polygon, segment
        Is parallel to.. ; is perpendicular to...
    #### Symbols
        //, ⊥

    8. Describes triangles: scalene triangles, right triangles, isosceles triangles, equilateral triangles

    9. Classifies triangles
    10. Describes circles

    #### Vocabulary
        Equilateral triangles, isosceles triangle, right triangle, scalene triangle
        Circle, central angle, diameter, radius, circumference

1. Frieze patterns and tessellations
    1. Identifies congruent figures

    2. Observes and produces patterns using geometric figures
    3. Observes and produces frieze patterns and tessellations
        1. using reflections
        1. using translations
    #### Vocabulary
        Frieze pattern, tesselation
        Reflection, line of reflection, symmetric figure
    #### Vocabulary
        Translation, translation arrow

## Measurement

Before they enter preschool, children have acquired the rudiments of measurement in that they have begun to evaluate and compare size. In preschool, they begin to measure things using instruments such as a rope or growth chart.

Establishing a relationship between two geometric figures means recognizing similar shapes or identical measurements
(congruence) but also realizing that a figure can fit inside another repeatedly to completely cover it (tessellation, measurement). Measuring therefore involves much more than merely taking a reading on an instrument. Measurement sense is developed by making comparisons and estimates, using a variety of conventional and unconventional units of measure. To develop their sense of measuring (of time, mass, capacity, temperature, angles, length, area and volume), students must participate in activities that allow them to design and build instruments, to use invented and conventional measuring instruments and to manipulate conventional units of measure. They must learn to calculate direct measurements (e.g. calculate a perimeter or area, graduate a ruler) and indirect measurements (e.g. read a scale drawing, make a scale drawing, measure the area of a figure by decomposing it, calculate the thickness of a sheet of paper when the thickness of several sheets is known).

The table below presents the learning content associated with measurement. The concepts and processes targeted will provide students with increasingly complex tools that will help them develop and use all three mathematics competencies.


1. Lengths
    1. Compares lengths
    2. Constructs rulers
    3. Estimates and measures the dimensions of an object using unconventional units
    4. Estimates and measures the dimensions of an object using conventional units

        1. metre, decimetre and centimetre
        1. metre, decimetre, centimetre and millimetre
        1. metre, decimetre, centimetre, millimetre and kilometre
    5. Establishes relationships between units of measure for length

        1. metre, decimetre, centimetre and millimetre
        1. metre, decimetre, centimetre, millimetre and kilometre
    6. Calculates the perimeter of plane figures

    #### Vocabulary
        Width, length, height, depth
        Unit of measure, centimetre, decimetre, metre
    #### Symbols
        m, dm, cm
    #### Vocabulary
        Perimeter, millimetre
    #### Symbol
        mm
    #### Vocabulary
        Kilometre
    #### Symbol
        km
1. Surface areas

    1. Estimates and measures surface area

        1. using unconventional units
        1. using conventional units
    #### Vocabulary
        Surface, area
    #### Vocabulary
        Square centimetre, square decimetre, square metre
    #### Symbol
        s
        m^2 , dm^2 , cm^2
1. Volumes

    1. Estimates and measures volume

        1. using unconventional units
        1. using conventional units
    #### Vocabulary
        Volume
    #### Vocabulary
        Cubic centimetre, cubic decimetre, cubic metre
    #### Symbols
        m^3 , dm^3 , cm^3
1. Angles

    1. Compares angles

    #### Vocabulary
        Angle, right angle, acute angle, obtuse angle

    2. Estimates and determines the degree measurement of angles

    #### Vocabulary
        Degree, protractor
    #### Symbols
        ∠, °
1. Capacities
    1. Estimates and measures capacity using unconventional units
    2. Estimates and measures capacity using conventional units

    3. Establishes relationships between units of measure
        (e.g. : 1 L = 1000 mL, ½ L = 500 mL)
    #### Vocabulary
        Capacity, litre, millilitre
    #### Symbols
        L, mL

1. Masses
    1. Estimates and measures mass using unconventional units
    2. Estimates and measures mass using conventional units

    3. Establishes relationships between units of measure
        (e.g. : 1 kg = 1000 g, ½ kg = 500 g)

    #### Vocabulary
        Mass, gram, kilogram
    #### Symbols
        g, kg
1. Time

    1. Estimates and measures time using conventional units
    2. Establishes relationships between units of measure

    #### Vocabulary
        Day, hour, minute, second
    #### Symbols
        h, min, s, representation of time: 3 h, 3 h 25 min, 03:25, 3:25 a.m.
    #### Vocabulary
        Daily cycle, weekly cycle, yearly cycle
1. Temperatures
    1. Estimates and measures temperature using conventional units

    #### Vocabulary
        Degree Celsius
    #### Symbol
        °C

## Statistics

Throughout elementary school, students participate in conducting surveys to answer questions and draw conclusions.
They learn to formulate different types of questions, determine categories or answer choices, plan and carry out data collection and organize data in tables. To develop statistical thinking, students are thus introduced to descriptive statistics, which allow them to summarize raw data in a clear and reliable (rigorous) way.

By participating in the activities suggested, students will learn to display data using tables, horizontal and vertical bar graphs, pictographs or broken-line graphs, depending on the type of data used. They will also learn to interpret data by observing its distribution (e.g. range, centre, groupings) or by comparing data in a given table or graph. They will ask themselves questions as they compare different questions, samples chosen, the data obtained and their different
 representations. They will also have the opportunity to interpret circle graphs1 and develop an understanding of the arithmetic mean in order to be able to calculate it.

The table below presents the learning content associated with statistics. The concepts and processes targeted will provide students with increasingly complex tools that will help them develop and use all three mathematics competencies.

1. Statistics

    1. Formulates questions for a survey (based on age-appropriate topics, students’ language level, etc.)

    2. Collects, describes and organizes data (classifies or categorizes) using tables
    3. Interprets data using
        1. a table, a bar graph and a pictograph
        1. a table, a bar graph, a pictograph and a broken-line graph
        1. a table, a bar graph, a pictograph, a broken-line graph and a circle graph
    4. Displays data using

        1. a table, a bar graph and a pictograph
        1. a table, a bar graph, a pictograph and a broken-line graph
    5. Understands and calculates the arithmetic mean

    #### Vocabulaire
        Survey, table
        Bar graph, pictograph
    #### Vocabulaire
        Broken-line graph
    #### Vocabulaire
        Circle graph, arithmetic mean

## Probability

When attempting to determine the probability of an event, students in elementary school spontaneously rely on intuitive, yet often arbitrary, reasoning. Their predictions may be based on emotions, which may cause them to wish for a predicted outcome or to refute actual results. The classroom activities suggested should help foster probabilistic reasoning. This implies taking into account the uncertainty of outcomes, which may represent a challenge of sorts, since students will tend
 to determine outcomes by looking for patterns or expecting outcomes to balance out.1

In elementary school, students observe and conduct experiments involving chance. They use qualitative reasoning to practise predicting outcomes by becoming familiar with concepts of certainty, possibility and impossibility. They also practise comparing experiments to determine events that are more likely, just as likely and less likely to occur. They list the outcomes of a random experiment using tables or tree diagrams and use quantitative reasoning to compare the actual frequency of outcomes with known theoretical probabilities.

The table below presents the learning content associated with probability. The concepts and processes targeted will provide students with increasingly complex tools that will help them develop and use all three mathematics competencies.

1. Probability

    1. When applicable, recognizes variability in possible outcomes (uncertainty)
    2. When applicable, recognizes equiprobability (e.g. quantity, symmetry of an object [cube])
    3. When applicable, becomes aware of the independence of events in an experiment

    4. Experiments with activities involving chance, using various objects
        (e.g. spinners, rectangular prisms, glasses, marbles, thumb tacks, 6-, 8- or 12-sided dice)

    5. Predicts qualitatively an outcome or several events using a probability line, among other things

        1. certain, possible or impossible outcome
        1. more likely, just as likely, less likely event
    6. Distinguishes between prediction and outcome
    7. Uses tables or diagrams to collect and display the outcomes of an experiment
    8. Enumerates possible outcomes of

        1. a simple random experiment
        1. a random experiment, using a table, a tree diagram
    9. Compares qualitatively the theoretical or experimental probability of events
    10. Recognizes that a probability is always between 0 and 1
    11. Uses fractions, decimals or percentages to quantify a probability
    12. Compares the outcomes of a random experiment with known theoretical probabilities
    13. Simulates random experiments with or without the use of technology

    #### Vocabulary
        Chance, random experiment, enumeration, tree diagram
        Certain outcome, possible outcome, impossible outcome
        Event, likely, just as likely, more likely, less likely, event probability


## Mathematics

## Examples of strategies

The strategies that are helpful for the development and use of the three mathematics competencies are integrated into the learning process. It is possible to emphasize some of these strategies, depending on the situation and educational intent.
Since students must build their own personal repertoire of strategies, it is important to encourage them to become independent in this regard and help them learn how to use these strategies in different contexts.

### Cognitive and metacognitive strategies

#### Planning
* What is the task that I am being asked to do?
* What prior learning do I need to use?
* What information is relevant?
* Do I need to break the problem down?
* How much time will I need to do this task?
* What resources will I need?

#### Comprehension
* Which terms seem to have a mathematical meaning different from their meaning in everyday language?
* What is the purpose of the question? Am I able to explain it in my own words?
* Do I need to find a counter-example to prove that what I am stating is false?
* Is all the information in the situation relevant? Is some information missing?
* What kind of diagram could demonstrate the steps involved in the task?

#### Organization
* Should I group, list, classify, reorganize or compare the data, or use diagrams
* (representations that show the relationships between objects or data)?
* Can I use concrete objects or simulate or mime the situation?
* Can I use a table or chart? Should I draw up a list?
* Are the main ideas in my approach well represented?
* What concepts and mathematical processes should I use?
* What type of representation (words, symbols, figures, diagrams, tables, etc.) could I use to translate this situation?

#### Development
* Can I represent the situation mentally or in written form?
* Have I solved a similar problem before?
* What additional information could I find using the information I already have?
* Have I used the information that is relevant to the task? Have I considered the unit of measure, if applicable?
* What mathematical expression translates the situation?
* Can I see a pattern?
* Which of the following strategies could I adopt?
* Make systematic trials
* Work backwards
* Give examples
* Break the problem down
* Change my point of view
* Eliminate possibilities
* Simplify the problem (e.g. reduce the number of data values, replace values by values that can be manipulated more easily, rethink the situation with regard to a particular element)

#### Regulation
* Is my approach effective and can I explain it?
* Can I check my solution using reasoning based on an example or a counter-example?
* What I have I learned? How did I learn it?
* Did I choose an effective strategy and take the time I needed to fully understand the problem?
* What are my strengths and weaknesses?
* Did I adapt my approach to the task?
* What was the result expected?
* How can I explain the difference between the expected result and the actual result?
* What strategies used by my classmates or suggested by the teacher can I add to my repertoire of strategies?
* Can I use this approach in other situations?

#### Generalization
* In what ways are the examples similar or different?
* Which models can I use again?
* Can the observations made in a particular case be applied to other situations?
* Are the assertions I made or conclusions I drew always true?
* Did I identify examples or counterexamples?
* Did I see a pattern?
* Am I able to formulate a rule?

#### Retention
* What methods did I use (e.g. repeated something several times to myself or out loud; highlighted, underlined, circled, recopied important concepts; made a list of terms or symbols)?
* Would I be able to solve the problem again on my own?
* What characteristics would a situation need in order for me to reuse the same strategy?
* Is what I learned connected in any way to what I already knew?

#### Development of automatic processes
* Did I find a solution model and list the steps involved?
* Did I practise enough in order to be able to repeat the process automatically?
* Am I able to effectively use the concepts learned?
* Did I compare my approach to that of others?

#### Communication
* Did I show enough work so that my approach was understandable?
* What forms of representation (words, symbols, figures, diagrams, tables, etc.) did I use to interpret a message or convey my message?
* Did I experiment with different ways of conveying my mathematical message?
* Did I use an effective method to convey my message?
* What methods would have been as effective, more effective or less effective?

### Other strategies

#### **Affective strategies**
* How do I feel?
* What do I like about this situation?
* Am I satisfied with what I am doing?
* What did I do particularly well in this situation?
* What methods did I use to overcome difficulties and which ones helped me the most to:
  - reduce my anxiety?
  - stay on task?
  - control my emotions?
  - stay motivated?
* Am I willing to take risks?
* What are my successes?
* Do I enjoy exploring mathematical situations?

#### **Resource management strategies**
* Whom can I turn to for help and when should I do so?
* Did I accept the help offered?
* What documentation (e.g. glossary, ICT) did I use? Was it helpful?
* What manipulatives helped me in my task?
* Did I estimate the time needed for the activity correctly?
* Did I plan my work well (e.g. planned short, frequent work sessions; set goals to attain for each session)?
* What methods did I use to stay on task (appropriate environment, available materials)?

