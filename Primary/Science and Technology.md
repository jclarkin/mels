# Progression of Learning

[MELS Source](http://www.education.gouv.qc.ca/fileadmin/site_web/documents/education/jeunes/pfeq/PDA_PFEQ_science-technologie-primaire_2009_EN.pdf)

# Science and Technology

## August 24, 2009

## Table of Contents

- Introduction
- Material World
- Earth and Space
- Living Things
- Strategies

## Science and Technology

## Introduction

This document is complementary to the Science and Technology program. It aims to provide information on the knowledge and skills students must acquire for each year of elementary school. It is intended to help teachers with their lesson planning. The document includes tables outlining the knowledge related to the categories _Material World_ , _Earth and Space_ and _Living Things_ , as well as Exploration Strategies, Strategies for Recording, Using and Interpreting Information and Communication Strategies. The nature and scope of this knowledge is further clarified by a list of statements indicating the degree of complexity of the subject matter at the elementary school level.

Given the vast range of knowledge in question, it is recommended that the focus be on the statements that appear in bold. For the most part, these statements were selected because they are interrelated and provide a well-balanced knowledge base within each category. This knowledge will enable students to construct their representation of the world and develop their scientific and technological literacy.

As early as kindergarten, children begin showing an interest in science and technology^1 by expressing curiosity about the world around them. During the first cycle of elementary school, students explore scientific and technological activity through the other subjects and the broad areas of learning. The knowledge to be acquired is often based on their immediate environment and involves simple and usually observable phenomena. In this way, the students begin developing their scientific and technological literacy and continue to do so throughout their studies. During the second and third cycles, teachers present concrete and meaningful activities that encourage children to be inquisitive and raise questions. The problems addressed in discussing these questions often guide the teacher in choosing the knowledge that will be covered in class.

By appropriately using the knowledge listed in this document, the students will develop the competencies outlined in the Science and Technology program. In order to propose explanations for or solutions to scientific or technological problems , students must become familiar with strategies and acquire conceptual and technical knowledge that will enable them to understand and explore the problem and then justify their choices. In addition, in order to make the most of scientific and technological tools, objects and procedures , they must apply the knowledge needed to use tools, design various objects and evaluate their use and impact. Finally, in order to communicate in the languages used in science and technology , they must have the knowledge that will enable them to interpret and convey messages by proficiently using the languages and types of representations associated with this subject.

* [Crash Course Kids: Engineering: The Engineering Process](https://www.youtube.com/playlist?list=PLhz12vamHOnZ4ZDC0dS6C9HRN5Qrm0jHO)
* [Crash Course Kids: Physical Science: Properties of Matter](https://www.youtube.com/playlist?list=PLhz12vamHOnaY7nvpgtQ0SIbuJdC4HA5O)
* [Crash Course Kids: Physical Science: Intro to Gravity](https://www.youtube.com/playlist?list=PLhz12vamHOnYXbP8_-PnzYDP3ibgdfH_4)
* [Crash Course Kids: Earth Science: Earth's Spheres & Natural Resources](https://www.youtube.com/playlist?list=PLhz12vamHOnYmvLSYtQvuxDrWSi795yDa)
* [Crash Course Kids: Space Science: The Sun & Its Influence on Earth](https://www.youtube.com/playlist?list=PLhz12vamHOnagseIgy26MoPI79NXiFBwN)
* [Crash Course Kids: Space Science: Intro to Stars](https://www.youtube.com/playlist?list=PLhz12vamHOna6ySCBRBGgCLLoTbMgqMzd)
* [Crash Course Kids: Life Science: Life Science & Flow of Energy](https://www.youtube.com/playlist?list=PLhz12vamHOnZv8kM6Xo6AbluwIIVpulio)


## Material World


1. Matter

    1. Properties and characteristics of matter
        1. Classifies objects according to their properties
            (e.g. colour, shape, size, texture, smell)

        1. Classifies materials (e.g. fabrics, sponges, papers) according to their degree of absorption

        1. Distinguishes between materials that are permeable to water and those that are not

        1. Distinguishes between translucent substances (transparent or coloured) and opaque substances


        1. Describes the shape, colour and texture of an object or a substance

        1. Distinguishes between the mass (quantity of matter) of an object and its weight (gravitational force acting on the mass)

        1. Classifies solids according to their density (identical volumes and different masses or identical masses and different volumes)

        1. Associates the buoyancy of a volume of liquid in an identical volume of a different liquid with the densities of these liquids (relative density)

        1. Explains the buoyancy of a substance in another substance, using their respective densities (relative density)

        1. Describes various other physical properties of an object, a substance or a material
        (e.g. elasticity, hardness, solubility)

        1. Recognizes the materials of which an object is made

    2. Mixtures

        1. Recognizes mixtures in his/her environment (e.g. air, juice, salad dressing, soup, raisin bread)

        1. Distinguishes between mixtures of miscible and immiscible liquids (e.g. water and milk, water and oil)

        1. Distinguishes between substances that are soluble in water (e.g. salt, sugar) and those that are not (e.g. pepper, sand)

    3. Solid, liquid, gaseous state, phase changes

        1. Distinguishes among the three states of matter (solid, liquid, gas)

        1. Recognizes water in its solid (ice, snow), liquid and gaseous (steam) state

        1. Describes the operations involved in changing water from one state to another (heating or cooling)

        1. Determines the state of various objects and substances in his/her environment (e.g. glass, air, milk, plastic)

    4. Conservation of matter

        1. Recognizes that the quantity of the matter remains the same once a change has occurred
            (e.g. 50 ml of water in a saucer or a glass, whole piece of chalk or ground chalk, flattened piece of modelling clay or a ball of modelling clay)

    5. Changes in matter

        1. Demonstrates that physical changes (e.g. deforming, breaking, grinding, phase changes) do not change the properties of matter

        1. Demonstrates that chemical changes (e.g. cooking, combustion, oxidation, acid-base reactions) change the properties of matter

        1. Explains how certain household products are made (e.g. soap, paper)^1

    6. Common household products

        1. Associates the uses of certain household products with their properties
            (e.g. cleaning products remove grease, vinegar and lemon juice help preserve certain foods)


        1. Recognizes commonly used products that are potentially dangerous (safety- related symbols)

1. **B. Energy**

    1. Forms of energy

        1. Describes different forms of energy ( mechanical, electrical, light, chemical, heat, sound, nuclear)

        1. Identifies sources of energy in his/her environment (e.g. moving water, chemical reaction in a battery, sunlight)

    2. Transmission of energy

        1. Distinguishes between substances that are thermal conductors and those that are thermal insulators

        1. Distinguishes between substances that are electrical conductors and those that are electrical insulators

        1. Identifies the components of a simple electric circuit (wire, source, light bulb, switch)

        1. Describes the functions of the components of a simple electric circuit (conductor, insulator, energy source, light bulb, switch)

        1. Identifies the characteristics of a sound wave (e.g. volume, timbre, echo)

        1. Describes the behaviour of light rays (reflection, refraction)

        1. Explains the motion of convection in liquids and gases (e.g. boiling water)

    3. Transformation of energy

        1. Describes situations in which human beings consume energy (e.g. heating, transportation, food consumption, recreation)

        1. Names means used by human beings to limit their energy consumption
        (e.g. fluorescent light bulbs, timers) and to conserve energy (e.g. insulation)

        1. Explains the insulating properties of various substances (e.g. polystyrene, mineral wool, straw)

        1. Describes the transformations of energy from one form to another

        1. Recognizes the transformations of energy from one form to another in various devices (e.g. flashlight: chemical to light; electric kettle: electrical to heat)

1. **C. Forces and motion**

    1. Electrostatic

        1. Describes the effect of electrostatic attraction (e.g. paper attracted by a charged object)

    2. Magnetism and electromagnetism

        1. Recognizes the effects of magnetism on magnets (attraction and repulsion)

        1. Identifies situations in which magnets are used

        1. Distinguishes between a magnet and an electromagnet

        1. Identifies objects that use the principles of electromagnetism (e.g. electromagnetic crane, fire door)

    3. Gravitational attraction on an object

        1. Describes the effect of gravitational attraction on an object (e.g. free fall)

    4. Pressure

        1. Recognizes various manifestations of pressure
        (e.g. inflatable balloon, atmospheric pressure, airplane wing)

        1. Describes the effects of pressure on an object
        (e.g. compression, displacement, increase in temperature)

    5. Characteristics of motion

        1. Describes the characteristics of motion (e.g. direction, speed)

    6. Effects of a force on the direction of an object

        1. Identifies situations involving the force of friction (pushing on an object, sliding an object, rolling an object)

        1. Identifies examples of a force
        (e.g. pulling, pushing, throwing, squeezing, stretching)

        1. Describes the effects of a force on an object
            (e.g. Sets it in motion, changes its motion, stops it)


        1. Describes the effects of a force on a material or structure

    7. Combined effects of several forces on an object

        1. Predicts the combined effect of several forces on an object at rest or an object moving in a straight line (e.g. reinforcement, opposition)

1. **D. Systems and interaction**

    1. Everyday technical objects

        1. Describes the parts and mechanisms that make up an object

        1. Identifies the needs that an object was originally designed to meet

    2. Simple machines

        1. Recognizes simple machines (lever, inclined plane, screw, pulley, winch wheel) used in an object (e.g. lever in seesaw, inclined plane for an access ramp)

        1. Describes the uses of certain simple machines (to adjust the force required)

    3. Other machines

        1. Identifies the main function of some complex machines (e.g. cart, waterwheel, wind turbine)

    4. How manufactured objects work

        1. Identifies the mechanical parts (e.g. gears, cams, springs, simple machines, connecting rods )

        1. Recognizes two types of motion (rotation and translation)

        1. Describes a simple sequence of mechanical parts in motion

    5. Servomechanism and robots

        1. Recognizes robotic structures that use a servomechanism

    6. Transportation technology (e.g. car, airplane, boat)

        1. Recognizes the influence and impact of transportation technology on people’s way of life and surroundings

    7. Electron technology

        1. Recognizes the influence and the impact of electric appliances on people’s way of life and surroundings (e.g. telephone, radio, television, computer)

1. Techniques and instrumentation

    1. Use of simple measuring instruments

        1. Appropriately uses simple measuring instruments (rulers, dropper, graduated cylinder, balance, thermometer, chronometer)

    2. Use of simple machines

        1. Appropriately uses simple machines (lever, inclined plane, screw, pulley, winch, wheel)

    3. Use of tools

        1. Appropriately and safely uses tools (e.g. pliers, screwdriver, hammer, wrench, simple template)

    1. Design and manufacture of instruments, tools, machines, structures (e.g. bridges, towers), devices (e.g. water filtration device), models (e.g. glider) and simple circuits


        1. Knows the symbols associated with types of motion, electrical components and mechanical parts

        1. Interprets a diagram or a plan containing symbols

        1. Uses symbols associated with mechanical parts and electrical components in a diagram or drawing

        1. Draws and cuts parts out of various materials using appropriate tools

        1. Uses appropriate assembling methods (e.g. screws, glue, nails, tacks, nuts)

        1. Uses appropriate tools for proper finishing work

        1. Uses simple machines, mechanisms or electrical components to design or make an object

1. Appropriate language

    1. Terminology related to an understanding of the material world

        1. Appropriately uses terminology related to the material world

        1. Distinguishes between the meaning of a term used in a scientific or technological context and its meaning in everyday language (e.g. source, matter, body, energy, machine)

    2. Conventions and types of representations specific to the concepts studied

        1. Communicates using appropriate types of representations that reflect the rules and conventions of science and technology (e.g. symbols, graphs, tables, drawings, sketches, norms and standardization)

## Earth and Space



1. Matter

    1. Properties and characteristics of matter on Earth

        1. Compares the properties of different types of soil (e.g. composition, capacity to retain water, capacity to retain heat)

        1. Describes the various ways in which the quality of water, soil or air affects living things

    1. Distinguishes between a fossil (or a trace of a living thing) and a rock

    1. Distinguishes between a rock and a mineral

        1. Classifies rocks (presence of strata, size of the crystals) and minerals (colour, texture, lustre, hardness) according to their properties

    2. Organization of matter

        1. Describes the observable properties of crystals (colour, geometric patterns)

        1. Describes the main structures on the Earth’s surface (e.g. continents, oceans, ice caps, mountains, volcanoes)

    3. Transformation of matter

        1. Describes different types of precipitation (rain, snow, hail, freezing rain)

        1. Identifies natural sources of fresh water (brooks, lakes, rivers) and natural sources of salt water (seas, oceans)

        1. Explains the water cycle (evaporation, condensation, precipitation, runoff and infiltration)

        1. Describes certain natural phenomena (e.g. erosion, lightning, tornado, hurricane)

        1. Describes the impact of certain natural phenomena on the environment or on the well-being of individuals

1. Energy

    1. Sources of energy

        1. Explains that the sun is the main source of energy on Earth

        1. Identifies natural sources of energy (sun, moving water, wind)

        1. Identifies fossil fuel-based energy (e.g. oil, coal, natural gas)

    2. Transmission of energy

        1. Describes methods for transmitting thermal energy (e.g. radiation, convection, conduction)

    3. Transformation of energy

        1. Describes what renewable energy is

        1. Explains that sunlight, moving water and wind are renewable sources of energy

        1. Describes the methods invented by humans to transform renewable sources of energy into electricity (hydroelectric dam, wind turbine, solar panels)

        1. Describes what nonrenewable energy is

        1. Explains that fossil fuels are nonrenewable sources of energy

        1. Names fuels derived from petroleum (e.g. gasoline, propane, butane, fuel oil, natural gas)

1. **C. Forces and motion**

    1. Rotation of the Earth

        1. Associates the cycle of day and night with the rotation of the Earth

    2. The tides

        1. Describes the ebb and flow of the tides (rise and fall of sea levels)

1. **D. Systems and interaction**

    1. Light and shadow

        1. Describes the influence of the apparent position of the sun on the length of shadows

    2. System involving the sun, the Earth and the moon

        1. Associates the sun with the idea of a star, the Earth with the idea of a planet and the moon with the idea a natural satellite

        1. Describes the rotational and revolutionary motion of the Earth and the moon

        1. Illustrates the phases of the lunar cycle (full moon, new moon, first and last quarters)

        1. Illustrates the formation of eclipses (lunar, solar)

    3. Solar system

        1. Recognizes the main components of the solar system (sun, planets, natural satellites)

        1. Describes the characteristics of the main components of the solar system
            (e.g. composition, size, orbit, temperature)

    4. Seasons

        1. Describes the changes to the environment throughout the seasons
            (temperature, amount of daylight, type of precipitation)

        1. Explains the sensations experienced (hot, cold, comfortable) with regard to temperature measurements

        1. Associates the changing of the seasons with the revolution and tilt of the Earth

    5. Stars and the Galaxies

        1. Recognizes the stars and the constellations on a map of the stars

        1. Distinguishes between stars, constellations and galaxies

    6. Meteorological systems and climates

        1. Makes connections between weather conditions and the types of clouds in the sky

        1. Associates the average amount of precipitation with the climate of a region (dry, humid)

        1. Associates the average temperature with the climate of a region (polar, cold, temperate, mild, hot)

    7. Technologies related to the Earth, the atmosphere and outer space

        1. Recognizes the influence and the impact of technologies related to the Earth, the atmosphere and outer space on people’s way of life and surroundings (e.g. prospecting equipment, meteorological instruments, seismograph, telescope, satellite, space station)

1. Techniques and instrumentation

    1. Use of simple observational instruments

        1. Appropriately uses simple observational instruments (e.g. magnifying glass, binoculars)

    2. Use of simple measuring instruments

        1. Appropriately uses simple measuring instruments (e.g. rulers, dropper, graduated cylinder, balance, thermometer, wind vane, barometer, anemometer, hygrometer)

    3. Design and manufacture of measuring instruments and prototypes

        1. Designs and manufactures measuring instruments and prototypes^1

1. Appropriate language

    1. Terminology related to an understanding of the Earth and the universe

        1. Appropriately uses terminology related to an understanding of the Earth and the universe

        1. Distinguishes between the meaning of a term used in a scientific or technological context and its meaning in everyday language (e.g. space,
    2. Conventions and types of representations specific to the concepts studied

        1. Communicates using appropriate types of representations that reflect the rules and conventions of science and technology (e.g. symbols, graphs, tables, drawings, sketches)

## Living Things

1. Matter

    1. Characteristics of living things

        1. Explains the basic needs of the metabolism of living things (e.g. nutrition, respiration)

        1. Describes activities connected to the metabolism of living things (transformation of energy, growth, maintenance of systems and body temperature)

        1. Distinguishes among the different types of embryonic development (viviparous for the majority of mammals, oviparous or ovoviviparous for the rest)

        1. Describes the types of sexual reproduction in animals (roles of the male and the female)

        1. Describes the types of sexual reproduction in plants (pistil, stamen, pollen, seed and fruit)

        1. Describes types of asexual reproduction in plants (e.g. budding, propagation by cuttings, formation of rootstocks and tubers)

    2. Organization of living things

        1. Describes the functions of certain parts of the anatomy ( e.g. limbs, head, heart, stomach)

        1. Describes the characteristics of different kingdoms (microorganisms, fungi, plants, animals)

        1. Classifies life forms according to their kingdom

        1. Lists animals according to their classification (mammals, reptiles, birds, fish, amphibians)

        1. Describes the anatomy of plants (roots, stems, leaves, flowers, fruits, seeds)

        1. Associates the parts of a plant with their general functions (roots, stems, leaves, flowers, fruits, seeds)

        1. Associates the parts and systems of the anatomy of animals with their general functions

        1. Explains the sensorial functions of certain parts of the anatomy (skin, eyes, mouth, ears, nose)

        1. Describes the anatomy and the function of the main organs of the female and male reproductive systems

    3. Transformations of living things

        1. Names the basic needs for plant growth (water, air, light, mineral salts)

        1. Describes the growth stages of a flowering plant

        1. Describes the growth stages of various animals

        1. Describes the changes in appearance of animals that undergo a metamorphosis (e.g. butterfly, frog)

        1. Explains the stages of growth and development in humans

        1. Describes the physical changes that take place during puberty

        1. Describes the main stages of the evolution of life forms

1. **B. Energy**

    1. Sources of energy for living things

        1. Compares the nutrition of domestic animals with that of wild animals

        1. Explains the nutritional needs common to all animals (water, sugars, lipids, proteins, vitamins, minerals)

        1. Associates familiar animals with their diet (carnivorous, herbivorous, omnivorous)

        1. Describes how photosynthesis works

        1. Distinguishes between photosynthesis and respiration

        1. Explains how water, light, mineral salts and carbon dioxide are essential to plants

        1. Describes agricultural and food technologies (e.g. crossbreeding of plants and their propagation by cuttings, selection and breeding of animals, food production, pasteurization)

    2. Transformation of energy in living things

        1. Illustrates a simple food chain (3 or 4 links)

        1. Describes an ecological pyramid of a given environment

1. **C. Forces and motion**

    1. How animals move

        1. Describes the different ways animals move (walking, creeping, flying, jumping)

        1. Names other ways animals move and why (e.g. defence, mating ritual)

    2. Motion in plants

        1. Distinguishes among the three types of motion in plants (geotropism, hydrotropism, phototropism)

        1. Explains how the types of motion in plants enable them to meet their basic needs

1. **D. Systems and interaction**

    1. Interaction between living organisms and their environment

        1. Describes the physical characteristics that demonstrate how animals adapt to their environment

        1. Describes the behaviours of familiar animals that enable them to adapt to their environment

        1. Identifies habitats and the animal and plant populations found in them

        1. Describes how animals meet their basic needs within their habitat

        1. Describes relationships between living things (parasitism, predation)

        1. Explains how animals and plants adapt to increase their chances of survival (e.g. mimicry, camouflage)

    2. Use of living things for consumption

        1. Provides examples of how living things are used (e.g. meat, vegetable, wood, leather)

    3. Interaction between humans and their environment

        1. Describes the impact of human activity on the environment (e.g. use of resources, pollution, waste management, land use, urbanization, agriculture)

    4. Food production techniques

        1. Describes the main steps in the production of various basic foods (e.g. making butter, bread, yogurt)

    5. Environmental technologies

        1. Explains the scientific and technological concepts associated with recycling and composting (e.g. properties of matter, phase changes, physical changes, chemical changes, food chain, energy)

1. Techniques and instrumentation

    1. Use of simple observational instruments

        1. Appropriately uses simple observational instruments (e.g. magnifying glass, binoculars)

    2. Use of simple measuring instruments

        1. Appropriately uses simple measuring instruments (e.g. rulers, dropper, graduated cylinder, balance, thermometer)

    3. Design and manufacture of environments

        1. Designs and manufactures environments^1 (e.g. aquarium, terrarium, incubator, greenhouse)

1. Appropriate language

    1. Terminology related to an understanding of living things

        1. Appropriately uses terminology related to an understanding of living things

        1. Distinguishes between the meaning of a term used in a scientific or technological context and its meaning in everyday language (e.g. habitat, metamorphosis)

    2. Conventions and types of representations specific to the concepts studied

        1. Communicates using appropriate types of representations that reflect the rules and conventions of science and technology (e.g. symbols, graphs, tables, drawings, sketches)

## Strategies

The strategies listed below are fundamental to the approaches used in science and technology. Whereas certain strategies apply at every step in the student’s work (e.g. using different types of reasoning, exchanging information), others are used at different stages (e.g. becoming aware of his or her previous representations, using different tools for recording information). It is recommended that students start using strategies in the first cycle of elementary school.

### Exploration strategies

* Studying a problem or a phenomenon from different points of view (e.g. social, environmental, historical, economic perspectives)
* Distinguishing between the different types of information useful for solving the problem
* Recalling similar problems that have already been solved
* Becoming aware of his or her previous representations
* Drawing a diagram for the problem or illustrating it
* Formulating questions
* Putting forward hypotheses (e.g. individually, as a team, as a class)
* Exploring various ways of solving the problem
* Anticipating the results of his or her approach
* Imagining solutions to a problem in light of his or her explanations
* Taking into account the constraints involved in solving a problem or making an object (e.g. specifications, available resources, time allotted)
* Examining his or her mistakes in order to identify their source
* Using different types of reasoning (e.g. induction, deduction, inference, comparison, classification)
* Using empirical approaches (e.g. trial and error, analysis, exploration using one’s senses)

### Strategies for recording, using and interpreting information

* Using different sources of information (e.g. books, newspapers, Web sites, magazines, experts)
* Validating sources of information
* Using a variety of observational techniques and tools
* Using technical design to illustrate a solution (e.g. diagrams, sketches, technical drawings)
* Using different tools for recording information (e.g. diagrams, graphs, procedures, notebooks, logbook)

### Communication strategies

* Using different means of communication to propose explanations or solutions (e.g. oral presentation, written presentation, procedure)
* Using tools to display information in tables and graphs or to draw a diagram
* Organizing information for a presentation (e.g. tables, diagrams, graphs)
* Exchanging information
* Comparing different possible explanations for or solutions to a problem in order to assess them (e.g. full-group discussion)
