# Progression of Learning

# Physical Education and Health

## August 24, 2009


## Table of Contents

- Introduction
- Competency 1 – To perform movement skills in different physical activity settings
   - Knowledge
   - Motor Skills
- Competency 2 – To interact w ith others in different physical activity settings
   - Knowledge
   - Strategies (action rules)
   - Motor Skills
   - Behaviour
- Competency 3 – To adopt a healthy, active lifestyle
   - Knowledge (lifestyle habits)
   - Knowledge (effects of a sedentary lifestyle)
   - Knowledge (anatomy and physiology of the human body)


## Physical Education and Health

## Introduction

This document is complementary to the Physical Education and Health program. It aims to help teachers plan and carry out
learning situations. It provides additional information on the knowledge and skills that students should acquire in each year
of elementary school. It is divided into three sections that correspond to the three competencies of the Physical Education
and Health program. Each section consists of a brief introduction that provides an overview of what the students are
expected to learn, as well as a partial list of examples of the knowledge and skills to be acquired.

Through a variety of learning situations, Physical Education and Health teachers will help students gradually acquire a
repertoire of knowledge and skills they can use to develop the three competencies of the program. In order to be able to
perform movement skills in different physical activity settings, students must acquire the knowledge and motor skills that
will allow them to choose and adequately perform sequences and combinations of movement skills. Similarly, in order to be
able to interact with others in different physical activity settings, students must develop a repertoire of knowledge,
strategies, motor skills and attitudes that will enable them to develop and carry out plans of action with one or more
partners. Lastly, in order to be able to adopt a healthy, active lifestyle, students must learn about healthful lifestyle habits,
the effects of a sedentary lifestyle and concepts of human anatomy and physiology.

```
In short, by gradually acquiring and using the knowledge and skills outlined in this document, students will develop the
competencies of the Physical Education and Health program.
```

## Physical Education and Health

## Competency 1 – To perform movement skills in different physical activity settings

## settings

In Physical Education and Health, students develop a repertoire of knowledge and motor skills that will allow them to
gradually practise various types of physical activities safely on their own, be it in a gymnasium or outside of school.
Teachers help students better understand concepts of time and space as well as principles of balance and coordination so
that students can apply these concepts and principles in action. Teachers also help students perform a range of motor
skills that will allow them to position themselves, move around and handle objects and implements in different types of
physical activities.

```
Knowledge
Motor Skills
```

## Physical Education and Health

## Competency 1: To perform movement skills in different physical activity

## settings

### Knowledge

```
Student constructs knowledge with teacher guidance.
```
```
Student applies knowledge by the end of the school year.
```
```
Student reinvests knowledge.
```
```
Elementary
```
```
Cycle
One
```
```
Cycle
Two
```
```
Cycle
Three
```
```
A. Elements related to the body^1123456
Identifies the main parts of the body
(e.g. head, trunk, lower limbs)
```
#### 1.

```
Locates his/her body and main body parts in space
(e.g. left, right, in front)
```
#### 2.

```
Names some sensations perceived by the body when it is moving or stationary
(e.g. imbalance, dizziness)
```
#### 3.

```
B. Concepts of time and space 123456
```
```
Determines how much space is available
(e.g. restricted space, play area)
```
#### 1.

```
Distinguishes different levels
(e.g. high, medium, low)
```
#### 2.

```
Identifies reference points
(e.g. inside a hoop, outside a mat, in relation to a cone, on the yellow line)
```
#### 3.

```
Distinguishes different directions
(e.g. before, behind, ahead, to the right)
```
#### 4.

```
Estimates distances
(e.g. near, far, five steps from.. .)
```
#### 5.

```
Recognizes duration
(e.g. second, minute)
```
#### 6.

```
Recognizes speed
(e.g. slow, fast)
```
#### 7.

```
Recognizes rhythm
(e.g. regular, jerky)
```
#### 8.

```
C. Principles of balance^123456
```
1. Identifies his/her centre of gravity

```
Finds a few ways of maintaining his/her balance (number of body parts in contact with
floor or surface, position of body parts used for support, surface used for support, etc.)
(e.g. spreads feet shoulder-width apart, uses arms when walking along a balance beam,
bends knees when landing)
```
#### 2.

```
D. Principles of coordination^123456
```
```
Explains a few different ways of coordinating movements (dissociation, linking of
movements, flow, etc.)
(e.g. moving one’s legs and then hands to jump like a marionette)
```
#### 1.

```
E. Types of support 123456
```
```
Names different types of support
(e.g. on hands, feet, buttocks)
```
#### 1.

```
F. Types of grips^123456
```
```
Identifies grips based on the object used
(e.g. baseball, Frisbee)
```
#### 1.

```
Identifies grips based on the apparatus used
(e.g. mixed grip on the horizontal bar)
```
#### 2.

```
Identifies grips based on the implement used
(e.g. racket, hockey stick)
```
#### 3.


```
G. Vocabulary related to the equipment used^123456
```
```
Names objects
(e.g. medicine ball, shuttlecock)
```
#### 1.

```
Names apparatus
(e.g. balance bench, stall bars, balance beam)
```
#### 2.

```
Names the main parts of objects used for locomotion
(e.g. snowshoe frame, ski binding, skate blade)
```
#### 3.

```
Names the main parts of implements
(e.g. head of a racket)
```
#### 4.

1. These elements include basic knowledge that students should acquire to better understand their bodies. The knowledge
    associated with human anatomy and physiology is presented in the section on Competency 3, _To adopt a healthy, active_
    _lifestyle_. This knowledge serves two purposes: to help students learn how to move independently, effectively and safely in
    their physical surroundings (e.g. move to the right, quickly, without running into obstacles) and help them develop
    healthy, active lifestyle habits (e.g. I know how my heart works and I want to take care of it; I know what muscle to stretch
    in order to.. .).


## Physical Education and Health

## Competency 1: To perform movement skills in different physical activity

## settings

### Motor Skills

```
Student constructs knowledge with teacher guidance.
```
```
Student applies knowledge by the end of the school year.
```
```
Student reinvests knowledge.
```
```
Elementary
```
```
Cycle
One
```
```
Cycle
Two
```
```
Cycle
Three
```
**Types of skills**^1

```
A. Locomotor skills 123456
```
1. Moving about an area with and without obstacles

```
a. Walks, crawls, skips, moves on all fours
```
```
b. Runs, gallops, hops, performs standing long jump
```
```
Goes around and through obstacles
(e.g. goes around cones, through a tunnel)
```
```
c.
```
```
d. Performs side steps and cross-steps, spins
```
```
Goes over obstacles
(e.g. jumps over hurdles)
```
```
e.
```
2. Moving about on apparatus

```
Moves through low apparatus
(e.g. walks along an overturned balance bench)
```
```
a.
```
```
Moves through apparatus of various heights
(e.g. moves along a horizontal ladder, goes up a climbing wall)
```
```
b.
```
```
Climbs apparatus of various heights
(e.g. goes up and down wall bars, climbs a rope)
```
```
c.
```
3. Moving about using objects

```
Moves about using different objects
(e.g. skates, skis, rolls a cylinder by walking on it)
```
```
a.
```
4. Running jumps

```
Performs different running jumps
(e.g. broad jump, high jump, half twist)
```
```
a.
```
```
Performs jumps using apparatus or springing apparatus
(e.g. performs a straddle or split jump using a mini-trampoline)
```
```
b.
```
```
Performs different jumps over an obstacle using springing apparatus
(e.g. vaults over a vaulting box using a springboard)
```
```
c.
```
5. Jumping rope

```
a. Jumps rope (basic jump)
```
```
Jumps rope in different ways
(e.g. on one foot, backwards, while moving, with feet crossed)
```
```
b.
```
6. Floor rotations

```
a. Performs a tuck forward roll
```
```
Rolls in different ways
(e.g. forward straddle roll)
```
```
b.
```
```
Performs complex rotations
(e.g. cartwheel, round-off)
```
```
c.
```

**B. Nonlocomotor skills**^123456

1. Balanced stances (postures) on the floor and on apparatus

```
Maintains simple postures using different body parts for support
(e.g. stork stand, arabesque on one knee)
```
```
a.
```
```
Maintains simple postures on apparatus
(e.g. stork stand on balance bench, squatting on a fixed cylinder)
```
```
b.
```
```
Maintains complex postures using different body parts for support
(e.g. headstand, handstand)
```
```
c.
```
2. Rotations on the spot and on apparatus

```
Turns on his/her own axis on the floor
(e.g. pivots on one foot, spins)
```
```
a.
```
```
Turns on his/her own axis in the air
(e.g. does a half twist, full twist)
```
```
b.
```
```
Turns on his/her own axis on apparatus
(e.g. pivots on a cylinder)
```
```
c.
```
**C. Manipulation skills 123456**

1. Object manipulation

```
Handles a variety of objects in different ways
(e.g. moves a ball around his/her body, rolls a hoop)
```
```
a.
```
```
Handles a variety of objects used in specialized activities
(e.g. circus arts, rhythmic gymnastics)
```
```
b.
```
```
c. Dribbles with one hand
```
```
i. on the spot and while moving
```
```
through obstacles
(e.g. dribbles a ball between cones)
```
```
ii.
```
```
d. Dribbles with feet
```
```
i. on the spot and while moving
```
```
ii. through obstacles
```
```
e. Juggles different patterns, using
```
```
i. one object
```
```
ii. two objects
```
```
iii. three objects
```
```
f. Juggles different patterns while moving, using
```
```
i. one object
```
```
ii. two objects
```
```
iii. three objects
```
2. Projecting objects without an implement

```
Throws a variety of objects underhand at a target
(e.g. throws a beanbag into a container, throws a ball at a bowling pin)
```
```
a.
```
```
Throws a variety of objects overhand at a target
(e.g. throws a ball at a target on the wall)
```
```
b.
```
```
Strikes or kicks a variety of objects at a target
(e.g. kicks a ball toward a bowling pin)
```
```
c.
```
```
Throws, strikes or kicks a variety of objects used in specialized activities
(e.g. track and field, mini-basketball, mini-volleyball, soccer)
```
```
d.
```
3. Projecting objects with an implement


```
Throws a variety of objects at a target using an implement
(e.g. uses a stick to throw a ring at a bowling pin)
```
```
a.
```
```
Strikes a variety of objects at a target using an implement
(e.g. hits a foam ball against a wall using a racket)
```
```
b.
```
```
Throws or strikes an object using an implement for specialized activities
(e.g. mini-tennis, hockey)
```
```
c.
```
4. Receiving objects with or without an implement

```
Catches a variety of objects using two hands
(e.g. catches a ball after throwing it against a wall)
```
```
a.
```
```
Catches a variety of objects using one hand
(e.g. catches a ball after one bounce on the floor)
```
```
b.
```
```
Catches an object using his/her foot in different ways
(e.g. deflects, blocks, traps or controls)
```
```
c.
```
```
Catches an object using an implement
(e.g. throws a ball against a wall and catches it with an intercrosse stick)
```
```
d.
```
```
Catches a variety of objects used in specialized activities
(e.g. tchoukball, ultimate Frisbee, mini-football)
```
```
e.
```
1. Locomotor, nonlocomotor and manipulation skills draw on the knowledge described in the preceding pages (spatial
    concept, principles of balance, etc). For example, students must move in different directions, vary their speed, use
    different grips, adjust their balance, etc.


## Physical Education and Health

## Competency 2 – To interact w ith others in different physical activity settings

## settings

**1**

To be able to interact with one or more partners in a variety of physical activities, students must develop a repertoire of
knowledge and strategies as well as certain motor skills and behaviour. With the help of their teachers, students learn to
better understand the principles and methods of communication, the principles of synchronization, as well as the different
roles they have to play so that they can apply this knowledge in different settings. They also help students better grasp a
series of strategies (action rules) that they can apply in action. Finally, with the help of their teachers, students develop
attitudes that will enable them to act ethically at all times.

```
Knowledge
Strategies (action rules)
Motor Skills
Behaviour
```
1. W hen students develop the competency _To interact with others_ , they draw on the resources used in the competency _To_
    _perform movement skills_ , as they have to perform movement skills while interacting.


## Physical Education and Health

## Competency 2: To interact with others in different physical activity

## settings

**1**

### Knowledge

```
Student constructs knowledge with teacher guidance.
```
```
Student applies knowledge by the end of the school year.
```
```
Student reinvests knowledge.
```
```
Elementary
```
```
Cycle
One
```
```
Cycle
Two
```
```
Cycle
Three
```
```
A. Principles of communication 123456
```
```
Names a few ways of being understood by others
(e.g. making eye contact, giving a signal, asking a question)
```
#### 1.

```
Names a few ways of being receptive to others’ messages
(e.g. listening without interrupting, looking at the person who is speaking)
```
#### 2.

```
Names a few ways of sending out misleading signals during game play
(e.g. feinting using one’s body, looking to the right and then passing to the left)
```
#### 3.

```
B. Methods of communication^123456
```
```
Names different ways of communicating
(e.g. calling out someone’s name, using a hand signal)
```
#### 1.

```
C. Principles of synchronization 123456
```
```
Indicates a few ways of synchronizing his/her movements
(i.e. performing movements or actions in the right place at the right time)
```
#### 1.

```
when throwing an object
(e.g. keeping track of a partner’s movement and throwing him/her a pass at the right
time)
```
```
a.
```
```
when receiving an object
(e.g. moving quickly to the appropriate spot to catch a ball thrown by a teammate)
```
```
b.
```
2. Recognizes different synchronization modes

```
simultaneous
(i.e. performing identical or different movements or actions at the same time)
```
```
a.
```
```
successive
(i.e. performing movements or actions one after the other)
```
```
b.
```
```
alternating
(i.e. repeating movements or actions in turns)
```
```
c.
```
```
overlapping
(i.e. performing a sequence of movements or actions starting at different times)
```
```
d.
```
```
D. Roles 123456
```
```
Explains in his/her own words the main actions of an offensive (attacking) player
(e.g. moving into open space, moving the object forward, scoring a goal)
```
#### 1.

```
Explains in his/her own words the main actions of a defensive player
(e.g. protecting his/her territory, guarding a player, preventing goals)
```
#### 2.

```
Explains in his/her own words the main actions of a team captain or leader
(e.g. calling a play in kinball, rounding up his/her teammates)
```
#### 3.

```
Explains in his/her own words the main actions of a carrier
(e.g. moving the object forward, protecting the object)
```
#### 4.

```
Explains in his/her own words the main actions of a noncarrier
(e.g. moving into an open space, calling the object)
```
#### 5.

```
Names the main positions occupied by players of a given activity
(e.g. top and base in acrobatic gymnastics)
```
#### 6.

```
Distinguishes the main types of support roles
(e.g. scorekeeper, timekeeper, referee)
```
#### 7.

1. W hen students develop the competency _To interact with others_ , they draw on the resources used in the competency _To_
    _perform movement skills_ , as they have to perform movement skills while interacting.


## Physical Education and Health

## Competency 2: To interact with others in different physical activity

## settings

### Strategies (action rules)

```
Student constructs knowledge with teacher guidance.
```
```
Student applies knowledge by the end of the school year.
```
```
Student reinvests knowledge.
```
```
Elementary
```
```
Cycle
One
```
```
Cycle
Two
```
```
Cycle
Three
```
```
A. Action rules in combat activities^123456
```
```
Names a few offensive action rules
(e.g. using the space available, throwing an opponent off-balance, making a feint)
```
#### 1.

```
Names a few defensive action rules
(e.g. reacting quickly to the actions of an opponent)
```
#### 2.

```
B. Action rules in duelling activities 123456
```
```
Names a few offensive action rules
(e.g. making a feint)
```
#### 1.

```
Names a few defensive action rules
(e.g. using the space available to get away from an opponent)
```
#### 2.

```
C. Action rules in group activities in a common space^123456
```
```
Names a few offensive action rules
(e.g. attacking the other team’s goal, moving the object forward)
```
#### 1.

```
Names a few defensive action rules
(e.g. protecting his/her goal, getting back in the defensive zone)
```
#### 2.

```
D. Action rules in group activities in separate spaces 123456
```
```
Names a few offensive action rules
(e.g. attacking the other team’s territory, keeping the object moving)
```
#### 1.

```
Names a few defensive action rules
(e.g. protecting an assigned area in his/her territory)
```
#### 2.

1. Given that these rules are applied in action, they are listed under _Motor Skills_ for each type of movement or action (e.g.
    opposition movements or actions).


## Physical Education and Health

## Competency 2: To interact with others in different physical activity

## settings

### Motor Skills

```
Student constructs knowledge with teacher guidance.
```
```
Student applies knowledge by the end of the school year.
```
```
Student reinvests knowledge.
```
```
Elementary
```
```
Cycle
One
```
```
Cycle
Two
```
```
Cycle
Three
```
**Types of movements or actions**

```
A. Cooperation movements or actions^1123456
```
1. Cooperates with partner(s) while performing movements or actions in the right place at the right time

```
Throws an object at a moving target
(e.g. passes a ball to a partner who is moving)
```
```
a.
```
```
Receives an object while moving
(e.g. catches a Frisbee thrown by a partner)
```
```
b.
```
```
Projects an object at a moving target using an implement
(e.g. uses a stick to pass a ring to a partner who is moving)
```
```
c.
```
```
Receives an object using an implement while moving
(e.g. uses a stick to receive a pass)
```
```
d.
```
2. Synchronizes his/her movements or actions with partner(s)

```
Adapts his/her actions to those of a partner according to different synchronization
modes (e.g. simultaneous, successive) (e.g. dribbles a ball at the same pace as a
partner)
```
```
a.
```
```
Adapts his/her actions to those of several partners according to different
synchronization modes
(e.g. does a roll with other students and arrives at the same time at the center of the
mat)
```
```
b.
```
```
Positions himself/herself, moves or manipulates objects taking into account his/her
partner(s)
(e.g. creates a human pyramid, juggles with a partner)
```
```
c.
```
```
B. Opposition movements or actions^123456
```
1. Opposition movements or actions in combat activities (e.g. judo)

```
Uses the space available
(e.g. gets closer to or away from an opponent, gains advantage over an opponent
using the boundaries of the combat area, regains his/her position by coming back to
the centre of the mat)
```
```
a.
```
```
Throws the opponent off-balance
(e.g. pushes and pulls in a friendly wrestling activity)
```
```
b.
```
```
Reacts to movements or actions of the opponent
(e.g. regains his/her balance, dodges an attack)
```
```
c.
```
```
Blocks the opponent
(e.g. keeps the opponent on the ground for three seconds)
```
```
d.
```
```
Deceives the opponent using a feint
(e.g. pretends to pull in order to push)
```
```
e.
```
2. Opposition movements or actions in duelling activities in a common space (e.g. tag and chase games)

```
Gets away from the opponent
(e.g. runs quickly away from the opponent)
```
```
a.
```
```
Reaches the opponent
(e.g. touches the opponent in a game of tag or in foam fencing)
```
```
b.
```
```
Dodges an attack by the opponent
(e.g. moves at the right time in order not to lose his/her flag)
```
```
c.
```

```
Deceives the opponent using a feint
(e.g. changes direction or speed in a game of one-on-one)
```
```
d.
```
3. Opposition movements or actions in duelling activities in separate spaces (e.g. badminton, mini‑tennis)

```
Uses the space available
(e.g. attacks the space left open by the opponent, gets back to the centre of his/her
game area)
```
```
a.
```
```
Recovers the object
(e.g. moves toward the point where the object will fall in order to recover it)
```
```
b.
```
```
Deceives the opponent using a feint
(e.g. changes the direction and path of the object)
```
```
c.
```
```
d. Catches the opponent wrongfooted
```
**C. Cooperation-opposition movements or actions**^123456

```
Cooperation-opposition movements or actions in group activities in a common space (e.g. mini-basketball, kinball,
capture the flag)
```
#### 1.

```
a. Attacks the other team’s goal^2
```
```
i. Throws or strikes the object, taking into account the distance to the target
```
```
ii. Throws or strikes the object, taking into account the opponents’ position
```
```
b. Keeps the object moving
```
```
i. Passes to an open or unguarded player
```
```
ii. Passes to the player in the best position in relation to the target
```
```
iii. Passes to the player in the best position in relation to opponents
```
```
iv. Counterattacks (reattacks quickly following a defensive action)
```
```
c. Moves the object forward
```
```
i. Moves toward the target
```
```
ii. Moves toward an open space
```
```
iii. Counterattacks
```
```
d. Recovers the object
```
```
Moves toward the point where the object will fall
(e.g. catches the rebound, recovers the ball after it hits the trampoline in
tchoukball)
```
```
i.
```
```
e. Gets free from an opponent or moves to an open space
```
```
i. Moves into an open space
```
```
ii. Moves into an open space in relation to partners
```
```
Moves away from an opponent
(e.g. makes a feint)
```
```
iii.
```
```
f. Falls back
```
```
i. Gets back in his/her defensive zone
```
```
g. Guards a player
```
```
Guards the carrier to prevent him/her from attacking the goal, passing the
object to a teammate or moving it forward
```
```
i.
```
```
ii. Guards the noncarrier to prevent him/her from receiving the object
```
```
h. Protects the goal (target)
```

```
Positions himself/herself between the object (carrier) and the target
(e.g. protects the key in mini-basketball, puts a wall in place in mini-handball)
```
```
i.
```
```
Stops throws or shots
(e.g. blocks, deflects, catches)
```
```
ii.
```
2. Cooperation-opposition movements or actions in group activities in separate spaces (e.g. mini-volleyball)

```
Keeps the object moving
(e.g. passes to a teammate, assumes a favourable attack position)
```
```
a.
```
```
Attacks by projecting the object into the opponents’ territory
(e.g. returns the ball to the opposite end of the playing area
```
```
b.
```
```
Attacks by projecting the object into an open space in the opponents’ territory
(e.g. serves a volleyball by taking advantage of space left open by the opposing
team)
```
```
c.
```
```
Protects his/her territory
(e.g. positions himself/herself in an assigned area to prevent the object from landing
in that area)
```
```
d.
```
```
Recovers the object
(e.g. moves quickly toward the point where the object will fall)
```
```
e.
```
1. The principles of synchronization have been included in this section in order to specify what the students should learn
    during cooperation activities. The other action rules mentioned in the program refer to opposition and cooperation-
    opposition movements and actions, which will be dealt with later on. Therefore, the few examples mentioned here refer
    only to cooperation activities (e.g. throwing and catching a ball and throwing passes).
2. These action rules (attack the other team’s goal, keep the object moving and move the object forward) refer to an
    expression in team sports known as “triple threat,” which consists in choosing one of the following options: shoot, pass or
    dribble the object forward.


## Physical Education and Health

## Competency 2: To interact with others in different physical activity

## settings

### Behaviour

```
Student constructs knowledge with teacher guidance.
```
```
Student applies knowledge by the end of the school year.
```
```
Student reinvests knowledge.
```
```
Elementary
```
```
Cycle
One
```
```
Cycle
Two
```
```
Cycle
Three
```
```
A. Ethics-related aspects^123456
```
```
Explains in his/her own words the rules of ethics relevant to a situation
(e.g. in a tag and chase game, refraining from using unfair tactics to win at all costs)
```
#### 1.

```
Names a few values that can be developed through games and sports
(e.g. respect, friendship, honesty)
```
#### 2.

3. Respects his/her peers (partners and opponents)

```
a. Uses language that shows respect for his/her partner
```
```
b. Cheers on his/her partners
```
```
c. Respects the point of view or ideas of others
```
```
d. Uses language that shows respect for opponents
```
```
Treats his/her opponents with respect
(e.g. refrains from using violent or aggressive behaviour)
```
```
e.
```
```
f. Accepts the mistakes of teammates
```
```
g. Helps partners who are having difficulty
```
4. Observes the rules
5. Respects the referee

```
a. Uses language that shows respect for the referee
```
```
b. Respects the referee’s decisions
```
6. Demonstrates fairness

```
Gives everyone a chance to play
(e.g. gives others a chance to get the object)
```
```
a.
```
```
Gives everyone a chance to win
(e.g. in a combat situation, confronts an opponent of the same skill level)
```
```
b.
```
```
Demonstrates a certain fighting spirit
(i.e. keeps trying, even in the face of defeat)
```
#### 7.

8. Shows a desire to surpass himself/herself
9. Accepts victory and defeat

```
Accepts defeat with dignity
(e.g. controls his/her emotions, gives opponents credit for good plays)
```
```
a.
```
```
Respects opponents in victory
(e.g. accepts victory with modesty without ridiculing the opponent)
```
```
b.
```
10. Shows appreciation for good plays by peers

```
a. Shows appreciation for the plays of his/her partner
```

```
b. Shows appreciation for the plays of his/her teammates and opponents
```
11. Demonstrates honesty in his/her behaviour
12. Demonstrates dignity and self-control


## Physical Education and Health

## Competency 3 – To adopt a healthy, active lifestyle

By using what they have learned while developing the two other competencies in the program, students can gradually
integrate a variety of physical activities into their daily lives. However, students need to acquire essential concepts if they
are to improve or maintain a healthy, active lifestyle. Physical Education and Health teachers must therefore help students
acquire a repertoire of knowledge regarding healthful lifestyle habits, the effects of a sedentary lifestyle and general
principles of human anatomy and physiology. It should be noted, though, that merely imparting knowledge about lifestyles
and health is not enough to motivate students to actively undertake a healthy and active lifestyle. Together with the school
team, parents and the community, teachers must develop activities that will enable students to actually take charge of their
own health and well-being.

```
Knowledge (lifestyle habits)
Knowledge (effects of a sedentary lifestyle)
Knowledge (anatomy and physiology of the human body)
```

## Physical Education and Health

## Competency 3: To adopt a healthy, active lifestyle

### Knowledge (lifestyle habits)

```
Student constructs knowledge with teacher guidance.
```
```
Student applies knowledge by the end of the school year.
```
```
Student reinvests knowledge.
```
```
Elementary
```
```
Cycle
One
```
```
Cycle
Two
```
```
Cycle
Three
```
```
A. Lifestyle habits 123456
```
```
Explains in his/her own words the concepts of lifestyle habits and health
(e.g. action often carried out every day for one’s health; state of physical and
psychological well-being)
```
#### 1.

```
Names lifestyle habits that are conducive to health and well-being
(e.g. regular physical activity)
```
#### 2.

```
Names lifestyle habits that are detrimental to health and well-being
(e.g. smoking, poor posture)
```
#### 3.

```
B. Regular physical activity^123456
```
```
Describes a few psychological benefits of his/her physical activity experience
(e.g. enjoyment, relaxation, feeling of competence)
```
#### 1.

```
Describes a few physiological benefits of his/her physical activity experience
(e.g. improved fitness, more energy)
```
#### 2.

```
Describes a few social benefits of his/her physical activity experience
(e.g. new friends, harmonious interpersonal relations, conflict management)
```
#### 3.

```
C. Safe participation in physical activity 123456
```
1. Explains in his/her own words the proper use of physical education equipment (light and heavy)

```
Explains the importance of using equipment safely in accordance with the type of
physical activity
(e.g. to prevent injury to oneself and to others)
```
```
a.
```
```
Names the safety rules for placing equipment
(e.g. placing mats at a safe distance from the wall or the work area of other teams)
```
```
b.
```
```
Names the safety rules for putting away equipment
(e.g. waiting one’s turn to put away one’s racket)
```
```
c.
```
```
Explains in his/her own words the importance of wearing appropriate clothing for a given
physical activity or context
(e.g. proper footwear, appropriate clothing for the weather)
```
#### 2.

```
Identifies a few exercises with a risk of injury
(e.g. hyperextension of the neck, bending upper body while keeping legs straight)
```
#### 3.

4. Recognizes potentially dangerous situations associated with a physical activity practised alone or with others

```
Identifies potentially dangerous situations
(e.g. untied shoelaces, wearing jewellery, improperly placed mat, heatstroke)
```
```
a.
```
```
Identifies behaviours to adopt
(e.g. waiting one's turn, respecting the maximum height allowed when rope climbing)
```
```
b.
```
```
Names safety rules to observe in different physical activity settings
(e.g. no running on the pool deck)
```
#### 5.

6. Identifies the stages of a physical activity session

```
Names the stages of a physical activity session
(e.g. warm-up, performance, cool-down)
```
```
a.
```
```
Explains in his/her own words the importance of warming up before a physical
activity
(e.g. to increase body temperature, prepare the muscles for a more strenuous
activity)
```
```
b.
```
```
Explains in his/her own words the importance of cooling down after a physical
activity
(e.g. to improve recovery, gradual transition to state of rest)
```
```
c.
```

```
Explains in his/her own words the importance of pacing oneself, depending on the activity
(e.g. to vary one’s speed while running, in order to control one’s breathing)
```
#### 7.

**D. Physical fitness (influencing factors)**^123456

1. Flexibility^1
    Explains in his/her own words the importance of stretching
    (e.g. to increase the range and ease of motion of a muscle, prevent and relieve
    muscle soreness)

```
a.
```
```
Performs stretching exercises
(e.g. stretches arm, thigh muscles)
```
```
b.
```
```
Explains in his/her own words a few principles of stretching effectively and safely
(e.g. appropriate time, steps to follow, proper position, duration)
```
```
c.
```
2. Posture^2
    Adopts proper posture while sitting on the floor and while standing
    (e.g. sitting cross-legged)

```
a.
```
```
Identifies postures that are beneficial (e.g. standing straight, shoulders relaxed) or
harmful (e.g. lifting a load without bending at the knees) depending on the type of
activity
```
```
b.
```
```
Explains in his/her own words the importance of proper posture
(e.g. to prevent unnecessary muscle tension and back pain)
```
```
c.
```
3. Cardiovascular endurance

```
Recognizes the level of intensity depending on the type of physical activity
(e.g. slow walking = low intensity, cross-country skiing = moderate to high intensity)
```
```
a.
```
```
Makes recommendations in his/her own words for improving or maintaining
cardiovascular endurance
(e.g. type of activity, nature [continuous, discontinuous], intensity, duration and
frequency)
```
```
b.
```
```
Explains in his/her own words the importance of cardiovascular exercise
(e.g. the heart is a muscle and it must be kept in shape; less breathlessness)
```
```
c.
```
```
d. Measures his/her heart rate at the wrist (radial artery) or neck (carotid artery)
```
```
Explains in his/her own words the importance of measuring one’s heart rate before,
during and after physical activity
(e.g. to pace oneself, determine the time needed to recover)
```
```
e.
```
```
Explains in his/her own words how to pace oneself depending on the physical
activity, its duration, intensity or distance to be covered
(e.g. to make sustained, even effort; save some energy to finish the activity)
```
```
f.
```
4. Muscular strength and endurance

```
Explains in his/her own words the importance of muscular exercises
(e.g. to tone muscles, work the entire body, help prevent injury)
```
```
a.
```
```
Performs a few muscular exercises in the upper and lower limbs
(e.g. arm extensions, sit-ups)
```
```
b.
```
```
c. Distinguishes between muscular exercises and cardiovascular exercises
```
```
E. Personal hygiene related to physical activity^3123456
Explains in his/her own words the importance of changing one’s clothes after engaging in
physical activity
```
#### 1.

```
Explains in his/her own words the importance of washing one’s clothes after engaging in
physical activity
```
#### 2.

```
Explains in his/her own words the importance of washing oneself after engaging in
physical activity
```
#### 3.

```
F. Relaxation and stress management^123456
```
```
Explains in his/her own words a few benefits of relaxation
(e.g. it reduces muscle tension, helps the body recover after a physical effort)
```
#### 1.

```
Performs some physical relaxation exercises
(e.g. lies down in resting position, breathes deeply [abdominal breathing], contracts and
relaxes muscles)
```
#### 2.

```
Performs a few mental relaxation exercises
(e.g. visualization)
```
#### 3.


```
Explains positive and negative forms of stress in his/her own words
(e.g. response to a situation perceived as stressful or stimulating)
```
#### 4.

```
Names some sources of stress in everyday life
(e.g. sports competition, conflict, exams, bullying)
```
#### 5.

```
Explains in his/her own words the importance of managing one’s stress
(e.g. to better adapt to situations, have better self-control, develop better relations with
others)
```
#### 6.

```
Names some stress management strategies
(e.g. engaging in physical activity, laughing, performing relaxation exercises, playing with
friends)
```
#### 7.

1. Because children under 12 do not necessarily need to stretch before engaging in physical activity, the point here is to
    introduce them to the concept and get them in the habit of stretching in order to prepare them for more elaborate
    stretching exercises in secondary school.
2. In the Physical Education and Health program, posture is one of the four influencing factors of physical fitness. The
    objective is to make students aware of the importance of developing good posture in order to prevent neck and back
    pain, which can occur in children as young as six to 12 years of age.
3. The concepts related to this lifestyle habit could be introduced in Cycle One.


## Physical Education and Health

## Competency 3: To adopt a healthy, active lifestyle

### Knowledge (effects of a sedentary lifestyle)

```
Student constructs knowledge with teacher guidance.
```
```
Student applies knowledge by the end of the school year.
```
```
Student reinvests knowledge.
```
```
Elementary
```
```
Cycle
One
```
```
Cycle
Two
```
```
Cycle
Three
```
```
A. Effects of a sedentary lifestyle 123456
```
```
Explains in his/her own words what “sedentary lifestyle” means
(e.g. not doing any physical activity, spending a lot of time in front of a computer)
```
#### 1.

```
Explains in his/her own words a few of the effects of a sedentary lifestyle on weight
(e.g. causes weight gain, diseases such as diabetes)
```
#### 2.

```
Explains in his/her own words a few of the effects of a sedentary lifestyle on flexibility
(e.g. loss of flexibility)
```
#### 3.

```
Explains in his/her own words a few of the effects of a sedentary lifestyle on muscle mass
(e.g. loss of muscle tone and strength)
```
#### 4.

```
Explains in his/her own words a few of the effects of a sedentary lifestyle on
cardiovascular endurance
(e.g. shortness of breath, cramps, lack of endurance during a race)
```
#### 5.


## Physical Education and Health

## Competency 3: To adopt a healthy, active lifestyle

### Knowledge (anatomy and physiology of the human body)

```
Student constructs knowledge with teacher guidance.
```
```
Student applies knowledge by the end of the school year.
```
```
Student reinvests knowledge.
```
```
Elementary
```
```
Cycle
One
```
```
Cycle
Two
```
```
Cycle
Three
```
```
A. Anatomy 123456
```
1. Distinguishes a bone from a muscle
2. Locates the heart and lungs

```
Identifies the main joints in the body
(e.g. elbows, knees, ankles)
```
#### 3.

```
B. Physiology^123456
```
1. Cardiovascular system

```
Explains in his/her own words the main function of the cardiovascular system during
physical activity
```
```
a.
```
2. Respiratory system

```
Explains in his/her own words the main function of the respiratory system during
physical activity
```
```
a.
```
3. Muscular system

```
Explains in his/her own words the overall function of the muscular system during
physical activity
```
```
a.
```
4. Body’s response to exercise

```
Identifies the body’s response during or after physical activity
(e.g. heat, perspiration, shortness of breath, increased heart rate, muscular fatigue)
```
```
a.
```

