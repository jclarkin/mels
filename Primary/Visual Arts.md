# Progression of Learning

# Visual Arts

## August 24, 2009


## Table of Contents

### Introduction 3

### Know ledge 4

### Applications of Know ledge 7

### Competency 1 – To produce individual works in the visual arts 7

### Competency 2 – To produce media works in the visual arts 9

### Competency 3 – To appreciate works of art, traditional artistic objects, media images,

### personal productions and those of classmates 11


## Visual Arts

## Introduction

In order to produce individual and media works in the visual arts and to appreciate works of art, students must acquire a
certain amount of knowledge related to visual arts language as well as to transforming gestures and the tools that serve as
extensions of them. Presented schematically in the program as essential knowledges, this learning is addressed here in
order to facilitate teachers’ planning. It is presented in four tables. The first table covers knowledge that students should
have acquired by the end of each cycle. The other three tables illustrate, by means of observable actions, how this
knowledge is mobilized in the exercise of each of the three competencies developed in the program. Related to the key
features of the competencies, the action verbs used in each statement show the progression of learning from one cycle to
the next. Teachers will be better equipped to ensure students’ competency development if they include in their planning
simple or complex tasks aimed at the acquisition and application of different items of knowledge in a given context.

Since competency development and acquisition of the knowledge underlying the competency are closely related, the
particulars contained in this document should enable teachers to help students acquire the tools they need to develop
each of the program’s competencies and to discover their artistic sensitivity and their creative potential.

Throughout elementary school, students in the Visual Arts program become familiar with the creative process by using
various elements of knowledge to produce their own images. They also use this knowledge to produce images for intended
viewers which, in some cycles, must include a visual message, adding to students’ cultural experience. Lastly, they learn to
express themselves using the appropriate subject-specific vocabulary and acquire the skills they need to exercise critical
judgment when appreciating a work of art, a traditional artistic object, a media image or a classmate’s production.

The elementary-level Arts Education programs were designed to ensure the progression of learning in each subject area

from the first to the sixth years. However, since continuity is required only for one of the two arts subjects,^1 the second
subject may not be offered continuously throughout elementary school. In such a case, it is important to provide students
with as complete an arts education as possible, taking their abilities into account. For example, if the visual arts course is
offered in one cycle only, teachers should make an effort to help students acquire not only the knowledge associated with
that cycle, but any other knowledge deemed essential. This knowledge appears as statements in bold type.

```
In short, by progressively acquiring the knowledge outlined in this document, students will develop the competencies
presented in the Visual Arts program. The tables will allow teachers to provide students with the conditions necessary for
competency development at the elementary level.
```
1. The _Basic school regulation for preschool, elementary and secondary education_ stipulates that two of the four arts
    subjects (Drama, Visual Arts, Dance and Music) are compulsory at the elementary level. According to these obligations,
    one of the two subjects taught in Cycles Two and Three must be the same as one of those taught in Cycle One.


## Visual Arts

## Knowledge

In their planning, teachers should include a variety of simple tasks (diverse exploration and experimentation activities) to
help students acquire and apply the knowledge in this table.

```
Student constructs knowledge with teacher guidance.
```
```
Student applies knowledge by the end of the school year.
```
```
Student reinvests knowledge.
```
```
The statements in bold type indicate knowledge that should be given priority if the program is not taught in all
cycles.
```
```
Elementary
```
```
Cycle
One
```
```
Cycle
Two
```
```
Cycle
Three
```
#### A. Transforming gestures and tools 123456

1. **Transforming gestures**^1 **,**^2
    **Names the following transforming gestures: gluing,** tearing, **cutting out,**
    **drawing,** printing, **modelling and painting**

```
a.
```
```
b. Identifies transforming gestures, including engraving
```
```
c. Identifies transforming gestures, including assembling and shaping
```
```
d. Differentiates among transforming gestures
```
2. **Techniques**

```
a. Names the following techniques: collage, drawing, modelling and painting
```
```
b. Identifies techniques, including engraving and printing
```
```
c. Identifies techniques, including assembling and shaping
```
```
d. Differentiates among techniques
```
3. **Materials**

```
Names the following materials: crayon, felt pen, gouache, paper and
cardboard, oil pastel and modelling clay
```
```
a.
```
```
b. Identifies materials, including clay, charcoal and dry pastel
```
```
c. Identifies materials, including ink
```
```
d. Differentiates among materials
```
4. **Tools**

```
a. Names the following tools: brush, scissors, mouse and electronic pen
```
```
b. Identifies tools, including a sponge
```
```
c. Identifies tools, including a brush
```
```
d. Differentiates among tools
```
#### B. Language of visual arts 123456

1. **Shape**

```
a. Names rounded or angular shapes
```
```
b. Identifies rounded or angular shapes
```

```
c. Differentiates between rounded and angular shapes
```
2. **Line**

```
a. Names thick and thin lines
```
```
b. Identifies lines, including horizontal, vertical, short and long lines
```
```
c. Identifies lines, including curved, straight, oblique, broken and circular lines
```
```
d. Differentiates among lines
```
3. **Colours of pigments**

```
a. Names the primary colours: cyan, primary yellow and magenta
```
```
b. Identifies the secondary colours: orange, green and violet
```
```
Identifies the warm colours: yellow, orange and magenta, and the cool
colours: cyan, green and violet
```
```
c.
```
```
d. Differentiates among colours
```
4. **Value**

```
a. Names light and dark values
```
```
b. Identifies light and dark values
```
```
c. Differentiates among values
```
5. **Texture**

```
a. Names some textures
```
```
b. Identifies some textures
```
```
c. Differentiates among textures
```
6. **Pattern**

```
a. Names some patterns
```
```
b. Identifies some patterns
```
```
c. Differentiates patterns
```
7. **Volume**

```
a. Names three-dimensional forms
```
```
b. Identifies three-dimensional forms
```
```
c. Differentiates among three-dimensional forms
```
8. **Spatial organization**

```
Names ways of organizing elements in space: enumeration, juxtaposition,
repetition and alternance
```
```
a.
```
```
Identifies ways of organizing elements in space, including superimposition,
symmetry and asymmetry
```
```
b.
```
```
c. Differentiates among ways of organizing elements in space
```
9. **Spatial representation**

```
Identifies ways of representing elements in space: perspective with
overlapping
```
```
a.
```

```
Identifies ways of representing elements in space, including perspective
with vanishing point
```
```
b.
```
```
c. Differentiates among ways of representing elements in space
```
#### C. Visual arts appreciation repertoire 123456

1. **Visual arts productions**^3
1. Since these elements are evident in action, they are included in the _Applications of Knowledge_ section_._
2. In Visual Arts, students transform materials with the aid of tools, using gestures that they learn to control during the
course of elementary school. The heading _Transforming Gestures and Tools_ reflects this reality.
3. Since these elements are evident in action, they are included in the _Applications of Knowledge_ section_._


## Visual Arts

## Applications of Knowledge

### Competency 1 – To produce individual works in the visual arts

```
Student constructs knowledge with teacher guidance.
```
```
Student applies knowledge by the end of the school year.
```
```
Student reinvests knowledge.
```
```
The statements in bold type indicate knowledge that should be given priority if the program is not taught in all
cycles.
```
```
Elementary
```
```
Cycle
One
```
```
Cycle
Two
```
```
Cycle
Three
```
#### A. To use personal ideas inspired by the stimulus for creation 123456

```
Looks for an idea related to the stimulus for creation while consulting sources of
information
```
```
a.
```
```
Looks for a few ideas related to the stimulus for creation while consulting
sources of information
```
```
b.
```
```
Looks for a variety of ideas related to the stimulus for creation while consulting
sources of information
```
```
c.
```
```
d. Chooses an idea that represents his/her perception of reality
```
```
e. Makes a sketch of his/her idea
```
```
f. Makes a few sketches of his/her idea
```
```
g. Makes sketches representing variations on his/her idea
```
#### B. To use transforming gestures and elements of visual arts language^123456

1. **Gestures and tools**^1
    **Experiments with transforming gestures such as: freehand drawing,**
    **applying coloured pigments with flat brushstrokes, tearing, notching, cutting**
    **out, spreading glue on a surface, joining and pinching a malleable material**

```
a.
```
```
Experiments with transforming gestures such as: applying coloured
pigments with flat brushstrokes, varied brushstrokes, and intaglio printing
```
```
b.
```
```
Experiments with transforming gestures such as: applying coloured
pigments with flat brushstrokes, varied brushstrokes and additional varied
brushstrokes
```
```
c.
```
```
d. Uses transforming gestures that represent his/her idea
```
```
e. Uses transforming gestures that clarify his/her idea
```
```
f. Handles the following tools: brush, scissors, mouse and electronic pen
```
```
g. Handles tools, including a sponge
```
```
h. Handles tools, including a paintbrush
```
2. **Language of visual arts**

```
Experiments with elements of visual arts language: shape, line, colour,
value, texture, pattern, volume
```
```
a.
```
```
b. Uses elements of visual arts language
```
```
c. Uses a variety of elements of visual arts language
```
```
d. Uses a combination of elements of visual arts language
```
#### C. To organize the elements he/she has chosen 123456


```
Uses the following ways of organizing space: enumeration, juxtaposition,
repetition and alternance
```
```
a.
```
```
Uses ways of organizing space, including superimposition, symmetry and
asymmetry
```
```
b.
```
```
c. Uses the following way of representing space: perspective with overlapping
```
```
Uses the following ways of representing space: perspective with
overlapping and perspective with vanishing point
```
```
d.
```
#### D. To finalize his/her production 123456

```
Makes adjustments to certain transforming gestures and to the language of
visual arts
```
```
a.
```
```
b. Adjusts his/her production based on the initial stimulus for creation
```
```
Enhances certain aspects of his/her production using transforming gestures,
elements of visual arts language, spatial organization and representation
```
```
c.
```
#### E. To share his/her creative experience 123456

```
Shares significant aspects of his/her experience with transforming gestures and
elements of visual arts language
```
```
a.
```
```
Describes the important aspects of his/her experience with transforming
gestures and elements of visual arts language
```
```
b.
```
```
c. Uses subject-specific vocabulary
```
1. The complete list of transforming gestures and materials for each cycle can be found in the _Essential Knowledges_ table
    in the Visual Arts program.


## Visual Arts

## Applications of Knowledge

### Competency 2 – To produce media works in the visual arts

```
Student constructs knowledge with teacher guidance.
```
```
Student applies knowledge by the end of the school year.
```
```
Student reinvests knowledge.
```
```
The statements in bold type indicate knowledge that should be given priority if the program is not taught in all
cycles.
```
```
Elementary
```
```
Cycle
One
```
```
Cycle
Two
```
```
Cycle
Three
```
#### A. To use creative ideas inspired by a stimulus for creation of media works 123456

```
Looks for an idea related to the stimulus for creation of media works, taking
the intended viewers into account and referring to sources of information
```
```
a.
```
```
Looks for a few ideas related to the stimulus for creation of media works,
taking the message and intended viewers into account
```
```
b.
```
```
Looks for a variety of ideas related to the stimulus for creation of media works, taking
the message and intended viewers into account
```
```
c.
```
```
Chooses an idea based on an intended viewer in his/her immediate
environment
```
```
d.
```
```
e. Chooses an idea based on the message and intended viewers
```
```
f. Makes a sketch of his/her idea
```
```
g. Makes a few sketches of his/her idea
```
```
h. Makes sketches representing variations on his/her idea
```
#### To use transforming gestures and elements of visual arts language

#### according to the message (Cycles Two and Three) and the intended viewer

#### B.

##### 123456

1. **Gestures and tools**^1
    **Experiments with transforming gestures such as: freehand drawing,**
    **applying coloured pigments with flat brushstrokes, tearing, notching, cutting**
    **out, spreading glue on a surface, joining and pinching a malleable material**

```
a.
```
```
Experiments with transforming gestures such as: applying coloured
pigments with flat brushstrokes, varied brushstrokes, and intaglio printing
```
```
b.
```
```
Experiments with transforming gestures such as: applying coloured
pigments with flat brushstrokes, varied brushstrokes and additional varied
brushstrokes
```
```
c.
```
```
d. Uses transforming gestures that convey his/her idea to the intended viewers
```
```
Uses transforming gestures that clarify the visual message intended for
viewers
```
```
e.
```
```
f. Handles the following tools: brush, scissors, mouse and electronic pen
```
```
g. Handles tools, including a sponge
```
```
h. Handles tools, including a paintbrush
```
2. **Language of visual arts**

```
Experiments with elements of visual arts language: shape, line, colour,
value, texture, pattern, volume
```
```
a.
```
```
b. Chooses elements of visual arts language based on the intended viewers
```
```
Chooses elements of visual arts language based on the visual message and
intended viewers
```
```
c.
```
```
d. Uses elements of visual arts language based on the intended viewers
```

```
Uses a variety of elements of visual arts language based on the visual message and
intended viewers
```
```
e.
```
```
Uses a combination of elements of visual arts language based on the visual
message and intended viewers
```
```
f.
```
#### To organize the elements that he/she has chosen, depending on the

#### message (Cycles Two and Three) and the intended viewer

#### C.

##### 123456

```
Uses the following ways of organizing space based on the intended viewers:
enumeration, juxtaposition, repetition and alternance
```
```
a.
```
```
Uses the following ways of organizing space based on the visual message
and intended viewers: superimposition, symmetry and asymmetry
```
```
b.
```
```
Uses the following way of representing space based on the visual message and
intended viewers: perspective with overlapping
```
```
c.
```
```
Uses the following ways of representing space based on the visual message and
intended viewers: perspective with overlapping and perspective with vanishing point
```
```
d.
```
#### D. To finalize his/her media creation^123456

```
a. Validates a control group’s understanding of the visual message
```
```
Validates his/her choices using a control group (e.g. message, cultural references,
colours, organization of elements)
```
```
b.
```
```
Makes adjustments to certain transforming gestures and to the language of
visual arts based on the intended viewers
```
```
c.
```
```
Adjusts his/her production based on the initial stimulus for creation, visual
message and intended viewers
```
```
d.
```
```
Enhances certain aspects of his/her production using transforming gestures,
elements of visual arts language, and spatial organization and representation based
on the visual message and intended viewers
```
```
e.
```
#### E. To share his/her experience of media creation^123456

```
Shares significant aspects of his/her experience with transforming gestures and the
elements of visual arts language
```
```
a.
```
```
Describes the important aspects of his/her experience with transforming
gestures and the elements of visual arts language
```
```
b.
```
```
c. Uses subject-specific vocabulary
```
1. The complete list of transforming gestures and materials for each cycle can be found in the _Essential Knowledges_ table
    in the Visual Arts program.


## Visual Arts

## Applications of Knowledge

### Competency 3 – To appreciate works of art, traditional artistic objects, media images, personal

### productions and those of classmates

```
Student constructs knowledge with teacher guidance.
```
```
Student applies knowledge by the end of the school year.
```
```
Student reinvests knowledge.
```
```
The statements in bold type indicate knowledge that should be given priority if the program is not taught in all
cycles.
```
```
Elementary
```
```
Cycle
One
```
```
Cycle
Two
```
```
Cycle
Three
```
#### To examine a work of art, traditional artistic object, media images, personal

#### or media visual arts production for elements of content

#### A.

##### 123456

```
a. Observes some subject-specific elements in student productions
```
```
Observes some subject-specific elements in works of art past and present, from here
and elsewhere
```
```
b.
```
1. **Language of visual arts**

```
Observes the elements of visual arts language: shape, line, colour, value, texture,
pattern, volume
```
```
a.
```
```
Locates the elements of visual arts language related to the proposed
appreciation criteria
```
```
b.
```
```
c. Locates the visual effects obtained using elements of visual arts language
```
```
Observes the organization of elements in a two- or three-dimensional space:
enumeration, juxtaposition, repetition, alternance
```
```
d.
```
```
Locates the organization of elements in a two- or three-dimensional space,
including superimposition, symmetry and asymmetry
```
```
e.
```
```
Locates the representation of elements in a two-dimensional space:
perspective with overlapping
```
```
f.
```
```
Locates the representation of elements in a two-dimensional space, including
perspective with vanishing point
```
```
g.
```
2. **Gestures**

```
a. Observes evidence of gestures used to produce the image
```
#### To examine a work of art, traditional artistic object or media images for

#### sociocultural references (Cycles Two and Three)

#### B.

##### 123456

```
a. Locates elements representing sociocultural aspects
```
#### C. To make connections between what he/she has felt and examined^123456

```
Names an element in the image that elicited an emotion, feeling or
impression
```
```
a.
```
```
b. Explains why this element elicited a reaction
```
```
c. Uses subject-specific vocabulary
```
#### D. To make a critical or aesthetic judgment 123456

```
a. Expresses his/her preferences based on his/her observations
```
```
b. Explains his/her preferences based on his/her observations
```
```
c. Justifies his/her point of view based on his/her observations
```
```
Compares works of art and productions based on his/her observations and the
proposed appreciation criteria
```
```
d.
```

```
e. Uses subject-specific vocabulary
```
#### E. To share his/her appreciation experience 123456

```
Shares significant aspects related to the appreciation of transforming
gestures and elements of visual arts language
```
```
a.
```
```
Describes important aspects related to the appreciation of transforming
gestures and elements of visual arts language
```
```
b.
```
```
c. Uses subject-specific vocabulary
```

