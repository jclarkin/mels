# Progression of Learning

[MELS Source](http://www.education.gouv.qc.ca/fileadmin/site_web/documents/education/jeunes/pfeq/PDA_PFEQ_univers-social_2009_EN.pdf)

# Geography, History and Citizenship

## August 24, 2009


## Table of Contents

- Introduction
- Know ledge related to the organization of a society in its territory
- Know ledge related to change in a society and its territory
- Know ledge related to the diversity of societies and their territories
- Researching and w orking w ith information in geography and history
- Techniques specific to geography and history


## Geography, History and Citizenship Education

## Introduction

This document is complementary to the Geography, History and Citizenship Education program. It aims to provide information about the knowledge students must acquire in geography, history and citizenship education in order to develop the competencies. It is intended to help teachers plan their teaching. It contains tables of the knowledge specific to the societies and territories studied. The knowledge is divided into three sections corresponding to the program’s three competencies. Each section is preceded by a short text presenting the learning students must acquire. The document also contains a table of the knowledge related to researching and working with information in geography and history and a table of the knowledge related to techniques specific to these subjects.

In Cycle One, students develop only one competency: _To construct his/her representation of space, time and society. To ensure continuity with Cycle Two, the knowledge students must acquire is presented at the beginning of each section. In Cycle One, students look at their own environment, here and now. The targeted knowledge is therefore based on their observation of the everyday objects, people and landscapes around them. They then compare their environment with a former environment (past) and a less familiar environment (elsewhere). Their understanding of the world, which they began constructing in preschool, thus continues with the development of the concepts of space, time and society.

In Cycles Two and Three, students study societies between 1500 and 1980. They acquire knowledge, related, for example, to demographics, economics, political organization and the assets and limitations of the territory. By relating these elements of knowledge, students develop the competency _To understand the organization of a society in its territory._ A research method and techniques specific to geography and history support the acquisition of knowledge.

The knowledge acquired about each society helps students develop a representation of its economy, culture and politics, which they then use to study the changes in a society. Thus, students learn To interpret change in a society and its territory. The new elements of knowledge are therefore based on their previous learning.

Lastly, students apply and enhance their knowledge by comparing certain aspects of the societies studied with those of another society during the same period. Based on a core of knowledge, students develop the competency To be open to the diversity of societies and their territories.

The Geography, History and Citizenship Education program aims to help young people become responsible and informed citizens. The knowledge acquired serves as a basis for a humanistic culture that will be developed throughout students’ schooling.

* [Kurzgesagt: Time: The History & Future of Everything – Remastered](https://www.youtube.com/watch?v=5TbUxGZtwGI)
* [Kurzgesagt: A New History for Humanity – The Human Era](https://www.youtube.com/watch?v=czgOWmtGVGs)
* [Kurzgesagt: What Happened Before History? Human Origins](https://www.youtube.com/watch?v=dGiQaabX3_o)
* [Heritage Minutes Canada: Playlist](https://www.youtube.com/watch?v=9F23fgzNbO4&list=PL1848FF9428CA9A4A)
* [Homeschool Pop: Continents of the World](https://www.youtube.com/watch?v=YrT5jcnu8NA)
* [Homeschool Pop: Oceans of the World](https://www.youtube.com/watch?v=q-up6zuCQQg)
* [Homeschool Pop: Famous Landmarks for Kids](https://www.youtube.com/watch?v=Uh332gXQYI4)
* [Khan Academy: World History](https://www.khanacademy.org/humanities/ap-world-history)

## Geography, History and Citizenship Education

## Know ledge related to the organization of a society in its territory

In Cycle One, students become familiar with the concept of organization. Based on their knowledge of their environment,
the people they know and the groups they belong to, they continue the process of developing their representation of
space, time and society, which they began in preschool. In Cycles Two and Three, they begin to understand the
organization of a society in its territory. They acquire knowledge related to the society’s location in space and time; its
demographic, cultural, economic and political characteristics; its adaptation to the territory, with its assets and limitations;
the people, groups and events that have marked its history; and the traces it has left on Québec society today.

```
Legend^1
Student constructs knowledge with teacher guidance.
```
```
Student applies knowledge by the end of the school year.
```
```
Elementary
```
```
Cycle
One
```
```
Cycle
Two
```
```
Cycle
Three
Today
First representation of a society (Cycle One)
```
```
A.
123456
```
1. **Location in space and time**^2
    Orients himself/herself in space, a simple drawing, an illustration or a scale model
    (e.g. my bicycle is in front of the garage, my swing is to the left of the tree, the post
    office is north of the playground)

```
a.
```
```
Orients himself/herself in space using the points of the compass (e.g. my house is
south of the lake, my swing is west of the tree)
```
```
b.
```
```
c. Orients himself/herself in time: calendar, day, month, year
```
```
Situates events in his/her life and that of friends and family on a graduated time line
(e.g. my birth, my first day of school, a trip, my parents’ births, the death of a friend
or relative)
```
```
d.
```
```
Indicates physical traits characteristic of different ages (e.g. a baby has no teeth,
grandparents sometimes have white hair)
```
```
e.
```
```
Indicates activities associated with different ages (e.g. children go to school, adults
can drive)
```
```
f.
```
2. **Human elements**

```
2.1. Demographic situation
```
```
a. Names groups he/she belongs to (e.g. family, friends, sports teams, class)
```
```
Describes the composition of the groups he/she belongs to (e.g. my soccer team is
made up of boys and girls and an adult: the coach; my class is made up of boys and
girls and an adult: the teacher)
```
```
b.
```
```
Indicates the number of members in the groups he/she belongs to (e.g. there are 10
players on my soccer team, there are 24 students in my class)
```
```
c.
```
```
2.2. Cultural situation
```
```
a. Names the language(s) spoken in his/her environment (e.g. French, English, Italian)
```
```
Names the religion(s) practised in his/her environment (e.g. Catholicism,
Protestantism)
```
```
b.
```
```
c. Names artistic expressions (e.g. painting, sculpture)
```
```
d. Names everyday objects (e.g. toys, CDs, iron, computer, bicycle, car)
```
```
2.3. Economic situation
```
```
Names elements of the landscape related to economic activities (e.g. farm, factory,
bank, port)
```
```
a.
```
```
b. Names needs satisfied by economic activities (e.g. food, entertainment)
```
```
Names means of transportation and transportation routes (e.g. car, train, airplane;
highway, road, railway)
```
```
c.
```

```
2.4. Political situation
```
```
a. Names institutions (e.g. city hall, municipal council)
```
```
Indicates rules of order for groups he/she belongs to (e.g. in class students must
raise their hand before they speak)
```
```
b.
```
3. **Natural elements**

```
a. Names types of relief (e.g. plain, valley, plateau, hill, mountain chain)
```
```
b. Names elements of climate (e.g. precipitation, temperature)
```
```
c. Names bodies of water (e.g. river, lake)
```
```
d. Names natural resources (e.g. forest, water, fertile soil, minerals)
```
4. **People, groups and events**

```
a. Names people in his/her environment (e.g. father, friend, teacher, coach)
```
```
Describes the roles of different members of the groups he/she belongs to (e.g. the
teacher transmits knowledge, the captain of my team encourages us)
```
```
b.
```
```
Names events in his/her life and that of friends and family (e.g. birth, first day of
school, move, parents’ births, death of a friend or relative)
```
```
c.
```
**B. Iroquoian society around 1500**^123456

1. **Location of the society in space and time**

```
Locates on a map the territory occupied by Iroquoian society: St. Lawrence and
Great Lakes lowlands
```
```
a.
```
```
Locates on a graduated time line events and people related to the history of the
society (e.g. Columbus’s discovery of America, Cabot’s voyages, Cartier’s voyages,
Donnacona)
```
```
b.
```
2. **Elements of the society that affect the organization of the territory**

```
2.1. Demographic situation
```
```
Describes the distribution of the population: along the St. Lawrence and in the Great
Lakes region
```
```
a.
```
```
b. Indicates the way of life: sedentary
```
```
c. Gives the approximate number of inhabitants
```
```
2.2. Cultural situation
```
```
a. Names a spiritual element: animism
```
```
b. Names artistic expressions (pottery, basket weaving)
```
```
c. Describes elements of everyday life: food, clothing, entertainment, customs
```
```
2.3. Economic situation
```
```
a. Names economic activities: agriculture, hunting, fishing, gathering, barter
```
```
b. Names means of transportation: canoe, snowshoes
```
```
c. Indicates transportation routes: waterways, forest trails
```
```
2.4. Political situation
```
```
a. Indicates means of selecting leaders: women elders appointed chiefs
```

```
b. Indicates means of decision making: council
```
3. **Assets and limitations of the territory**

```
a. Indicates assets related to the relief (e.g. plains were good for farming)
```
```
Indicates assets and limitations related to climate (e.g. the temperature and rain in
the summer were good for farming; the temperature and snow in the winter limited
activities and travel)
```
```
b.
```
```
Indicates assets and limitations related to bodies of water (e.g. rivers and lakes
facilitated access to the territory; rapids limited travel)
```
```
c.
```
```
Explains why resources were assets (e.g. forests provided construction materials for
longhouses and canoes; animals were used for food)
```
```
d.
```
4. **Influence of people on social and territorial organization**

```
a. Names a group that played a role in selecting leaders: women elders
```
5. **Elements of continuity with the present**

```
a. Indicates traces left by Iroquoian society: place names, artefacts, sites
```
**C. French society in New France around 1645 123456**

1. **Location of the society in space and time**

```
Locates on a map the territory that belonged to France in North America: the St.
Lawrence Valley and the Great Lakes region
```
```
a.
```
```
Locates on a map the territory occupied by French society in New France: the St.
Lawrence Valley
```
```
b.
```
```
Situates on a graduated time line events and people related to the history of the
society (e.g. founding of Québec City, Trois-Rivières and Montréal; explorations and
explorers; Champlain, Laviolette, Maisonneuve)
```
```
c.
```
2. **Elements of the society that affect the organization of the territory**

```
2.1. Demographic situation
```
```
a. Describes the distribution of the population: concentrated in the St. Lawrence Valley
```
```
b. Describes the composition of the population: Native peoples, French
```
```
c. Gives the approximate number of inhabitants
```
```
2.2. Cultural situation
```
```
a. Names languages spoken: Native languages, French
```
```
b. Names religions practised: Native spiritualities, Catholicism
```
```
c. Names artistic expressions (e.g. painting, architecture, embroidery)
```
```
d. Describes elements of everyday life: food, clothing, entertainment, customs
```
```
2.3. Economic situation
```
```
a. Names economic activities: fur trade, agriculture, hunting and fishing
```
```
b. Names means of transportation: canoe, boat, cart
```
```
c. Indicates transportation routes: waterways, forest trails, early roads
```
```
2.4. Political situation
```
```
a. Indicates the means of decision making: made unilaterally by the king of France
```

```
Indicates the means of selecting leaders: the king appointed the governor and the
company
```
```
b.
```
```
c. Names an institution: the Company of One Hundred Associates
```
```
Indicates one obligation and one privilege of the Company: the Company was
obliged to populate the colony; the Company was given the monopoly of the fur
trade
```
```
d.
```
3. **Assets and limitations of the territory**

```
a. Indicates assets related to the relief (e.g. plains were good for farming)
```
```
Indicates assets and limitations related to climate (e.g. the temperature and rain in
the summer were good for farming; the temperature and snow in the winter limited
activities and travel)
```
```
b.
```
```
Indicates assets and limitations related to bodies of water (e.g. confluences favoured
the establishment of trading posts; rapids limited travel)
```
```
c.
```
```
Explains why resources were assets (e.g. the abundance of beavers enabled the
development of the fur trade)
```
```
d.
```
4. **Influence of people and events on social and territorial organization**

```
a. Names important people: Champlain, Laviolette, Maisonneuve
```
```
Names groups that played a role: Native peoples, religious groups (e.g. Jesuits),
coureurs de bois, companies
```
```
b.
```
```
Indicates events that marked society: first settlements, Iroquois wars, explorations,
creation of trading posts
```
```
c.
```
5. **Elements of continuity with the present**

```
Indicates traces left by the society (e.g. language, religion, customs and traditions,
place names)
```
```
a.
```
**D. Canadian society in New France around 1745**^123456

1. **Location of the society in space and time**

```
Locates on a map the territory that belonged to France in North America: the St.
Lawrence and Great Lakes lowlands, Ohio and Mississippi valleys to Louisiana
```
```
a.
```
```
Locates on a map the territory occupied by Canadian society in New France: the St.
Lawrence Valley
```
```
b.
```
```
Situates on a graduated time line events and people related to the history of the
society (e.g. Great Peace of Montréal, intendancy of Gilles Hocquart)
```
```
c.
```
2. **Elements of the society that affect the organization of the territory**

```
2.1. Demographic situation
```
```
Describes the distribution of the population: concentrated in the St. Lawrence Valley,
in particular at Québec City, Trois-Rivières and Montréal
```
```
a.
```
```
b. Describes the composition of the population: Native peoples, French, Canadians
```
```
c. Gives the approximate number of inhabitants
```
```
2.2. Cultural situation
```
```
a. Names languages spoken: Native languages, French
```
```
b. Names the religions practised: Native spiritualities, Catholicism
```
```
c. Names artistic expressions (e.g. sculpture, painting, working gold, music)
```
```
d. Describes elements of everyday life: food, clothing, entertainment, customs
```
```
2.3. Economic situation
```
```
Names economic activities (e.g. agriculture, animal husbandry, early industries,
trade and in particular the fur trade)
```
```
a.
```

```
b. Names means of transportation (e.g. canoe, boat, cart)
```
```
c. Indicates transportation routes: waterways, forest paths, Chemin du Roy
```
```
2.4. Political situation
```
```
Indicates the means of decision making: made unilaterally by the king of France or
his representative in the colony (the governor)
```
```
a.
```
```
b. Indicates the means of selecting leaders: appointed by the king of France
```
```
Indicates the roles of the leaders in the colony: governor (e.g. army, Native
relations), intendant (e.g. finance, justice)
```
```
c.
```
```
d. Names an institution: the Sovereign Council
```
3. **Assets and limitations of the territory**

```
Indicates assets related to the relief (e.g. the Appalachians were a natural defence
against the English threat; the St. Lawrence Valley facilitated settlement of the
territory)
```
```
a.
```
```
Indicates assets and limitations related to climate (e.g. the temperature and rain in
the summer were good for farming; the temperature and snow in the winter limited
activities and travel)
```
```
b.
```
```
Indicates assets and limitations related to bodies of water (e.g. the rivers and lakes
facilitated access to the territory; the rapids limited travel)
```
```
c.
```
```
Explains why resources were assets (e.g. forests provided oak for shipbuilding; the
presence of iron supplied the Forges du Saint-Maurice)
```
```
d.
```
4. **Influence of people and events on social and territorial organization**

```
a. Names important people: Talon, Frontenac, Msgr. de Laval
```
```
Names groups that played a role (e.g. colonists, filles du Roy, coureurs de bois,
military)
```
```
b.
```
```
Indicates events that marked society: establishment of cottage industries, seigneurial
system, triangular trade
```
```
c.
```
5. **Elements of continuity with the present**

```
Indicates traces left by the society (e.g. land divided into rectangular strips along
waterways)
```
```
a.
```
**E. Canadian society around 1820**^123456

1. **Location of the society in space and time**

```
Locates on maps with different scales the territory occupied by Canadian society: St.
Lawrence and Great Lakes lowlands
```
```
a.
```
```
Situates, on graduated time lines with different scales, events and people related to
the history of the society (e.g. the first governors: Murray, Carleton; the arrival of the
Loyalists; the creation of the House of Assembly; Papineau)
```
```
b.
```
2. **Elements of the society that affect the organization of the territory**

```
2.1. Demographic situation
```
```
Describes the distribution of the population: concentrated along the St. Lawrence
and in the Great Lakes region
```
```
a.
```
```
b. Describes the composition of the population: Native peoples, Canadians, English
```
```
c. Gives the approximate number of inhabitants
```
```
2.2. Cultural situation
```
```
a. Names the main languages spoken: French, English
```
```
b. Names the main religions practised: Native spiritualities, Catholicism, Protestantism
```
```
c. Names artistic expressions (e.g. painting, literature, architecture)
```

```
d. Describes elements of everyday life: food, clothing, entertainment, customs
```
```
2.3. Economic situation
```
```
Names economic activities (e.g. agriculture, animal husbandry, trade and in
particular the lumber trade)
```
```
a.
```
```
b. Names means of transportation: land or sea, depending on the season
```
```
c. Indicates transportation routes (e.g. waterways, roads, canals)
```
```
2.4. Political situation
```
```
Indicates the means of selecting leaders: the king of England appointed the
governor and the people elected representatives
```
```
a.
```
```
Indicates the means of decision making: the representatives passed laws and the
governor approved or vetoed them
```
```
b.
```
```
c. Names an institution: the House of Assembly
```
3. **Assets and limitations of the territory**

```
a. Indicates assets related to the relief (e.g. plains were good for farming)
```
```
Indicates assets and limitations of bodies of water (e.g. rivers made it possible to
transport logs and provided hydraulic energy for the mills; it was necessary to build
canals to cross the rapids)
```
```
b.
```
```
Explains why resources were assets (e.g. the forests satisfied the mother country’s
lumber needs)
```
```
c.
```
4. **Influence of people and events on social and territorial organization**

```
a. Names important people (e.g. Murray, Carleton, Papineau)
```
```
b. Names groups who played a role (e.g. English merchants, Loyalists, Patriots)
```
```
Indicates events that marked the society (e.g. the Conquest, parliamentary
government, Napoleonic Wars, opening of lumber camps)
```
```
c.
```
5. **Elements of continuity with the present**

```
Indicates traces left by the society (e.g. parliamentary government, canals,
townships)
```
```
a.
```
**F. Québec society around 1905**^123456

1. **Location of the society in space and time**

```
Locates on maps with different scales the territory occupied by Québec society:
borders of Québec
```
```
a.
```
```
Situates, on graduated time lines with different scales, events and people related to
the history of the society (e.g. Canadian Confederation, Honoré Mercier)
```
```
b.
```
2. **Elements of the society that affect the organization of the territory**

```
2.1. Demographic situation
```
```
Describes the distribution of the population: along the St. Lawrence and in the
regions
```
```
a.
```
```
Describes the composition of the population: Native peoples, French Canadians,
English Canadians and European immigrants
```
```
b.
```
```
c. Gives the approximate number of inhabitants
```
```
2.2. Cultural situation
```
```
a. Names the main languages spoken: French, English
```
```
b. Names the main religions practised: Native spiritualities, Catholicism, Protestantism
```

```
c. Names artistic expressions (e.g. painting, architecture, music, literature)
```
```
d. Describes elements of everyday life: food, clothing, entertainment, customs
```
```
2.3. Economic situation
```
```
a. Names economic activities: agriculture, animal husbandry, industry, trade
```
```
b. Names means of transportation: land or sea, depending on the season
```
```
c. Indicates transportation routes: waterways, roads, railways, canals
```
```
2.4. Political situation
```
```
a. Indicates the means of selecting leaders: the people elected representatives
```
```
b. Indicates the means of decision making: representatives passed laws
```
```
c. Names an institution: the Legislative Assembly
```
3. **Assets and limitations of the territory**

```
Indicates assets related to the relief (e.g. differences in level made it possible to
build hydroelectric dams)
```
```
a.
```
```
Indicates assets related to bodies of water (e.g. the fast flow of the rivers facilitated
the production of electricity needed to supply the pulp and paper and aluminum
industries)
```
```
b.
```
```
Explains why resources were assets (e.g. the coniferous forests made it possible to
develop a pulp and paper industry)
```
```
c.
```
4. **Influence of people and events on social and territorial organization**

```
Names important people (e.g. John A. MacDonald, Honoré Mercier, Wilfrid Laurier,
Thérèse Casgrain)
```
```
a.
```
```
b. Names groups that played a role (e.g. colonists, suffragettes, unions)
```
```
Indicates events that marked society: Canadian Confederation, industrialization,
urbanization, unionization, electrification, colonization
```
```
c.
```
5. **Elements of continuity with the present**

```
a. Indicates traces left by the society: electrification, unionization
```
**G. Québec society around 1980 123456**

1. **Location of the society in space and time**

```
Locates on maps with different scales the territory occupied by Québec society:
borders of Québec
```
```
a.
```
```
Situates, on graduated time lines with different scales, events and people related to
the history of the society (e.g. election of Jean Lesage, election of Robert Bourassa
(1970), construction of the James Bay hydroelectric power stations, election of René
Lévesque, agricultural zoning, adoption of the Charter of the French Language )
```
```
b.
```
2. **Elements of the society that affect the organization of the territory**

```
2.1. Demographic situation
```
```
Describes the distribution of the population: along the St. Lawrence and in the
regions
```
```
a.
```
```
Describes the composition of the population: Native peoples (Amerindian and Inuit),
people of French and British descent, people of other origins
```
```
b.
```
```
c. Gives the approximate number of inhabitants
```
```
2.2. Cultural situation
```

```
a. Names the main languages spoken: French, English
```
```
b. Names the main religions practised (e.g. Catholicism, Protestantism)
```
```
c. Names artistic expressions (e.g. painting, architecture, music, dance, literature)
```
```
d. Describes elements of everyday life: food, clothing, entertainment, customs
```
```
2.3. Economic situation
```
```
a. Names economic activities: agriculture, animal husbandry, industry, trade
```
```
b. Names means of transportation: land, sea, air
```
```
c. Indicates transportation routes: highways, railways, seaway, airways
```
```
2.4. Political situation
```
```
a. Indicates the means of selecting leaders: the people elected representatives
```
```
b. Indicates the means of decision making: the representatives passed laws
```
```
c. Names a political institution: the National Assembly
```
3. **Assets and limitations of the territory**

```
Indicates assets related to the relief (e.g. the differences in level made it possible to
build hydroelectric power stations in the James Bay region)
```
```
a.
```
```
Indicates assets related to bodies of water (e.g. the fast flow of the rivers facilitated
the production of electricity for local consumption and export)
```
```
b.
```
```
c. Explains why resources were assets (e.g. the forests provided lumber for export)
```
4. **Influence of people and events on social and territorial organization**

```
a. Names important people: Jean Lesage, Robert Bourassa, René Lévesque
```
```
Indicates events that marked society: the Quiet Revolution, construction of
hydroelectric power stations, St. Lawrence Seaway, agricultural zoning
```
```
b.
```
5. **Elements of continuity with the present**

```
Indicates traces left by the society: health insurance, comprehensive secondary
schools, CEGEPs
```
```
a.
```
1. Presenting societies in chronological fashion allows students to acquire knowledge specific to each type of social and
    territorial organization studied. This knowledge is, for the most part, applied in the same cycle. Students will use some of
    this knowledge when studying changes or differences. That is why this document, unlike similar documents in other
    subjects, does not contain indications concerning the reinvestment of knowledge.
2. Locate societies in space and time using the techniques specific to geography and history.


## Geography, History and Citizenship Education

## Know ledge related to change in a society and its territory

In Cycle One, students become familiar with the concept of change. By comparing everyday objects, economic activities,
means of transportation and transportation routes over a period of about a century, they continue the process of
developing their representation of time, which they began in preschool. In Cycles Two and Three, students learn to
interpret change in a society and its territory. They compare a society during two different periods. They acquire
knowledge about the changes that occurred, the role of certain people and groups and the influence of certain events.

```
Legend^1
Student constructs knowledge with teacher guidance.
```
```
Student applies knowledge by the end of the school year.
```
```
Elementary
```
```
Cycle
One
```
```
Cycle
Two
```
```
Cycle
Three
Past and present
First representation of time (Cycle One)
```
```
A.
123456
```
```
a. Names changes in everyday objects (e.g. toys, CDs, iron, computer, bicycle)
```
```
b. Names changes in economic activities (e.g. agriculture, industry)
```
```
Names changes in means of transportation and transportation routes (e.g. car, train,
airplane, roads, highways)
```
```
c.
```
```
B. Iroquoian society between 1500 and 1745^123456
```
```
Indicates changes in the society during this period: occupation of the territory, use of
European products (e.g. axe, pot, gun, alcohol), religion, European diseases
```
```
a.
```
```
Names groups that played a role in the changes (e.g. fishermen, missionaries,
colonists, military, coureurs de bois)
```
```
b.
```
```
C. French and Canadian society in New France between 1645 and 1745 123456
```
```
Indicates changes in the society during this period (e.g. size of the territory, political
organization, distribution and composition of the population, presence of cottage
industries)
```
```
a.
```
```
Names people and groups that played a role in the changes: Jean Talon, Gilles
Hocquart, explorers, filles du Roy
```
```
b.
```
```
Indicates events that marked this period: implementation of the seigneurial system,
explorations, increase in the birth rate, diversification of the economy
```
```
c.
```
```
D. Canadian society between 1745 and 1820^123456
```
```
Indicates changes in the society during this period (e.g. occupation of the territory,
presence of anglophones, lumber trade, canal building, the first newspapers)
```
```
a.
```
```
Names people and groups that played a role in the changes (e.g. Murray, Carleton,
English merchants, Loyalists, Gazette de Québec )
```
```
b.
```
```
Indicates events that marked this period (e.g. the Conquest, the first printing shops,
parliamentary government)
```
```
c.
```
```
E. Canadian society and Québec society between 1820 and 1905^123456
```
```
Indicates changes in the society during this period: occupation of the territory,
industrialization, urbanization, settlement, railway building
```
```
a.
```
```
Names people and groups that played a role in the changes: John A. MacDonald,
Honoré Mercier, unions
```
```
b.
```
```
Indicates events that marked this period: Canadian Confederation, unionization,
immigration, railway building
```
```
c.
```
```
F. Québec society between 1905 and 1980 123456
```
```
Indicates changes in the society during this period (e.g. transportation and
communication networks, hydroelectric power, rural electrification, mandatory school
attendance, free health care)
```
```
a.
```
```
Names people who played a role in the changes (e.g. Maurice Duplessis, Jean
Lesage, Robert Bourassa, René Lévesque, P.‑E. Trudeau)
```
```
b.
```
```
Indicates events that marked this period: the Quiet Revolution, the nationalization of
hydroelectric power, the construction of hydroelectric power stations, charters of
rights and freedoms
```
```
c.
```

1. Presenting societies in chronological fashion allows students to acquire knowledge specific to each type of social and
    territorial organization studied. This knowledge is, for the most part, applied in the same cycle. Students will use some of
    this knowledge when studying changes or differences. That is why this document, unlike similar documents in other
    subjects, does not contain indications concerning the reinvestment of knowledge.


## Geography, History and Citizenship Education

## Know ledge related to the diversity of societies and their territories

In Cycle One, students become familiar with the concept of diversity. By comparing everyday objects, economic activities
and means of transportation, they continue the process of developing their representation of space, which they began in
preschool. In Cycles Two and Three, students become more open to the diversity of societies and their territories. They
compare two societies during the same period. They acquire knowledge about their differences in terms of the
characteristics of the territory, language, religion and type of government.

```
Legend^1
Student constructs knowledge with teacher guidance.
```
```
Student applies knowledge by the end of the school year.
```
```
Elementary
```
```
Cycle
One
```
```
Cycle
Two
```
```
Cycle
Three
Here and elsewhere
First representation of space (Cycle One)
```
```
A.
123456
```
1. Indicates differences between his/her environment and an unfamiliar environment:

```
a. everyday objects (e.g. toys, furniture, clothing)
```
```
means of transportation and transportation routes (e.g. car, train, airplane, roads,
highways)
```
```
b.
```
```
c. economic activity (e.g. development and processing of resources, services)
```
```
d. characteristics of the territory (e.g. relief, climate, bodies of water, resources)
```
```
B. Iroquoian society and Algonquian society around 1500^123456
```
1. Indicates differences between Iroquoian society and Algonquian society around 1500:

```
a. way of life (sedentary; nomadic)
```
```
b. economic activities (agriculture; lack of agriculture)
```
```
c. political structure (matriarchal; patriarchal)
```
```
d. dwellings (villages of longhouses; wigwams)
```
```
C. Iroquoian society and Inca society around 1500^123456
```
1. Indicates differences between Iroquoian society and Inca society around 1500:

```
means of selecting chiefs and their power (chosen by women elders, limited powers;
hereditary, full powers)
```
```
a.
```
```
b. social structure (community; hierarchy)
```
```
c. dwellings (villages of longhouses; towns)
```
```
characteristics of the territory occupied (e.g. relief, climate, bodies of water,
resources)
```
```
d.
```
```
Canadian society in New France and societies in the Thirteen Colonies
around 1745
```
```
D.
123456
```
1. Indicates differences between Canadian society in New France and societies in the Thirteen Colonies around 1745:

```
a. number of inhabitants
```
```
b. type of government (no House of Assembly; House of Assembly)
```
```
c. language (French; English)
```
```
d. religion (Catholicism; Protestantism)
```
```
e. economic activities (fur trade; diversified economy)
```

```
f. military force (e.g. soldiers, ships, armaments)
```
```
characteristics of the territory occupied (e.g. relief, climate, bodies of water,
resources)
```
```
g.
```
```
E. Québec society and Canadian society in the Prairies around 1905 123456
```
1. Indicates differences between Québec society and Canadian society in the Prairies around 1905:

```
a. composition of the population (francophones; anglophones, European immigrants)
```
```
b. economic activities (e.g. industry, agriculture, animal husbandry)
```
```
c. main languages (French; English)
```
```
d. main religions (Catholicism; Protestantism)
```
```
characteristics of the territory occupied (e.g. relief, climate, bodies of water,
resources)
```
```
e.
```
```
F. Canadian society in the Prairies and on the West Coast around 1905 123456
```
1. Indicates differences between Canadian society in the Prairies and on the West Coast around 1905:

```
composition of the population (francophones; anglophones, European and Asian
immigrants)
```
```
a.
```
```
b. economic activities (agriculture, animal husbandry; mining, forestry)
```
```
c. main religions (Catholicism; Protestantism)
```
```
characteristics of the territory occupied (e.g. relief, climate, bodies of water,
resources)
```
```
d.
```
```
Québec society and an undemocratic society around 1980 (at the teacher’s
discretion)
```
```
G.
123456
```
1. Indicates differences between Québec society and an undemocratic society around 1980:

```
a. composition of the population
```
```
b. main languages
```
```
c. method of making political decisions (democratic; authoritarian)
```
```
d. right to vote (yes; no)
```
```
e. charter of rights and freedoms (yes; no)
```
```
H. Micmac society and Inuit society around 1980 123456
```
1. Indicates differences between Micmac society and Inuit society around 1980:

```
a. distribution of the population (Gaspésie, Maritimes; Nunavik)
```
```
b. economic activities (e.g. fishing, hunting, tourism, crafts)
```
```
c. languages (Micmac; Inuktitut)
```
```
characteristics of the territory occupied (e.g. relief, climate, bodies of water,
resources)
```
```
d.
```
1. Presenting societies in chronological fashion allows students to acquire knowledge specific to each type of social and
    territorial organization studied. This knowledge is, for the most part, applied in the same cycle. Students will use some of
    this knowledge when studying changes or differences. That is why this document, unlike similar documents in other
    subjects, does not contain indications concerning the reinvestment of knowledge.


## Geography, History and Citizenship Education

## Researching and w orking w ith information in geography and history

Teachers show students how to research and work with information in geography and history. Starting in Cycle One, they
model certain elements of the process. In Cycles Two and Three, they introduce students to all of the steps in the process.

```
Legend
Student constructs knowledge with teacher guidance.
```
```
Student applies knowledge by the end of the school year.
```
```
Elementary
```
```
Cycle
One
```
```
Cycle
Two
```
```
Cycle
Three
```
```
123456
```
1. **Learns about a problem**

```
a. Defines the problem
```
```
b. Draws on previous learning
```
```
c. Considers research strategies that will lead to a solution
```
2. **Asks questions**

```
a. Spontaneously frames questions
```
```
b. Organizes question in categories
```
```
c. Selects useful questions
```
3. **Plans research**

```
a. Makes a research plan
```
```
b. Locates sources of information
```
```
c. Chooses or creates data-gathering tools
```
4. **Gathers and processes information**

```
a. Collects data
```
```
b. Sorts data into categories
```
```
c. Distinguishes between facts and opinions
```
```
d. Criticizes data
```
```
e. Distinguishes between relevant and irrelevant documents
```
```
f. Compares data
```
5. **Organizes information**

```
a. Chooses a way to communicate information
```
```
b. Makes a plan
```
```
c. Identifies the essential elements of information
```
```
d. Arranges data in tables, lists, diagrams or text
```
```
e. Uses supporting documents
```
```
f. Indicates sources
```

6. **Communicates the results of research**

```
a. Chooses appropriate language
```
```
b. Uses various media
```
```
c. Presents a production
```

## Geography, History and Citizenship Education

## Techniques specific to geography and history

Teachers introduce students to techniques specific to geography and history. They propose learning and evaluation
situations that enable students to use maps, atlases and time lines. Using a globe and a wall map helps students locate
things in space. Using images, tables and diagrams (histograms, double-entry tables, bar or circle graphs, climate charts)
enables them to find information about the societies and territories studied. The documents used should be varied and
adapted to the students’ abilities.

```
Legend
Student constructs knowledge with teacher guidance.
```
```
Student applies knowledge by the end of the school year.
```
```
Elementary
```
```
Cycle
One
```
```
Cycle
Two
```
```
Cycle
Three
```
```
123456
```
1. **Interprets simple maps**

```
a. Reads the title
```
```
b. Decodes the legend
```
```
c. Reads the scale
```
```
d. Uses the points of the compass
```
```
e. Uses spatial reference points
```
2. **Constructs a time line**

```
a. Selects information
```
```
b. Calculates the amount of time to be represented
```
```
c. Determines what unit of measure to use
```
```
d. Draws an axis
```
```
e. Divides the time line into segments depending on the scale chosen
```
```
f. Indicates information
```
```
g. Indicates a title
```
3. **Interprets a time line**

```
a. Reads the title
```
```
b. Decodes the chronological scale
```
```
c. Uses chronological reference points
```
```
d. Finds information: places, actors, circumstances
```
```
Interprets illustrated documents
(e.g. illustrations, posters, murals, paintings)
```
### 4.

```
a. Determines the nature of the document
```
```
b. Locates the source and date
```
```
c. Reads the title
```
```
d. Determines the main subject
```

```
e. Determines places, actors, circumstances
```
5. **Interprets tables and diagrams**

```
a. Reads the title
```
```
b. Decodes the legend
```
```
c. Locates the scale
```
```
d. Identifies the nature of the information
```
```
e. Finds data
```
6. **Constructing tables and diagrams**

```
a. Selects information
```
```
b. Indicates and naming each entry
```
```
c. Establishes the scale
```
```
d. Indicates the legend
```
```
e. Indicates the data
```
```
f. Indicates a title
```

