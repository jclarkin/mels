# Progression of Learning

[MELS Source](http://www.education.gouv.qc.ca/fileadmin/site_web/documents/education/jeunes/pfeq/PDA_PFEQ_art-dramatique-primaire_2009_EN.pdf)

# Drama

## August 24, 2009


## Table of Contents

### Introduction 3

### Know ledge 4

### Applications of Know ledge 7

### Competency 1 – To invent short scenes 7

### Competency 2 – To interpret short scenes 11

### Competency 3 – To appreciate dramatic works, personal productions and those of

### classmates 15


## Drama

## Introduction

In order to invent or interpret short scenes and to appreciate dramatic works, students must acquire a certain amount of
knowledge related to the language of drama, performance techniques, styles of theatre and elements of drama. Presented
schematically in the program as essential knowledges, this learning is addressed here in order to facilitate teachers’
planning. It is presented in four tables. The first table covers knowledge that students should have acquired by the end of
each cycle. The other three tables illustrate, by means of observable actions, how this knowledge is mobilized in the
exercise of each of the three competencies developed in the program. Related to the key features of the competencies, the
action verbs used in each statement show the progression of learning from one cycle to the next. Teachers will be better
equipped to ensure students’ competency development if they include in their planning simple or complex tasks aimed at
the acquisition and application of different items of knowledge in a given context.

Since competency development and acquisition of the knowledge underlying the competency are closely related, the
particulars contained in this document should enable teachers to help students acquire the tools they need to develop
each of the program’s competencies and to discover their artistic sensitivity and their creative potential.

Throughout elementary school, students in the Drama program become familiar with the creative process by using various
elements of knowledge to invent their own short scenes. They also use this knowledge to interpret the meaning of a story,
which adds to their cultural experience. Lastly, they learn to express themselves using the appropriate subject-specific
vocabulary and acquire the skills they need to exercise critical judgment when appreciating dramatic works, personal
productions and those of classmates.

The elementary-level Arts Education programs were designed to ensure the progression of learning in each subject area

from the first to the sixth years. However, since continuity is required only for one of the two arts subjects,^1 the second
subject may not be offered continuously throughout elementary school. In such a case, it is important to provide students
with as complete an arts education as possible, taking their abilities into account. For example, if the drama course is
offered in one cycle only, teachers should make an effort to help students acquire not only the knowledge associated with
that cycle, but any other knowledge deemed essential. This knowledge appears as statements in bold type.

```
In short, by progressively acquiring the knowledge outlined in this document, students will develop the competencies
presented in the Drama program. The tables will allow teachers to provide students with the conditions necessary for
competency development at the elementary level.
```
1. The _Basic school regulation for preschool, elementary and secondary education_ stipulates that two of the four arts
    subjects (Drama, Visual Arts, Dance and Music) are compulsory at the elementary level. According to these obligations,
    one of the two subjects taught in Cycles Two and Three must be the same as one of those taught in Cycle One.


## Drama

## Knowledge

In their planning, teachers should include a variety of simple tasks (diverse exploration and experimentation activities) to
help students acquire and apply the knowledge in this table.

```
Student constructs knowledge with teacher guidance.
```
```
Student applies knowledge by the end of the school year.
```
```
Student reinvests knowledge.
```
```
The statements in bold type indicate knowledge that should be given priority if the program is not taught in all
cycles.
```
```
Elementary
```
```
Cycle
One
```
```
Cycle
Two
```
```
Cycle
Three
```
#### A. Language of drama 123456

1. **Expression using the body**

```
Names two expressions using the body related to characters: attitude and
gestures
```
```
a.
```
```
Names four expressions using the body related to characters and dramatic action:
attitude, gestures, mimicry and movement
```
```
b.
```
```
Names the expressions using the body related to characters and dramatic action:
attitude, gestures, mimicry, movement and rhythm
```
```
c.
```
2. **Expression using the voice**

```
2.1. Vocal sounds
```
```
a. Names some noises and sounds related to actions
```
```
b. Names some noises and sounds related to actions or emotions
```
```
2.2. Speech
```
```
a. Names the following expression using the voice: intensity
```
```
b. Names the following expressions using the voice: intensity and duration
```
```
Names the following expressions using the voice: intensity, duration, pitch and
timbre
```
```
c.
```
```
2.3. Vocal ensemble
```
```
a. Identifies some characteristics of a chorus
```
#### B. Performance techniques 123456

1. **Performance conditions**

```
Names the following performance conditions: attention, memorization and direction
of gaze
```
```
a.
```
```
Names the following performance conditions: listening, memorization and
direction of gaze
```
```
b.
```
```
Names the following performance conditions: concentration, memorization and
direction of gaze
```
```
c.
```
2. **Vocal techniques**

```
Names one of the following vocal techniques: posture, sound production, breathing
or speed of delivery
```
```
a.
```
```
Names some of the following vocal techniques: breathing, posture, sound
production, projection of sound, pronunciation and speed of delivery
```
```
b.
```
```
Distinguishes among the following vocal techniques: breathing, posture, sound
production, projection of sound, pronunciation, speed of delivery, rhythm and
intonation
```
```
c.
```

3. **Body techniques**

```
a. Names one of the following body techniques: relaxation, flexibility or levels
```
```
Names some of the following body techniques: relaxation, flexibility, rhythms, levels
and energy
```
```
b.
```
```
Distinguishes among the following body techniques: relaxation, flexibility, rhythms,
levels and energy
```
```
c.
```
```
d. Identifies new body techniques such as exaggeration or balance/imbalance
```
4. **Expressive elements**

```
a. Names an expressive element such as the nature of a character
```
```
Names some expressive elements such as the nature of a character and the
characteristics of a story
```
```
b.
```
#### C. Styles of theatre 123456

1. **Marionettes**

```
a. Names the basic positions of concealed or full-view manipulation
```
```
Names the following means of characterizing marionettes: breathing, direction of
gaze, gait and voice
```
```
b.
```
2. **Clown performance**

```
Names the following comic effects: physical actions, word play, repetition and
exaggeration
```
```
a.
```
```
Lists elements of clown comedy such as imitation, reversal of situation, parody of
circus games and problem solving
```
```
b.
```
3. **Shadow theatre**

```
Names elements of shadow theatre technique such as distance, body position,
shadow and special effects
```
```
a.
```
4. **Performance in masks**

```
Names elements of mask technique such as direction of gaze, origin and quality of
gesture, rhythm and position in space
```
```
a.
```
#### D. Elements of drama 123456

1. **Space**

```
a. Names types of simple blocking in a performance space
```
```
b. Names types of memorized blocking journey, using place markers
```
```
c. Distinguishes playing area from the performance area
```
```
d. Identifies the levels in a performance area
```
2. **Object**

```
a. Names the following functions of an object: imaginary and utilitarian
```
```
b. Distinguishes between the functions of an object: imaginary and utilitarian
```
3. **Set design**^1^
4. **Costume**

```
a. Names the function of a costume element
```
```
b. Names the function of some costume elements
```
```
c. Describes the function of a costume
```

5. **Sound environment**

```
a. Identifies different vocal effects related to characters
```
```
b. Identifies different sound effects related to stories
```
6. **Lighting**

```
a. Identifies the position of a character in the light
```
```
b. Recognizes the intensity and colour of light
```
```
c. Names simple lighting effects
```
#### E. Structures 123456

1. **Story development**

```
Names a characteristic of continuous story development such as the beginning and
ending of the story
```
```
a.
```
```
Names characteristics of continuous story development such as
development, plot twists and ending
```
```
b.
```
```
c. Names the following discontinuous story development: tableaux
```
2. **Types of discourse**^1
3. **Improvisation**^1
4. **Writing**^1

#### F. Drama appreciation repertoire 123456

1. **Types of excerpts**^1
1. Since these elements are evident in action, they are included in the _Applications of Knowledge_ section_._


## Drama

## Applications of Knowledge

### Competency 1 – To invent short scenes

```
Student constructs knowledge with teacher guidance.
```
```
Student applies knowledge by the end of the school year.
```
```
Student reinvests knowledge.
```
```
The statements in bold type indicate knowledge that should be given priority if the program is not taught in all
cycles.
```
```
Elementary
```
```
Cycle
One
```
```
Cycle
Two
```
```
Cycle
Three
```
#### A. To use personal ideas inspired by the stimulus for creation 123456

1. **Structure**

```
1.1. Story development
```
```
a. Creates a story with continuous development, with a beginning and an ending
```
```
Creates a story with continuous development, with a plot twist and an
ending
```
```
b.
```
```
Creates a story with continuous development, with a plot twist and an
ending, and with discontinuous development, using tableaux
```
```
c.
```
```
1.2. Type of discourse
```
```
a. Drafts a dialogue
```
```
b. Drafts a narration
```
```
1.3. Writing (individual or group)
```
```
a. Writes a basic storyline
```
#### To use elements of the language of drama, performance techniques, styles

#### of theatre and elements of drama

#### B.

##### 123456

1. **Language of drama**

```
1.1. Expression using the body
```
```
a. Finds an attitude and a gesture related to the character
```
```
b. Finds attitudes, gestures, mimicry and movements related to the character
```
```
Chooses gestures, attitudes, mimicry, movements and rhythm related to the
character
```
```
c.
```
```
1.2. Expression using the voice
```
```
1.2.1 Vocal sounds
```
```
a. Uses in his/her creation a noise and a sound related to the dramatic action
```
```
Uses in his/her creation noises and sounds related to the dramatic action and the
character’s emotions
```
```
b.
```
```
Combines noises and sounds related to the dramatic action and the character’s
emotions
```
```
c.
```
```
1.2.2 Speech
```

```
a. Finds a vocal intensity related to the nature of the character
```
```
b. Finds a vocal intensity related to the emotions and nature of the character
```
```
Combines expressions using the voice related to the emotions and nature of the
character, such as intensity, pitch and timbre
```
```
c.
```
```
d. Finds some chorus effects for his/her creation
```
2. **Performance techniques**

```
2.1. Vocal techniques
```
```
Uses one of the following vocal techniques: breathing, posture, sound production or
speed of delivery
```
```
a.
```
```
Uses some of the following vocal techniques: breathing, posture, sound production,
projection of sound, pronunciation and speed of delivery
```
```
b.
```
```
Combines some vocal techniques such as breathing, posture, sound production,
projection of sound, pronunciation, speed of delivery, rhythm and intonation
```
```
c.
```
```
2.2. Body techniques
```
```
a. Uses one of the following body techniques: relaxation, flexibility or levels
```
```
Uses some of the following body techniques: relaxation, flexibility, levels, rhythms
and energy
```
```
b.
```
```
Incorporates one of the following body techniques into the composition of his/her
character: relaxation, flexibility, levels, rhythms or energy
```
```
c.
```
```
d. Uses the following body techniques: exaggeration, balance, imbalance
```
```
2.3. Expressive elements
```
```
a. Finds a trait for his/her character
```
```
b. Finds a few traits for his/her character
```
```
c. Chooses the traits for his/her character
```
3. **Styles of theatre**

```
3.1. Marionettes
```
```
a. Uses basic positions for concealed or full-view manipulation
```
```
b. Characterizes the marionette using breathing, direction of gaze, gait and voice
```
```
3.2. Clown performance
```
```
Uses the following comic effects: physical actions, word play, repetition and
exaggeration
```
```
a.
```
```
Uses the following elements of clown comedy: imitation, reversal of situation, parody
of circus games and problem solving
```
```
b.
```
```
3.3. Shadow theatre
```
```
Uses the following technical elements: distance, body position, shadow and special
effects
```
```
a.
```
```
3.4. Performance in masks
```
```
a. Directs his/her gaze (nose)
```
```
Finds the emotion to be expressed, the quality of the gesture, rhythm and position in
space
```
```
b.
```
4. **Elements of drama**


```
4.1. Space
```
```
a. Chooses simple blocking in a performance space
```
```
b. Memorizes blocking in a performance space
```
```
c. Shifts from a limited playing area to a performance area
```
```
d. Uses levels in the performance space
```
```
4.2. Object
```
```
a. Finds an imaginary or utilitarian function for the object
```
```
b. Uses the object according to its imaginary or utilitarian function
```
```
4.3. Set design
```
```
a. Arranges the playing area (space)
```
```
b. Arranges the playing area (objects and space)
```
```
c. Transforms the playing area (during performance)
```
```
4.4. Costume
```
```
a. Chooses a costume element based on the character in the story
```
```
b. Chooses some costume elements based on the character in the story
```
```
c. Chooses the costume elements based on the character in the story
```
```
4.5. Sound environment
```
```
a. Finds different vocal effects related to the character
```
```
b. Finds different sound effects related to the story
```
```
4.6. Lighting
```
```
a. Stands in the light based on his/her performance choices
```
```
b. Chooses the intensity and colour of the light based on the story
```
```
c. Looks for simple lighting effects related to the story
```
#### C. To organize the elements he/she has chosen 123456

```
Tries out sequences of elements of dramatic language , performance techniques,
styles of theatre and elements of drama
```
```
a.
```
```
b. Puts the elements of the story in order
```
```
c. Organizes the content of his/her improvisations
```
```
d. Organizes the content of his/her improvisations based on his/her creative intention
```
```
Improvises spontaneously and with preparation: sounds, gestures and
words
```
```
e.
```
#### D. To finalize a production 123456

```
a. Makes adjustments to his/her short scene
```

```
b. Adjusts his/her production based on the initial stimulus for creation
```
```
Enhances certain aspects of his/her production using elements of dramatic
language, performance techniques, styles of theatre or elements of drama
```
```
c.
```
1. **Performance techniques**

```
1.1. Performance conditions
```
```
a. Pays attention in his/her prepared or spontaneous improvisations
```
```
b. Focuses his/her listening in prepared or spontaneous improvisations
```
```
c. Concentrates in his/her prepared or spontaneous improvisations
```
```
d. Directs his/her gaze in his/her prepared or spontaneous improvisations
```
```
1.2. Rules that apply to group performance
```
```
Responds to a performance direction in his/her prepared or spontaneous
improvisations
```
```
a.
```
```
Responds to performance directions in his/her prepared or spontaneous
improvisations
```
```
b.
```
#### E. To share his/her creative experience^123456

```
Shares significant aspects related to the use of elements of dramatic language,
performance techniques, styles of theatre or elements of drama
```
```
a.
```
```
Describes important aspects related to the use of elements of dramatic
language, performance techniques, styles of theatre or elements of drama
```
```
b.
```
```
c. Uses subject-specific vocabulary
```

## Drama

## Applications of Knowledge

### Competency 2 – To interpret short scenes

```
Student constructs knowledge with teacher guidance.
```
```
Student applies knowledge by the end of the school year.
```
```
Student reinvests knowledge.
```
```
The statements in bold type indicate knowledge that should be given priority if the program is not taught in all
cycles.
```
```
Elementary
```
```
Cycle
One
```
```
Cycle
Two
```
```
Cycle
Three
```
#### A. To become familiar with the dramatic content of the scene 123456

1. **Structure**

```
1.1. Story development
```
```
a. Locates the beginning and ending of the story
```
```
b. Locates the development, the plot twists and the resolution of the story
```
```
c. Locates the tableaux in the story
```
```
Locates in the scene an element of dramatic language to interpret the character in
the story
```
```
d.
```
```
Locates in the scene some elements of dramatic language to interpret the
character in the story
```
```
e.
```
```
Locates in the scene the elements of dramatic language to interpret the character in
the story
```
```
f.
```
```
g. Makes a few attempts at performing
```
```
h. Translates the content of the scene into dramatic action
```
```
1.2. Type of discourse
```
```
a. Locates the dialogue and narration in the story
```
#### To apply elements of the language of drama, performance techniques,

#### styles of theatre and elements of drama

#### B.

##### 123456

1. **Language of drama**

```
1.1. Expression using the body
```
```
a. Uses an attitude and a gesture related to the character in the story
```
```
Uses some different attitudes, gestures, mimicry and movements related to the
character in the story
```
```
b.
```
```
Chooses some different attitudes, gestures, mimicry, movements and rhythms
related to the character in the story
```
```
c.
```
```
1.2. Expression using the voice
```
```
1.2.1 Vocal sounds
```
```
a. Uses a noise and a sound related to the dramatic actions
```
```
Uses some noises and sounds related to the dramatic actions and the character’s
emotions
```
```
b.
```
```
Integrates some noises and sounds related to the dramatic actions and the
character’s emotions
```
```
c.
```

```
1.2.2 Speech
```
```
Locates the vocal intensity that corresponds to the nature of the character in
the story
```
```
a.
```
```
Uses the vocal intensity that corresponds to the character’s emotion and nature in
the story
```
```
b.
```
```
Combines means that correspond to the character’s emotion and nature in the story
such as intensity, pitch and timbre
```
```
c.
```
2. **Performance techniques**

```
2.1. Performance conditions
```
```
a. Memorizes a few lines and movements in the performance space
```
```
b. Memorizes a short script and movements in the performance space
```
```
c. Memorizes a script excerpt and his/her movements in the performance space
```
```
2.2. Vocal techniques
```
```
Uses a vocal technique related to the character in the story (breathing, posture,
production of sound or speed of delivery)
```
```
a.
```
```
Uses some vocal techniques related to the character in the story (breathing, posture,
sound production, projection of sound, pronunciation and speed of delivery)
```
```
b.
```
```
Combines some vocal techniques related to the character in the story (breathing,
posture, sound production, projection of sound, pronunciation, speed of delivery,
rhythm and intonation)
```
```
c.
```
```
2.3. Body techniques
```
```
a. Uses one of the following body techniques: relaxation, flexibility or levels
```
```
Uses some of the following body techniques: relaxation, flexibility, levels, rhythm and
energy
```
```
b.
```
```
Integrates one of the following body techniques: relaxation, flexibility, levels, rhythm
or energy
```
```
c.
```
```
d. Uses exaggeration, balance or imbalance
```
3. **Styles of theatre**

```
3.1. Marionettes
```
```
a. Uses the basic positions for concealed or full-view manipulation
```
```
b. Characterizes the marionette using breathing, direction of gaze, gait and voice
```
```
3.2. Clown performance
```
```
Uses the following comic effects: physical actions, word play, repetition and
exaggeration
```
```
a.
```
```
Uses the following elements of clown comedy: imitation, reversal of situation, parody
of circus games and problem solving
```
```
b.
```
```
3.3. Shadow theatre
```
```
Uses the following technical elements: distance, body position, shadow and special
effects
```
```
a.
```
```
3.4. Performance in masks
```
```
a. Directs his/her gaze (nose)
```
```
Locates the emotion to be expressed, the quality of the gesture, rhythm and position
in space
```
```
b.
```

4. **Elements of drama**

```
4.1. Space
```
```
a. Does simple blocking related to the content of the story
```
```
b. Memorizes blocking related to the content of the story
```
```
c. Shifts from a limited playing area to a performance area
```
```
d. Uses levels in the performance space
```
```
4.2. Object
```
```
a. Uses the object based on the content of the story
```
```
4.3. Set design
```
```
a. Respects the arrangement of the playing area (space)
```
```
b. Respects the arrangement of the playing area (objects and space)
```
```
c. Transforms the playing area (during performance)
```
```
4.4. Costume
```
```
a. Chooses a costume element related to the character in the story
```
```
b. Chooses some costume elements related to the character in the story
```
```
c. Chooses the costume related to the character in the story
```
#### C. To bring out the expressive elements of the scene^123456

1. **Performance techniques**

```
1.1. Expressive elements
```
```
a. Respects the nature of the character in the story
```
```
b. Respects the characteristics of the story
```
2. **Elements of drama**

```
2.1. Sound environment
```
```
a. Produces vocal effects related to the character
```
```
b. Produces sound effects related to the story
```
```
2.2. Lighting
```
```
a. Stands in the light according to the established performance directions
```
```
b. Uses an intensity and colour that respect the story directions
```
```
c. Uses some simple lighting effects to support the dramatic action
```
#### D. To apply the rules for group performance^123456

1. **Performance techniques**


```
1.1. Performance conditions
```
```
a. Pays attention during the performance
```
```
b. Focuses his/her listening during the performance
```
```
c. Sustains his/her concentration during the performance
```
```
1.2. Rules that apply to group performance
```
```
a. Respects the established performance directions
```
```
b. Respects some characteristics of the chorus established in the performance
```
#### E. To share his/her interpretation experience 123456

```
Shares significant aspects related to the use of elements of dramatic language,
performance techniques, styles of theatre or elements of drama
```
```
a.
```
```
Describes important aspects related to the use of elements of dramatic
language, performance techniques, styles of theatre or elements of drama
```
```
b.
```
```
c. Uses subject-specific vocabulary
```

## Drama

## Applications of Knowledge

### Competency 3 – To appreciate dramatic works, personal productions and those of classmates

```
Student constructs knowledge with teacher guidance.
```
```
Student applies knowledge by the end of the school year.
```
```
Student reinvests knowledge.
```
```
The statements in bold type indicate knowledge that should be given priority if the program is not taught in all
cycles.
```
```
Elementary
```
```
Cycle
One
```
```
Cycle
Two
```
```
Cycle
Three
```
#### To examine an excerpt from a dramatic work or a dramatic production for

#### elements of content

#### A.

##### 123456

```
a. Observes some subject-specific elements in student productions
```
```
Observes some subject-specific elements in excerpts of works past and present,
from here and elsewhere
```
```
b.
```
1. **Language of drama**

```
1.1. Expression using the body
```
```
Observes some of the following expressions using the body: attitude and
gestures related to the characters
```
```
a.
```
```
Observes some of the following expressions using the body: attitude, gestures,
mimicry and movement related to the characters or the dramatic action
```
```
b.
```
```
Observes some of the following expressions using the body: gestures, attitude,
mimicry, movement and rhythm related to the characters or the dramatic action
```
```
c.
```
```
1.2. Expression using the voice
```
```
1.2.1 Vocal sounds
```
```
Locates some of the following expressions using the voice in dramatic action: noises
and sounds
```
```
a.
```
```
Locates some of the following expressions using the voice in dramatic action or
related to the character’s emotions: noises and sounds
```
```
b.
```
```
Verifies the pertinence of the noises and sounds used in dramatic action or related
to the character’s emotions
```
```
c.
```
```
1.2.2 Speech
```
```
a. Observes the intensity of the voice related to the nature of the character
```
```
b. Observes the intensity of the voice related to the character’s nature and emotions
```
```
Verifies the pertinence of the choice of intensity, timbre and pitch related to the
character’s nature and emotions
```
```
c.
```
```
d. Observes the choral effects integrated into the work or production
```
2. **Performance techniques**

```
2.1. Performance conditions
```
```
Observes one of the following performance conditions: attention, memorization or
direction of gaze
```
```
a.
```
```
Observes some of the following performance conditions: listening,
memorization and direction of gaze
```
```
b.
```
```
Observes some of the following performance conditions: concentration,
memorization and direction of gaze
```
```
c.
```

```
2.2. Rules that apply to group performance
```
```
a. Locates a performance direction used
```
```
b. Locates some of the performance directions used
```
```
c. Verifies the accuracy of the performance directions used
```
```
2.3. Vocal techniques
```
```
Observes one of the following vocal techniques: posture, sound production, speed
of delivery
```
```
a.
```
```
Observes some of the following vocal techniques: breathing, posture, sound
production, projection of sound, pronunciation, speed of delivery
```
```
b.
```
```
Verifies the pertinence of the vocal techniques used: breathing, posture, sound
production, projection of sound, pronunciation, rhythm, intonation, speed of delivery
```
```
c.
```
```
2.4. Body techniques
```
```
a. Observes one of the following body techniques: relaxation, flexibility, levels
```
```
Observes some of the following body techniques: relaxation, flexibility, levels,
rhythms, energy
```
```
b.
```
```
Verifies the pertinence of the body techniques used: relaxation, flexibility, levels,
rhythms, energy
```
```
c.
```
```
d. Observes exaggeration, balance, imbalance
```
```
2.5. Expressive elements
```
```
a. Observes an expressive element used to show the nature of the character
```
```
b. Observes some expressive elements used to show the nature of the character
```
```
c. Observes the expressive elements used to show the nature of the character
```
3. **Styles of theatre**

```
3.1. Marionnettes
```
```
a. Observes the basic positions of concealed or full-view manipulation
```
```
Observes means used to characterize marionettes such as breathing, direction of
gaze, gait and actions
```
```
b.
```
```
3.2. Clown performance
```
```
Observes some of the following comic effects: physical actions, word play, repetition
and exaggeration
```
```
a.
```
```
Observes the following elements of clown comedy: imitation, reversal of situation,
parody of circus games and problem solving
```
```
b.
```
```
3.3. Shadow theatre
```
```
Observes the following technical elements: distance, body position, shadow and
special effects
```
```
a.
```
```
3.4. Performance in masks
```
```
Observes the direction of gaze (nose), the origin and quality of gesture, rhythm and
position in space
```
```
a.
```
4. **Elements of drama**

```
4.1. Space
```

```
a. Observes the simple blocking used
```
```
b. Verifies the accuracy of the memorized blocking
```
```
c. Observes the levels used
```
```
4.2. Object
```
```
a. Observes the imaginary or utilitarian function of the object in the story
```
```
4.3. Set design
```
```
a. Observes the arrangement of the playing area (space)
```
```
b. Observes the arrangement of the playing area (objects and space)
```
```
c. Observes the transformation of the playing area (during performance)
```
```
4.4. Costume
```
```
a. Observes the choice of a costume element
```
```
b. Observes the choice of some costume elements
```
```
c. Observes the choice of costume
```
```
4.5. Sound environment
```
```
a. Observes different vocal effects related to the character
```
```
b. Observes different sound effects related to the story
```
```
4.6. Lighting
```
```
a. Observes the position of the character in the light
```
```
b. Observes the intensity and colour of the light
```
```
c. Observes simple lighting effects
```
5. **Structures**

```
5.1. Story development
```
```
a. Summarizes the story (beginning and ending)
```
```
b. Summarizes the story development, plot twists and ending
```
```
c. Identifies the tableaux
```
#### B. To examine an excerpt from a dramatic work for sociocultural references 123456

```
a. Observes an element representing a sociocultural aspect
```
```
b. Locates elements representing a sociocultural aspect
```
#### C. To make connections between what he/she has felt and examined^123456

```
a. Names an element observed in the short scene that elicited an emotion
```
```
b. Explains why an element in particular elicited an emotion
```

```
Gives an example of the connections made between what he/she felt and the
element observed
```
```
c.
```
```
d. Uses subject-specific vocabulary
```
#### D. To make a critical or aesthetic judgment 123456

```
a. Shares a preference based on an observation
```
```
b. Explains why some elements caught his/her attention
```
```
c. Justifies his/her point of view after verification
```
```
d. Formulates a comment based on his/her observations
```
```
e. Formulates comments using examples from his/her observations
```
```
Compares the short scenes based on his/her observations and the proposed
appreciation criteria
```
```
f.
```
```
g. Uses subject-specific vocabulary
```
#### E. To share his/her appreciation experience^123456

```
Shares significant aspects related to the appreciation of elements of dramatic
language, styles of theatre or elements of drama
```
```
a.
```
```
Describes important aspects related to the appreciation of elements of
dramatic language, performance techniques, styles of theatre or elements of
drama
```
```
b.
```
```
c. Uses subject-specific vocabulary
```

