# Progression of Learning

# English Language Arts

## August 24, 2009


## Table of Contents

- Introduction
- Conventions of Written and Media Language
- Language-Learning Processes
- Text Types, Structures and Features
   - Self-Expressive Text Types
   - Narrative and Literacy Text Types
   - Information-Based Text Types


## English Language Arts

## Introduction

This document is a complementary to the English Language Arts (ELA) program. It neither replaces nor rewrites the current elementary ELA program of study. The focus of the document is on providing more information to teachers about some of the requirements found in the content of the ELA program and their connection to the progressive development of literacy from the beginning to the end of elementary school. Teachers are encouraged to include this document in their planning for teaching. As is the case with the ELA program, this program supplement also plays an important role in determining short- and long-term pedagogical strategies and goals.

The ELA program is first and foremost a literacy program in which speaking, listening, viewing, writing and production of media texts are learned in an integrated fashion. This integration lies at the core of the development of critical literacy. Similarly, the three sections that follow assume a connection between the development of essential knowledge about language and texts, and the language-learning processes that mobilize this knowledge, giving it context, purpose and function.

The first section of the document describes the conventions of written and media language that represent the building blocks on which language as a system is constructed. Students learn how these building blocks work through active engagement with language in situations where they have the opportunity to both enjoy spoken, written and media texts, and create them.

The students’ knowledge about how language conventions influence the ways in which we communicate is learned in action, through the language-learning processes that comprise the second section of the document. These processes are vital insofar as they provide students with essential knowledge that will enable lifelong literacy and learning. Knowledge about the context in which a text is written or produced, the meaning(s)/message(s) it conveys and the audience to whom it is directed provide the foundation for the growth of critical and fluent speakers, listeners, readers, writers and producers. This knowledge is central to the response, writing and production processes described here.

We communicate with one another through the creation of different text types, all of which have explicit and important purposes in shaping our life in society. The third section of the document looks at a number of essential social functions that texts play in the world in the form of required texts, together with the structures and features of these required text types. Students develop their knowledge of how texts work as they interpret, write and produce texts. In this sense, the essential knowledge described in the section on text types, structures and features develops in tandem with the processes students’ use to construct meaning(s) and what they are learning about the conventions of written and media language.

All of the existing content in the ELA program plays a key role in the development of literacy, whether or not it reappears in the program supplement. For example, given the amount of information on the conventions and development of spoken language that is already contained in the ELA program, including it in this document would have resulted in nothing more than repetition. In other words, it is anticipated that teachers will integrate the existing program content with the additional information provided in this document as they plan for teaching.

* [Khan Academy: Grammar](https://www.khanacademy.org/humanities/grammar)
* [Khan Academy: Story Telling](https://www.khanacademy.org/humanities/hass-storytelling)

## English Language Arts

## Conventions of Written and Media Language

Print and images are read and produced differently. The following chart specifies the knowledge students are expected to
develop about these conventions throughout their elementary education. However, it is understood that students are not
expected to locate or identify parts of speech in a discrete fashion, such as in an objective test. Rather, it is anticipated that
students learn these conventions in increasingly more complex contexts and in relation to more complex texts.

```
Student constructs knowledge with teacher guidance.
```
```
Student applies knowledge by the end of the school year.
```
```
Student reinvests knowledge.
```
```
Elementary
```
```
Cycle
One
```
```
Cycle
Two
```
```
Cycle
Three
```
```
A. Understanding the Conventions of Written Language 123456
```
```
The student understands and applies conventions of written language to express thoughts, ideas and information for a
specific purpose and audience, in own reading and writing:
```
1. Grammar (sentence structure and syntax)

```
a. Writes sentences in an order that supports a main idea or story
```
```
Uses linguistic structures and features to express thoughts, ideas and information for a specific purpose and
audience:
```
```
b.
```
```
i. In simple sentences by using subject, verb, modifier
```
```
In syntactic structures that carry meaning, (e.g. the structure of a question, an
apology, a request)
```
```
ii.
```
```
iii. In compound sentences by using prepositions, conjunctions, interjections
```
```
In simple and compound sentences (i.e. varies types of sentences and uses
transitional phrases)
```
```
iv.
```
2. Usage Conventions (agreement and word choice)

```
a. Uses vocabulary and/or terminology related to the type of writing
```
```
b. Uses consistent verb tenses and correct pronoun references
```
```
Selects words that convey the intended meaning and create a picture in the reader’s
mind
```
```
c.
```
```
Uses literal and figurative language in a variety of ways (e.g. imitating, creating new
words, rhyming)
```
```
d.
```
3. Mechanics (spelling, capitalization and punctuation)

```
a. Capitalization
```
```
i. Applies capitalization rules: the first word in a sentence, proper nouns
```
```
b. Punctuation
```
```
i. Applies end punctuation rules: period, question mark, exclamation point
```
```
ii. Applies rules for commas: items in a series, greetings
```
```
iii. Uses apostrophes to punctuate contractions and singular possessive
```
```
iv. Uses quotation marks to punctuate dialogue
```
```
c. Spelling
```
```
Uses conventions of writing: spacing between words and lines, consistent
left-right and up-down orientation
```
```
i.
```
```
Uses invented spelling by choosing letters on the basis of sound for unknown
or challenging words, e.g. kaj (cage) or hows (house)
```
```
ii.
```

```
iii. Indicates words that are misspelled
```
```
Applies common spelling patterns/generalizations including: word families,
regular plurals, prefixes, suffixes, irregular plurals, words ending in –y, doubling
final consonant
```
```
iv.
```
```
Uses resources to correct own spelling (e.g. environmental print, word lists,
dictionaries, peers, spell check)
```
```
v.
```
```
B. Producing and Interpreting Media Texts 123456
```
The student interprets and uses some common conventions of media language to connote meaning(s)/message(s) in a
specific context/situation:

1. Images (in photographs, drawings and illustrations):

```
Uses and interprets the visual element of color (e.g. dark reds and blacks in a picture
book to show anger or fear)
```
```
a.
```
```
Uses and interprets the visual element of perspective in illustrations or drawings (e.g.
to connote a viewpoint, as in a faded, distant image that evokes a memory)
```
```
b.
```
```
Uses and interprets camera techniques, such as camera distance (e.g. long and
medium shots, close-ups)
```
```
c.
```
2. Sound—in television, movies, some commercials (e.g. scary music indicates suspense)

```
Visuals that convey information and/or ideas, such as timelines, graphs, graphics in comic
books
```
#### 3.


## English Language Arts

## Language-Learning Processes

Knowledge about the processes used to read, interpret, write and produce written and media texts is central to the
development of critical literacy. The processes detailed in the following chart are: response process, writing process and
production process (media texts). It is anticipated that these different processes develop throughout elementary school as
students work with increasingly complex purposes, texts and audiences. Any process is by definition nonlinear in its
development, as well as context- and text-dependent in its application, making it vital that students’ work consistently with
these language-learning processes throughout each cycle of elementary school.

```
Student constructs knowledge with teacher guidance.
```
```
Student applies knowledge by the end of the school year.
```
```
Student reinvests knowledge.
```
```
Elementary
```
```
Cycle
One
```
```
Cycle
Two
```
```
Cycle
Three
```
```
A. Response Process 123456
```
```
In a given context or situation, the student understands how to apply the stages of the reading process to read and
interpret a text:
```
1. Prereading/Viewing

```
Understands the purpose for reading, listening to and/or viewing (e.g. for enjoyment,
to learn something, to escape to new places, for instructions).
```
```
a.
```
```
Uses prior knowledge (e.g. what s/he already knows about the topic, author,
genre/text type)
```
```
b.
```
```
Previews the text (e.g. attends to the cover, dedication, title page and author’s notes
for clues that will add to understanding or enjoyment of the text)
```
```
c.
```
```
Uses knowledge of the genre/text type to be viewed/read: immersion into models of
the text type to determine important structures and features of the text type, and how
these contribute to meaning in the text (e.g. understands the structure and features
of familiar text types such as main character, sequence of events in narratives
[stories]; visual features in information-based texts)
```
```
d.
```
```
Builds needed background knowledge and experiences (e.g. of content, setting
and/or author, in a variety of ways such as watching a documentary on a related
topic, reading a picture book on a similar theme before reading a chapter book, using
the Internet)
```
```
e.
```
2. During Reading/ Viewing

```
Makes explicit connections between own personal experiences and story
experiences
```
```
a.
```
```
b. Applies knowledge of cueing systems to construct meaning
```
```
c. Uses a variety of reading strategies to make meaning of different text types
```
```
Relies on common structures and features of literary, popular and information-based
texts to construct meaning (e.g. narrative structure: beginning, middle, end; or a
feature such as dialogue)
```
```
d.
```
```
e. Relies on common structures and features of media texts to construct meaning
```
```
f. Recognizes the most common rhetorical conventions of information-based texts to build meaning, namely:
```
```
i. Description of ideas and concepts
```
```
ii. Sequence/chronology
```
```
iii. Compare/contrast
```
```
iv. Problem-solution
```
```
v. Cause- effect
```
```
Uses the purpose for reading and clues in the text to determine important aspects of
a text (e.g. nonfiction features that signal importance such as boldface print, italics)
```
```
g.
```

3. After Reading/Viewing: Interpreting the Text

```
a. Constructs a personal response to the text (i.e. constructs meaning)
```
```
Uses details and evidence in the text to infer
meaning(s)
```
```
b.
```
```
c. Integrates new information with what is already known to construct meaning
```
```
Uses evidence to distinguish between own thinking, values and beliefs and those
presented in the text (e.g. figures out what values are important to a character)
```
```
d.
```
```
Uses other readers’ interpretations to clarify and extend own ideas (e.g. discusses
information, ideas and new insights with peers)
```
```
e.
```
```
Understands that all spoken, written and media texts are constructed by people to
appeal to a specific or target audience
```
```
f.
```
```
Understands that texts must be questioned, since they are constructed by people with specific purposes in
mind:
```
```
g.
```
```
Determines the specific or target audience by selecting details from the text
(e.g. a magazine ad all in pink is probably targeting girls)
```
```
i.
```
```
Identifies and locates information about who wrote the text (i.e. its
writer/producer) and why (i.e. the purpose)
```
```
ii.
```
```
iii. Examines how the message attracts and holds the reader’s/viewer’s attention
```
```
iv. Distinguishes fact from opinion, and real from imaginary
```
```
v. Considers who/what has been left out of the text and why this might be
```
```
Identifies some of the ways that the author/producer has tried to influence the
reader/audience
```
```
vi.
```
```
B. Writing Process^123456
```
In a given context or situation, the student understands how to apply stages of the writing process to write a text:

1. Prewriting:

```
Understands the purpose for the writing (e.g., to entertain, to inform, to
communicate)
```
```
a.
```
```
b. Selects topic and text type based on purpose and audience
```
```
Examines models of text type through immersion into the text (e.g. features of text,
strategies author used to craft the text)
```
```
c.
```
2. Drafting, i.e. initial version(s) of texts

```
a. Writes to a specific familiar audience of family, friends and teacher
```
```
Writes to a specific wider audience on self-selected and assigned topics, issues and
concerns
```
```
b.
```
```
c. Uses a structure that fits the type of writing (e.g. letter format, narrative)
```
```
Adjusts writing decisions to purpose and audience (e.g. the register and syntax of a
postcard, flyer and letter are different)
```
```
d.
```
```
Connects needs and expectations of a specific audience to writing decisions (e.g.
provides additional details or information, sequences events or information to
enhance reader’s comprehension)
```
```
e.
```
3. Revision, i.e. making changes to content of text and/or message and/or meaning

```
a. Rereads for clarity
```
```
b. Adds descriptive words and sufficient details
```
```
c. Sequences information, events
```
```
d. Deletes unnecessary details and/or information
```
4. Editing, i.e. rearranging/re-ordering what has already been written and proofreading

```
a. Checks for spelling, punctuation and capitalization
```

```
b. Checks for conventions of grammar
```
```
c. Rearranges sentences and paragraphs for clarity and effect
```
5. Publishing

```
a. Selects personally significant pieces of writing to publish
```
```
Selects layout and highlights relevant structures and features to enhance the
presentation
```
```
b.
```
```
c. Feedback: seeks and provides throughout all stages of the writing process
```
```
C. Production Process (Media) 123456
```
In a given context or situation, the student, working within a team, applies all stages of the media production process to
produce a text:

1. Preproduction

```
Understands the purpose for the production (e.g. to sell something, to influence the
way people think, to give information, to entertain)
```
```
a.
```
```
b. Selects text type depending on purpose, audience and context
```
```
Examines models of text type to be produced through immersion into the type (e.g.
unique features of a text, target audience, how message/meaning is communicated)
```
```
c.
```
```
d. Drafts storyboard (i.e. a plan or representation of the project)
```
```
e. Identifies and gathers material, resources, expertise for the production
```
```
Determines criteria for production of a familiar text type (e.g. features of an effective
poster or PSA, target audience)
```
```
f.
```
2. Production

```
Uses prior knowledge of media text type from experiences with similar texts,
immersion into text
```
```
a.
```
```
b. Uses images and/or print and/or sound to produce a familiar media text
```
```
c. Uses storyboard and/or other planning resources to guide production of the text
```
```
Uses appropriate technology resources for the specific production as needed (e.g.
downloading digital images for a multimedia picture book, using a still or video
camera, adding visual effects and/or animation to a comic strip)
```
```
d.
```
3. Postproduction

```
Reviews images, records narration, adds titles or text, adds transitions, depending
on the production and its message/meaning
```
```
a.
```
```
b. Edits, depending on technology resources
```
```
c. Considers feedback from peers and others
```
```
d. Presents text to intended audience
```
```
e. Evaluates the effectiveness of the text given audience and purpose
```

## English Language Arts

## Text Types, Structures and Features

```
The Elementary English Language Arts (ELA) program focuses on the broad categories of self-expressive, narrative,
literary, popular and information-based text types. The distinction between literary and narrative is made to accommodate
students’ own writing and media production, both of which fall into the latter category.
```
The following charts indicate the knowledge about required self-expressive, narrative-literary and information-based text
types that students are expected to develop by the last year of elementary school. This knowledge includes required texts
in each broad category, such as illustrated picture books in the narrative-literary category, as well as an understanding of
the structures and features found in specific text types. Students use this knowledge to construct meaning while listening
to, reading, writing, viewing and/or producing texts. The expectation here is that students learn about different texts by
examining their specific structures and features, rather than being asked to identify or define terms in an isolated fashion.

```
In each of the three charts that follow, required texts are listed (e.g. A-1) followed by their structures and features, (e.g.
A-2). It should be noted that the popular text types referred to in the ELA program have been included in the
self-expressive, narrative-literary and information-based text type categories. As well, the required texts have been
categorized according to their most common social purpose or function. For example, although the journal appears in
self-expressive text types, journals may also include narrative and information. However, the main social purpose of a
journal is to record one’s memories, experiences, hopes, ideas and reflections, rendering it a perfect example of a
self-expressive text type.
```
```
Self-Expressive Text Types
Narrative and Literary Text Types
Information-Based Text Types
```

## English Language Arts

## Text Types, Structures and Features

### Self-Expressive Text Types

Self-expressive texts allow us to participate in the life of family, friends and community. The complexity of a self-expressive
text is achieved through the way its structures and features interact to create meaning(s). For this reason, the progression
that students demonstrate in working with knowledge about how self-expressive texts are constructed is directly related to
the increasing sophistication of concepts, themes and social knowledge in the texts that they interpret, write and produce.

In the _Elementary English Language Arts_ program, self-expressive texts fall into the following categories:

```
Texts that reinforce and maintain relationships with others
Reflective texts that help us to reflect on, think and wonder about life, current events and personal experiences, as
well as to reflect on our actions and evaluate what we learn.
```
```
Student constructs knowledge with teacher guidance.
```
```
Student applies knowledge by the end of the school year.
```
```
Student reinvests knowledge.
```
```
Elementary
```
```
Cycle
One
```
```
Cycle
Two
```
```
Cycle
Three
```
```
A. Texts That Reinforce or Maintain Relationships With Others^123456
```
1. Required Text Types

```
a. Speaking
```
```
i. Formal and informal thank-yous, expressions of appreciation and support
```
```
ii. Formal and informal introduction (e.g. of a guest speaker)
```
```
b. Reading, Listening, Writing & Media Production
```
```
i. Thank-you notes
```
```
ii. Invitations
```
```
iii. Greeting cards
```
```
iv. Friendly letters
```
```
Poetry of different kinds written by and for children (e.g. free verse, list poems,
rap, shape poems, free verse)
```
```
v.
```
```
Structures and Features
```
```
The student understands the purpose of the following structures and features and uses this knowledge when
reading, writing and producing own or others’ texts.
```
#### 2.

```
a. Reading, Writing and Media Production
```
```
i. Salutation, body and closing in a friendly letter
```
```
ii. Relevant details such as time, place and location in an invitation
```
```
Suitable message, given the communication context, in thank-you notes and
greeting cards
```
```
iii.
```
```
Images (photo or drawing) to respond to the reader’s expectations and/or
needs (e.g. the illustration on a thank-you note or invitation)
```
```
iv.
```
```
Self expressive language to relate ideas, feelings, experiences (e.g. in own
poetry)
```
```
v.
```
```
Self-expressive language in poetry: line breaks or stanzas, images, figurative
language to create vivid pictures
```
```
vi.
```
```
B. Reflective Texts 123456
```

1. Required Text Types

```
a. Speaking
```
```
Texts that focus on reflecting and evaluating own learning, in reading/ writing/
production conferences, including sharing of Integrated Portfolio
```
```
i.
```
```
b. Writing and Media Production
```
```
i. Journals
```
```
ii. Multimedia journals
```
```
Structures and Features
The student understands the purpose of the following structures and features and uses this knowledge when
reading and writing/producing texts.
```
#### 2.

```
a. Writing and Media Production
```
```
i. Self-expressive language to relate ideas, feelings, experience
```
```
Word choice to indicate a specific time frame (e.g. past tense to indicate a
memory)
```
```
ii.
```
```
iii. Synthesis of ideas and feelings to focus on what is most important
```

## English Language Arts

## Text Types, Structures and Features

### Narrative and Literacy Text Types

Narrative texts are one of the oldest forms for recording and making sense of human experience, as well as articulating the
world of the imagination.

The complexity of a narrative or literary text is achieved through the way its structures and features interact to create
meaning(s), concepts, the passage of time and characters. For this reason, the progression that students demonstrate in
working with knowledge about how narrative-literary texts are constructed is directly related to the increasing sophistication
of concepts, themes and social knowledge in the texts that they listen to, interpret, write and produce.

```
Student constructs knowledge with teacher guidance.
```
```
Student applies knowledge by the end of the school year.
```
```
Student reinvests knowledge.
```
```
Elementary
```
```
Cycle
One
```
```
Cycle
Two
```
```
Cycle
Three
```
```
A. Narrative and Literary Texts 123456
```
1. Required Text Types

```
Speaking
The student produces own stories, as well as dramatizations of others’ stories, through:
```
```
a.
```
```
Role-play involving character from own stories, from literature and from
nonfiction
```
```
i.
```
```
ii. Storytelling
```
```
b. Reading and Listening (written and media texts)
```
```
i. Children’s literature
```
```
Nursery rhymes
```
```
Illustrated wordless and picture books written for younger children
```
```
Illustrated wordless and picture books written for older children with more
sophisticated concepts, language, issues, characterization, etc.^
```
```
Poetry
```
```
Classic and modern fairy tales
```
```
Early/beginning chapter books
```
```
ii. Young adult literature
```
```
Age appropriate popular, contemporary fiction (e.g. novels, poetry,
graphic novels)^
```
```
iii. Media texts
```
```
Comic strips
```
```
Stories in children’s magazines, illustrated picture books, online talking
books (i.e. that combine spoken word and print)^
```
```
Age appropriate films, video clips, animation
```
```
iv. Nonfiction (written and media)
```
```
Biographical picture books of increasing sophistication
```
```
Articles in children’s magazines or online Web page (e.g. about a sport
star, civil rights activist)
```
```
Memoir in a variety of text types
```

```
c. Writing and Media Production
```
```
i. Stories based on ideas, experiences and events
```
```
ii. Illustrated narrative in comic strip using own drawings, images or photos
```
```
iii. Illustrated picture books using drawings and/or images and/or photos
```
```
Photo stories (e.g. sequencing photos and/or images to create a scene from a
story)
```
```
iv.
```
```
Structures and Features
The student understands the purpose of the following structures and features and uses this knowledge to construct
meaning when reading, listening to and producing spoken and written texts.
```
#### 2.

```
a. Spoken and Written Texts
```
```
i. Plot structures and features
```
```
Predictable story patterns
```
```
Sequence of events
```
```
Incidents (e.g. actions that take place in the story usually related to the
main conflict)^
Foreshadowing, i.e. the use of hints or clues to suggest what will happen
later in the story (Reading only)^
```
```
Flashback (Reading only)
```
```
Episodes, e.g. typically the subject of a chapter (Reading only)
```
```
Conflict, i.e. central problem around which a story is typically organized.
Examples would include man against man, man against nature, issues
involving what is right or wrong, etc.
```
```
Resolution of conflict
```
```
Theme, i.e. the central or underlying meaning or dominant idea(s) that
structures a narrative. It should be noted, however, that theme is not a
textual structure that every reader interprets in exactly the same way.
```
```
ii. Characterization
```
```
Main character in a story
```
```
Stock and/or flat characters, i.e. characters with only one or two qualities
or traits. Stereotypes, such as the mean stepmother, are examples of flat
characters.
Archetypes, e.g. the hero/heroine archetype, the villain, forces of good
and evil such as superheroes (Reading only)
```
```
iii. Setting
```
```
The physical landscape and social context in which the action of story
occurs, i.e. its time and place^
Descriptive details that construct the world of the story (e.g. the forest in
Max’s room allows the reader to move into the story)^
```
```
iv. Other features of narrative
```
```
Literary conventions (e.g. "Once upon a time" in a fairy tale, moral in a
fable)
```
```
Humor, suspense, repetition
```
```
Dialogue, e.g. to reveal character
```
```
Point of view, i.e. narrative voice in first or third person
```
```
Attitude of author to the material, i.e. writer’s position (Reading only)
```

```
Media texts
All of the structures and features of written narrative (above) also apply to narratives in the media. In addition,
the student understands the purpose of the following structures and features and uses this knowledge to
construct meaning when viewing and producing media texts.
```
b.

```
i. Plot structure and features
```
```
Use of images (photos or drawings) to extend the story and to provide
story details
Use of music and/or sound to create suspense, mood, humor, conflict,
etc. (Viewing only)
```
```
Use of colour to suggest emotion, to create mood, etc.
```
```
Use of different scenes or episodes to move the story forward (Viewing
only)^
```
```
ii. Characterization
```
```
Surface appearance of a character (e.g. clothing, physical attributes)
```
```
Use of details to convey an imaginary character (e.g. wings, exaggerated
or invented facial features)^
Use of explanation marks and speech bubbles to show thought and
dialogue, e.g. in comic books or some animation films^
Use of body language and gesture to convey character traits, including
emotions (Viewing only)^
Use of music and/or sound to signal or stress some aspect of character,
e.g. music to signal the reappearance of a character such as Tinkerbell
or Captain Hook in Peter Pan (Viewing only)
Use of camera angle (e.g. use of low angle to make someone look
stronger or like a bully, high angle to make someone look weaker or
vulnerable)
Stereotypes of individuals and groups (e.g. perceptions about gender in
comics and picture books) (Viewing only)^
```
```
iii. Setting
```
```
Repetition of symbols, or motifs, to create mood, suspense, sense of
continuity (e.g. scenes of the ocean in a story that takes place in Cape
Breton) (Viewing only)
Use of light and dark (e.g. to create a sense of foreboding, to change
time frame) (Viewing only)
```
```
Clothing and other details that create a sense of time and location
```

## English Language Arts

## Text Types, Structures and Features

### Information-Based Text Types

```
The vast majority of reading and writing done outside of school is information-based. These are the texts that we read in
order to research, learn and gain information on a range of events, issues and topics.
```
The complexity of an information-based text is achieved through the way its structures and features interact to create
meaning(s)/message(s). For this reason, the progression that students demonstrate in working with knowledge about how
information-based texts are constructed is directly related to the increasing sophistication of concepts, themes and social
knowledge in the texts that they interpret, write and produce.

In the _Elementary English Language Arts_ program, information-based texts fall into the following categories:

```
Planning texts are used to plan and organize our thoughts, ideas and actions, and to help us monitor our own
learning
Explanatory texts explain natural or social phenomena or how something works. They answer the questions “why” or
“how.”
Descriptive reports describe the way things are or were, usually focusing on events, information or both.
Persuasive texts try to move people to act or behave in a certain way, including selling or promoting a product.
```
```
Student constructs knowledge with teacher guidance.
```
```
Student applies knowledge by the end of the school year.
```
```
Student reinvests knowledge.
```
```
Elementary
```
```
Cycle
One
```
```
Cycle
Two
```
```
Cycle
Three
```
```
A. Planning Texts^123456
```
1. Required Text Types

```
a. Speaking and Listening
```
```
Understands the use of talk to compose a first draft of thoughts, ideas, and
information
```
```
i.
```
```
Understands the use of talk to clarify the steps in a procedure or an
organizational plan (e.g. brainstorming, pulling ideas together, asking pertinent
questions)
```
```
ii.
```
```
Writing and Media Production
The student writes/produces different planning texts, specifically:
```
```
b.
```
```
i. Learning/thinking logs and lists that record thoughts, ideas and information
```
```
Webbing and mapping texts, such as story mapping, to represent the
relationship(s) between ideas or separate pieces of information
```
```
ii.
```
```
Graphic organizers, outlines, timelines, graphs and diagrams to organize ideas
and information coherently
```
```
iii.
```
```
Structures and Features
The student understands the purpose of the following structures and features and uses this knowledge to construct
meaning/message when writing and producing texts.
```
#### 2.

```
a. Written and Media Texts
```
```
i. Events/information in sequence (e.g. timelines, graphs)
```
```
ii. Chronology or ordering of actions to undertake, (e.g. in notes or lists)
```
```
Visuals such as graphic organizers used to articulate relationship among
actions or ideas, (e.g. webbing)
```
```
iii.
```
```
iv. Hierarchy of ideas/ information, (e.g. in an rough outline, diagram)
```
```
Visuals/images for organizing or planning a text (e.g. arrows in a concept map
to indicate linked ideas)
```
```
v.
```
```
B. Explanatory Texts^123456
```

1. Required Text Types

```
a. Speaking
```
```
i. Directions and instructions (e.g. for a game)
```
```
ii. Explanation of a procedure or how something works
```
```
iii. Explanation of reasons for a decision
```
```
b. Reading, Listening, Writing and Media Production
```
```
i. Texts that explain a process/procedure (e.g. rules, recipes, directions)
```
```
ii. Illustrated and multimedia how-to books
```
```
Texts that explain how or why something happens using a narrative structure
(e.g. picture books such as The Magic School Bus , magazine articles)
```
```
iii.
```
```
iv. Posters that explain
```
```
v. Web sites (Reading and Viewing only)
```
```
Structures and Features
The student understands the purpose of the following structures and features and uses this knowledge when
speaking, reading, writing and producing texts.
```
#### 2.

```
a. Spoken, Written and Media texts
```
```
A title and a series of logical steps, in images and/or print, explaining how or
why something occurs
```
```
i.
```
```
Headings, captions or labels to focus readers’ attention on what is most
important
```
```
ii.
```
```
Sequential/chronological organization of information to explain how and/or why
something is done in the way it is
```
```
iii.
```
```
Images (photo or drawing) and text features that provide additional information
or contribute to the organization of information
```
```
iv.
```
```
Navigational aids such as table of contents or alphabetical listing in longer
texts, index, headings and page numbers
```
```
v.
```
```
vi. Bibliography (if needed)
```
**C. Descriptive Reports**^123456

1. Required Text Types

```
Speaking
The student produces:
```
```
a.
```
```
spoken reports based on family, community or school experiences (e.g. field
trips, favourite toys, special holidays)
```
```
i.
```
```
spoken reports that describe the way things are or were (e.g. an observed
event such as a solar eclipse, the stages of an experiment and what was
observed, a report on a topic or issue of personal interest, such as the
extinction of dinosaurs)
```
```
ii.
```
```
b. Reading, Viewing and Listening (spoken, written and media texts)
```
```
Nonfiction that describes and reports details about a topic (e.g. simple science
trade books written for children)
```
```
i.
```
```
ii. Articles in children’s magazines that report (e.g. on a topic, event)
```
```
Letters that describe or report (e.g. a character’s letter that describes an
experience or event)
```
```
iii.
```
```
Local and national newspaper articles that are appropriate and accessible to
children
```
```
iv.
```
```
Local and national radio and television news reports that are appropriate and
accessible to children
```
```
v.
```
```
Web pages, blogs and Internet sites appropriate and accessible to children
(e.g., short video clips)
```
```
vi.
```
```
c. Writing and Media Production
```

```
Reports on personal experiences in family, classroom, or school (e.g. an
anecdotal report)
```
```
i.
```
```
Reports on topics/subjects of personal interest (e.g. short nonfiction texts,
magazine articles)
```
```
ii.
```
```
Reports about their classroom or local community (e.g. in a class or school
newspaper, on a class Web site or in a class blog)
```
```
iii.
```
```
Structures and Features
The student understands the purpose of the following structures and features and uses this knowledge when
reading, writing and producing descriptive reports.
```
#### 2.

```
a. Written texts
```
```
i. Title to indicate contents
```
```
General statement about the topic in opening paragraph (e.g. Dogs are
mammals)
```
```
ii.
```
```
Logical sequencing of details, facts, opinions based on events and/or
information
```
```
iii.
```
```
iv. Subheadings and paragraphs to group and/or categorize information
```
```
v. Use of comparison and contrast
```
```
Visuals and/or graphic organizers to extend content of written text (e.g.
pictures, labels, diagrams)
```
```
vi.
```
```
Summary of events, observations, impressions to highlight what is most
important
```
```
vii.
```
```
Specific features of different formats depending on topics and purposes (i.e. a
science report has different features than a news article describing the school
community)
```
```
viii.
```
```
b. Media Texts
```
```
i. Headline or title to indicate contents
```
```
Images/ visuals to contribute to description of events, details, or impressions
(e.g. in a nonfiction article on plant-eating dinosaurs, a brochure describing the
school community, a blog about puppy mills)
```
```
ii.
```
```
Images/ visuals to classify and sequence details, recounts, events and
information (e.g. in a photo essay, in a local news story, on a website)
```
```
iii.
```
**D. Persuasive Texts**^123456

1. Required Text Types

```
Speaking
The student produces spoken persuasive texts, specifically:
```
```
a.
```
```
Texts that seek to persuade peers to change their habits and/or actions and/or
behaviours (e.g. appeals concerning the impact of plastic bottles on the
environment)
```
```
i.
```
```
Reading and Listening (written and media texts)
The student reads/views persuasive texts that encourage people to purchase something, partake in a special
activity or adopt a particular viewpoint, specifically:
```
```
b.
```
```
i. Popular signs and symbols, such as logos of popular food chains, clothing
```
```
Packaging for popular products aimed at children (e.g. cereal boxes, toys,
clothing)
```
```
ii.
```
```
iii. Promotional posters or flyers
```
```
iv. TV commercials aimed at children
```
```
v. Magazine and newspaper advertisements (i.e. in children’s magazines)
```
```
TV and/or movie reviews aimed at children (e.g. a movie trailer for a popular
film or a TV show)
```
```
vi.
```
```
vii. Public Service Ads and posters created for children
```
```
Opinion pieces, print or online, written for children on topics/issues of interest
(e.g. Should sports celebrities be considered heroes?)
```
```
viii.
```

```
Writing and Media Production
The student writes/produces persuasive texts that promote a product, event or service aimed at children,
specifically:
```
```
c.
```
```
Promotional posters or flyers (e.g. for a special event at school or to advertise a
favorite game)
```
```
i.
```
```
ii. Packaging (e.g. for a new toy, cereal box, video game)
```
```
iii. Ads (e.g. for a children’s magazine, for a commercial)
```
```
iv. TV and/or movie reviews for peers or younger children
```
```
Structures and Features
The student understands the purpose of the following structures and features and uses this knowledge to construct
meaning/message when reading, viewing, writing and producing texts.
```
#### 2.

```
a. Written and Media Texts
```
```
Use of persuasive images, words or phrases to promote a product and/or some
aspect of consumerism (e.g. on product packaging, in magazine ads, on a Web
site for a popular toy such as Barbie, in a popular logo)
```
```
i.
```
```
Strategic placement of images (photo or drawing) to attract the attention of
reader/viewer (e.g. as in a poster, in a magazine ad)
```
```
ii.
```
```
Music/sound to promote a product and/or appeal to the viewer to take action
(e.g. in a TV commercial) (Viewing only)
```
```
iii.
```

