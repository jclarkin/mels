# Progression of Learning

# Dance

## August 24, 2009


## Table of Contents

### Introduction 3

### Know ledge 4

### Applications of Know ledge 8

### Competency 1 – To invent dances 8

### Competency 2 – To interpret dances 10

### Competency 3 – To appreciate choreographic works, personal productions and those

### of classmates 12


## Dance

## Introduction

In order to invent or interpret dances and to appreciate choreographic works, students must acquire a certain amount of
knowledge related to the language of dance, movement technique, composition procedures and structures. Presented
schematically in the program as essential knowledges, this learning is addressed here in order to facilitate teachers’
planning. It is presented in four tables. The first table covers knowledge that students should have acquired by the end of
each cycle. The other three tables illustrate, by means of observable actions, how this knowledge is mobilized in the
exercise of each of the three competencies developed in the program. Related to the key features of the competencies, the
action verbs used in each statement show the progression of learning from one cycle to the next. Teachers will be better
equipped to ensure students’ competency development if they include in their planning simple or complex tasks aimed at
the acquisition and application of different items of knowledge in a given context.

Since competency development and acquisition of the knowledge underlying the competency are closely related, the
particulars contained in this document should enable teachers to help students acquire the tools they need to develop
each of the program’s competencies and to discover what their bodies are capable of by participating in a variety of
psychomotor, affective, cognitive, social and aesthetic experiences.

Throughout elementary school, students in the Dance program develop their creative potential and become familiar with
the creative process by using various elements of knowledge to invent their own sequences of movements. They also use
this knowledge to interpret dances, adding to their cultural experience. Lastly, they learn to express themselves using the
appropriate subject-specific vocabulary and acquire the skills they need to exercise critical judgment when appreciating a
choreographic work.

The elementary-level Arts Education programs were designed to ensure the progression of learning in each subject area

from the first to the sixth years. However, since continuity is required only for one of the two arts subjects,^1 the second
subject may not be offered continuously throughout elementary school. In such a case, it is important to provide students
with as complete an arts education as possible, taking their abilities into account. For example, if the dance course is
offered in one cycle only, teachers should make an effort to help students acquire not only the knowledge associated with
that cycle, but any other knowledge deemed essential. This knowledge appears as statements in bold type.

```
In short, by progressively acquiring the knowledge outlined in this document, students will develop the competencies
presented in the Dance program. The tables will allow teachers to provide students with the conditions necessary for
competency development at the elementary level.
```
1. The _Basic school regulation for preschool, elementary and secondary education_ stipulates that two of the four arts
    subjects (Drama, Visual Arts, Dance and Music) are compulsory at the elementary level. According to these obligations,
    one of the two subjects taught in Cycles Two and Three must be the same as one of those taught in Cycle One.


## Dance

## Knowledge

In their planning, teachers should include a variety of simple tasks (diverse exploration and experimentation activities) to
help students acquire and apply the knowledge in this table.

```
Student constructs knowledge with teacher guidance.
```
```
Student applies knowledge by the end of the school year.
```
```
Student reinvests knowledge.
```
```
The statements in bold type indicate knowledge that should be given priority if the program is not taught in all
cycles.
```
```
Elementary
```
```
Cycle
One
```
```
Cycle
Two
```
```
Cycle
Three
```
#### A. Language of dance 123456

1. **Body**^1

```
1.1. Locomotory and nonlocomotory movements
```
```
a. Names the locomotory and nonlocomotory movements
```
```
b. Distinguishes between locomotory and nonlocomotory movements
```
```
c. Describes the different locomotory and nonlocomotory movements
```
```
1.2. Partial-body movements
```
```
a. Names some everyday gestures (e.g. brushing his/her teeth)
```
```
b. Identifies some symbolic gestures (e.g. blowing a kiss)
```
```
c. Distinguishes between everyday and symbolic gestures
```
```
d. Names the different shapes
```
```
e. Identifies the different shapes
```
```
f. Distinguishes among the different shapes
```
2. **Time**

```
2.1. Metrical division
```
```
a. Identifies beat and stops
```
```
b. Identifies a medium tempo
```
```
c. Distinguishes a medium tempo from slow and fast tempi
```
```
d. Identifies a binary structure
```
```
e. Identifies a ternary structure
```
```
f. Distinguishes between a binary structure and a ternary structure
```
```
g. Identifies a simple rhythmic motif
```
```
h. Distinguishes between simple rhythmic motifs and other motifs
```
3. **Space**


```
3.1. Personal space
```
```
a. Distinguishes between high level and low level
```
```
b. Distinguishes a medium level from high and low levels
```
```
c. Identifies large and small spans in movements
```
```
d. Distinguishes between small spans and large spans in movements
```
```
e. Distinguishes between circular and curved trajectories
```
```
3.2. General space
```
```
a. Identifies forward and backward movements
```
```
b. Identifies movements to the right and to the left
```
```
c. Identifies straight and circular trajectories
```
```
d. Identifies curved trajectories
```
4. **Energy**

```
4.1. Movement performed
```
```
a. Identifies sudden effort
```
```
b. Identifies sustained effort, little effort and much effort
```
```
c. Identifies an acceleration in the movement
```
5. **Relation with partner**

```
5.1. Position
```
```
a. Names face-to-face and near/far relations
```
```
b. Identifies face-to-face and one-behind-the-other relations
```
```
c. Identifies above/below and side-by-side relations
```
```
5.2. Spatial actions
```
```
a. Distinguishes between coming together and staying together
```
```
Distinguishes the moving apart from the other spatial actions, such as coming
together and staying together
```
```
b.
```
```
Distinguishes meeting from the other spatial actions, such as coming together,
moving apart and staying together
```
```
c.
```
```
5.3. Coordination
```
```
a. Identifies a unison movement
```
```
b. Identifies an alternating movement
```
```
c. Distinguishes between a unison movement and an alternating movement
```
```
5.4. Groups
```
```
a. Identifies a circle formation
```

```
b. Identifies a circle formation and a queue formation
```
```
c. Identifies a circle formation, a queue formation and a line formation
```
```
5.5. Role-playing
```
```
a. Identifies the following roles: following a partner/partners and doing the same
```
```
Identifies the following roles: following a partner/partners, doing the same and doing
the opposite
```
```
b.
```
```
Distinguishes among the following roles: leading a partner/partners, doing the
opposite and action/reaction
```
```
c.
```
#### B. Movement technique 123456

1. **Abdominal breathing**

```
a. Names the principle of abdominal breathing in a relaxed position on the floor
```
```
b. Names the principle of abdominal breathing in sitting and then in standing positions
```
```
Differentiates between the principle of abdominal breathing in a relaxed position on
the floor and the principle of abdominal breathing in sitting and then in standing
positions
```
```
c.
```
2. **Body alignment**

```
a. Names a principle of the extension of the spinal column
```
```
b. Names a principle of knee/foot alignment
```
```
c. Names a principle of the curling and uncurling of the spinal column
```
3. **Lateral awareness**

```
a. Identifies the independent use of both sides of the body
```
```
b. Identifies the independent use of the right and left sides of the body
```
```
c. Identifies the alternating use of the right and left sides of the body
```
4. **Muscle tone**

```
a. Identifies muscle release
```
```
b. Identifies muscle contraction
```
5. **Mobility and functions of parts of the body**

```
a. Distinguishes between flexion and extension of the body
```
```
b. Identifies the part of the body that begins a movement
```
```
c. Identifies the part of the body that leads off a movement
```
6. **Weight transfer**

```
a. Identifies points of support in a balance
```
```
Names the characteristics of stable balance and the transfer of the centre of gravity
from top to bottom
```
```
b.
```
```
Distinguishes between the transfer of the centre of gravity from side to side and the
transfer of the centre of gravity from top to bottom
```
```
c.
```
7. **Focus**

```
a. Names the characteristics of directed gaze while immobile
```

```
b. Names the characteristics of directed gaze while moving on the spot
```
```
Distinguishes between the characteristics of directed gaze while immobile and those
of directed gaze while moving on the spot
```
```
c.
```
#### C. Rules for group movements 123456

```
a. Names possible responses to sound or visual cues
```
```
Names some rules for group movements, such as respecting the personal space of
others and adjusting to the movements of a partner
```
```
b.
```
```
c. Names the rules for group movements
```
#### D. Composition procedures^123456

1. **Repetition**

```
a. Identifies the repetition of a movement and of a sequence of movements
```
```
Identifies the repetition of several movements and of at least two linked sequences
of movements
```
```
b.
```
2. **Variation**

```
a. Identifies the variation of a movement in a sequence of movements
```
```
Identifies the variation of a sequence of movements in several sequences of
movements
```
```
b.
```
```
c. Identifies the variation of two sequences of movements in a dance
```
3. **Contrast**

```
Identifies contrast using one element of dance language and the expressive
quality of a movement
```
```
a.
```
```
Identifies contrast using two elements of dance language and their expressive
meanings
```
```
b.
```
#### E. Structures 123456

1. **Position**

```
a. Identifies one start position and one final position
```
2. **Sequence**

```
a. Identifies some actions or movement sequences
```
```
b. Identifies transitions between sequences of movements
```
```
c. Distinguishes between sequences of movements and the transitions between them
```
3. **Form**

```
a. Identifies personal form and round dance
```
```
b. Identifies personal form and A-B form as well as the farandole
```
```
Distinguishes among the different forms represented in a choreographic structure,
such as personal form, rondo and farandole
```
```
c.
```
#### F. Dance appreciation repertoire 123456

1. **Types of excerpts**^2^
1. The complete list of locomotory and nonlocomotory movements can be found in the _Essential Knowledges_ table in the
Dance program.
2. Since these elements are evident in action, they are included in the _Applications of Knowledge_ section.


## Dance

## Applications of Knowledge

### Competency 1 – To invent dances

```
Student constructs knowledge with teacher guidance.
```
```
Student applies knowledge by the end of the school year.
```
```
Student reinvests knowledge.
```
```
The statements in bold type indicate knowledge that should be given priority if the program is not taught in all
cycles.
```
```
Elementary
```
```
Cycle
One
```
```
Cycle
Two
```
```
Cycle
Three
```
#### A. To use personal ideas inspired by the stimulus for creation 123456

1. **Stimulus for creation**

```
Looks for ideas inspired by the stimulus for creation, using images and
his/her emotions
```
```
a.
```
```
Looks for ideas inspired by the stimulus for creation, paying attention to his/her
impressions, emotions and feelings
```
```
b.
```
```
c. Tries out a few movements
```
```
d. Looks for movements related to the stimulus for creation
```
```
e. Looks for ways of translating his/her ideas into movement
```
#### B. To use elements of dance language and elements of movement technique 123456

1. **Language of dance**

```
a. Uses locomotory and nonlocomotory movements in his/her creation
```
```
Chooses pertinent locomotory and nonlocomotory movements in his/her
creation
```
```
b.
```
```
Associates the locomotory and nonlocomotory movements with his/her creative
intention
```
```
c.
```
```
d. Uses the parts of the body in different ways
```
```
e. Looks for ways of using the parts of the body
```
```
f. Makes significant choices in the ways of using the parts of the body in a movement
```
```
Experiments with elements of metrical division such as beat, stop, medium
tempo and binary structure
```
```
g.
```
```
Chooses from among the following elements of metrical division: beat, stop, medium
tempo, simple rhythmic motif, binary structure and ternary structure
```
```
h.
```
```
i. Adapts the elements of metrical division to his/her creation
```
```
j. Uses personal and general space
```
```
Varies the use of personal and general space: levels, span, directions and
trajectories on the floor
```
```
k.
```
```
Chooses from among the different uses of personal and general space: levels, span,
directions, trajectories in the air and trajectories on the floor
```
```
l.
```
```
m. Experiments with sudden effort in movement
```
```
n. Experiments with variations in effort in movement
```
```
o. Chooses from among the variations in effort in movement
```
```
Experiments with ways of using relations with partners: position, spatial
actions, coordination, groups and role-playing
```
```
p.
```

```
q. Chooses ways of using relations with partners
```
```
r. Looks for ways of using relations with partners related to the creative intention
```
2. **Movement technique**

```
Experiments with some of the following movement techniques: abdominal
breathing, lateral awareness, mobility of parts of the body, weight transfer
and focus
```
```
a.
```
```
Uses some of the following movement techniques: abdominal breathing, body
alignment, lateral awareness, muscle tone, mobility and functions of parts of the
body, weight transfer and focus
```
```
b.
```
```
c. Integrates movement techniques into his/her creation
```
#### C. To organize the elements he/she has chosen^123456

1. **Composition procedures**

```
Experiments with one of the following composition procedures: repetition,
variation or contrast
```
```
a.
```
```
b. Uses a composition procedure in his/her creation
```
```
c. Chooses a composition procedure based on his/her creative intention
```
2. **Structures**

```
Experiments with elements of choreographic structure such as position,
sequence and form
```
```
a.
```
```
b. Uses some elements of choreographic structure
```
```
c. Adapts the elements of choreographic structure based on the creative intention
```
#### D. To finalize a production 123456

```
a. Makes adjustments to his/her sequence of movements
```
```
b. Adjusts his/her production based on the initial stimulus for creation
```
```
Enhances certain aspects of his/her production by using elements of dance
language and movement technique
```
```
c.
```
#### E. To share his/her creative experience^123456

```
Shares significant facts related to the use of elements of dance language and
movement technique
```
```
a.
```
```
Describes important aspects related to the use of elements of dance language and
movement technique
```
```
b.
```
```
c. Uses subject-specific vocabulary
```

## Dance

## Applications of Knowledge

### Competency 2 – To interpret dances

```
Student constructs knowledge with teacher guidance.
```
```
Student applies knowledge by the end of the school year.
```
```
Student reinvests knowledge.
```
```
The statements in bold type indicate knowledge that should be given priority if the program is not taught in all
cycles.
```
```
Elementary
```
```
Cycle
One
```
```
Cycle
Two
```
```
Cycle
Three
```
#### A. To become familiar with the choreographic content of the dance 123456

```
a. Locates an element of dance language
```
```
b. Locates different elements of dance language
```
```
c. Locates the elements of dance language
```
```
d. Locates the beginning and ending of the choreographic sequence
```
```
e. Locates an element of choreographic structure in a sequence
```
```
f. Locates different elements of choreographic structure in a sequence
```
```
g. Tries out a few movements
```
```
h. Attempts a few sequences of movements
```
```
i. Locates the linked sequences of movements and the transitions between them
```
#### B. To apply elements of movement technique 123456

```
a. Experiments with elements of movement technique related to the dance
```
```
b. Uses elements of movement technique related to the dance
```
```
c. Integrates elements of movement technique related to the dance
```
#### C. To bring out the expressive elements of the dance 123456

```
a. Becomes familiar with the expressive quality of a movement
```
```
b. Respects the expressive intention of the movement
```
```
c. Brings out the expressive character of the movement based on the intention
```
#### D. To apply the rules for group movements 123456

```
a. Responds to sound or visual cues
```
```
b. Adjusts his/her movements to the movements of a partner
```
```
c. Respects his/her personal space and that of others in the dance
```
```
d. Anticipates group movements in a choreographic sequence
```
#### E. To share his/her interpretation experience 123456

```
Shares significant aspects of the use of elements of dance language and
movement technique
```
```
a.
```

```
Describes important aspects of the use of elements of dance language and
movement technique
```
b.

c. **Uses subject-specific vocabulary**


## Dance

## Applications of Knowledge

### Competency 3 – To appreciate choreographic works, personal productions and those of

### classmates

```
Student constructs knowledge with teacher guidance.
```
```
Student applies knowledge by the end of the school year.
```
```
Student reinvests knowledge.
```
```
The statements in bold type indicate knowledge that should be given priority if the program is not taught in all
cycles.
```
```
Elementary
```
```
Cycle
One
```
```
Cycle
Two
```
```
Cycle
Three
```
#### A. To examine a choreographic work or excerpt for elements of content 123456

```
a. Observes subject-specific elements in classmates’ work
```
```
Observes subject-specific elements in excerpts of works past and present, from here
and elsewhere
```
```
b.
```
1. **Language of dance**

```
1.1. Locomotory and nonlocomotory movements^1
```
```
a. Observes the locomotory and nonlocomotory movements used in the production
```
```
Locates the locomotory and nonlocomotory movements used in the
production
```
```
b.
```
```
Identifies the different uses of the locomotory and nonlocomotory movements in the
production
```
```
c.
```
```
1.2. Partial-body movements
```
```
a. Observes body shapes as well as everyday gestures
```
```
b. Locates different body forms as well as symbolic and everyday gestures
```
```
1.3. Metrical division
```
```
Locates the following elements of metrical division: beat, stop, medium
tempo and binary structure
```
```
a.
```
```
Locates the following elements of metrical division: beat, stop, medium tempo,
simple rhythmic motif, binary structure and ternary structure
```
```
b.
```
```
c. Observes the use of the elements of metrical division in the context of the production
```
```
1.4. Personal and general space
```
```
a. Observes the use of space in terms of levels and directions
```
```
Locates different uses of space in terms of levels, span, directions and trajectories
on the floor
```
```
b.
```
```
c. Locates the uses of space
```
```
1.5. Energy
```
```
a. Locates sudden effort in movement
```
```
b. Locates variations in effort in movement: sudden, sustained, much and little
```
```
c. Locates acceleration
```

```
1.6. Relation with partner
```
```
Observes relations with partners in the actions associated with the following
aspects: position, spatial actions, coordination, groups and role-playing
```
```
a.
```
```
b. Locates different relations with partners
```
2. **Movement technique**

```
Observes some of the following elements of movement technique: lateral
awareness, mobility of parts of the body, weight transfer and focus
```
```
a.
```
```
Locates some of the following elements of movement technique: body alignment,
lateral awareness, muscle tone, mobility and functions of parts of the body, weight
transfer and focus
```
```
b.
```
```
c. Observes the use of elements of movement technique
```
3. **Composition procedures**

```
a. Observes composition procedures such as repetition, variation and contrast
```
```
Locates some of the following composition procedures: repetition, variation and
contrast
```
```
b.
```
```
c. Observes the use of composition procedures
```
4. **Structures**

```
a. Observes elements of structure such as position, sequence and form
```
```
b. Locates elements of structure such as position, sequence and form
```
```
c. Observes the use of elements of structure
```
#### To examine a choreographic work or excerpt for sociocultural references

#### (Cycle Two and Cycle Three)

#### B.

##### 123456

```
a. Observes an element representing sociocultural aspects
```
```
b. Locates elements representing sociocultural aspects
```
#### C. To make connections between what he/she has felt and examined^123456

```
a. Names an element of the choreographic sequence that elicited an emotion
```
```
b. Explains why a particular element elicited an emotion
```
```
Gives an example of the connections made between what he/she has felt and the
element observed
```
```
c.
```
```
d. Uses subject-specific vocabulary
```
#### D. To make a critical or aesthetic judgment^123456

```
a. States a preference based on an observation
```
```
b. Explains why certain elements caught his/her attention
```
```
c. Justifies his/her point of view based on verifications
```
```
d. Formulates a comment concerning his/her observations
```
```
e. Formulates comments using examples based on his/her observations
```
```
Compares the choreographic sequences based on his/her observations and the
proposed appreciation criteria
```
```
f.
```
```
g. Uses subject-specific vocabulary
```
#### E. To share his/her appreciation experience 123456


```
Shares significant aspects of the appreciation of the elements of dance
language and movement technique
```
```
a.
```
```
Describes important aspects of the appreciation of the elements of dance language
and movement technique
```
```
b.
```
```
c. Uses subject-specific vocabulary
```
1. The complete list of locomotory and nonlocomotory movements can be found in the _Essential Knowledges_ table in the
    Dance program.


