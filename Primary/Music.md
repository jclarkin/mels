# Progression of Learning

# Music

## August 24, 2009


## Table of Contents

### Introduction 3

### Know ledge 4

### Applications of Know ledge 7

### Competency 1 – To invent vocal or instrumental pieces 7

### Competency 2 – To interpret musical pieces 9

### Competency 3 – To appreciate musical works, personal productions and those of

### classmates 11


## Music

## Introduction

In order to invent vocal or instrumental pieces, interpret musical pieces and appreciate musical works, students must
acquire a certain amount of knowledge related to the language of music, instrumental techniques and structures.
Presented schematically in the program as essential knowledges, this learning is addressed here in order to facilitate
teachers’ planning. It is presented in four tables. The first table covers knowledge that students should have acquired by
the end of each cycle. The other three tables illustrate, by means of observable actions, how this knowledge is mobilized in
the exercise of each of the three competencies developed in the program. Related to the key features of the competencies,
the action verbs used in each statement show the progression of learning from one cycle to the next. Teachers will be
better equipped to ensure students’ competency development if they include in their planning simple or complex tasks
aimed at the acquisition and application of different items of knowledge in a given context.

```
Since competency development and acquisition of the knowledge underlying the competency are closely related, the
particulars contained in this document should enable teachers to help students acquire the tools they need to develop
each of the program’s competencies and to discover their artistic sensitivity and creative potential.
```
```
Throughout elementary school, students in the Music program become familiar with the creative process by using various
elements of knowledge to invent their own vocal or instrumental pieces. They also use this knowledge to interpret musical
pieces, adding to their cultural experience. Lastly, they learn to express themselves using the appropriate subject-specific
vocabulary and acquire the skills they need to exercise critical judgment when appreciating musical works, personal
productions and those of classmates.
```
```
The elementary-level Arts Education programs were designed to ensure the progression of learning in each subject area
from the first to the sixth years. However, since continuity is required only for one of the two arts subjects,^1 the second
subject may not be offered continuously throughout elementary school. In such a case, it is important to provide students
with as complete an arts education as possible, taking their abilities into account. For example, if the music course is
offered in one cycle only, teachers should make an effort to help students acquire not only the knowledge associated with
that cycle, but any other knowledge deemed essential. This knowledge appears as statements in bold type.
```
```
In short, by progressively acquiring the knowledge outlined in this document, students will develop the competencies
presented in the Music program. The tables will allow teachers to provide students with the conditions necessary for
competency development at the elementary level.
```
1. The _Basic school regulation for preschool, elementary and secondary education_ stipulates that two of the four arts
    subjects (Drama, Visual Arts, Dance and Music) are compulsory at the elementary level. According to these obligations,
    one of the two subjects taught in Cycles Two and Three must be the same as one of those taught in Cycle One.


## Music

## Knowledge

In their planning, teachers should include a variety of simple tasks (diverse exploration and experimentation activities) to
help students acquire and apply the knowledge in this table.

```
Student constructs knowledge with teacher guidance.
```
```
Student applies knowledge by the end of the school year.
```
```
Student reinvests knowledge.
```
```
The statements in bold type indicate knowledge that should be given priority if the program is not taught in all
cycles.
```
```
Elementary
```
```
Cycle
One
```
```
Cycle
Two
```
```
Cycle
Three
```
#### A. Language of music 123456

1. **Intensity and dynamics**

```
a. Identifies loud and soft sounds
```
```
b. Identifies dynamics: forte , piano , crescendo and decrescendo
```
```
c. Differentiates among the dynamics: forte , piano , crescendo and decrescendo
```
2. **Duration**

```
Identifies quarter notes, two eighth notes, rests and elements of the conventional
nontraditional code: long, very long, short, very short and rest
```
```
a.
```
```
b. Identifies notes and rests, including half notes
```
```
c. Differentiates among notes and rests, including triplets and whole notes
```
3. **Pitch**

```
a. Names the sounds from the diatonic scale
```
```
b. Identifies high and low registers
```
```
c. Differentiates between low , medium and high registers
```
4. **Tone colour**

```
a. Names the classroom’s percussion instruments
```
```
b. Differentiates between a child’s voice and an adult’s voice
```
```
c. Differentiates between a man’s voice and a woman’s voice
```
```
d. Identifies wind instruments
```
```
e. Distinguishes the instruments, depending on the repertoire used
```
5. **Quality of sound**

```
a. Names qualities of sound: crisp or resonant
```
```
Differentiates crisp sounds from resonant sounds and coarse sounds from smooth
sounds
```
```
b.
```
#### B. Graphic representation 123456

1. **Traditional code**

```
Lists the symbols related to dynamics (piano, forte, crescendo, decrescendo) and
duration (half note, quarter note, two eighth notes, rest)
```
```
a.
```

```
Lists the symbols related to pitch (on the staff, according to the musical works
studied)
```
```
b.
```
```
Distinguishes symbols related to dynamics, duration (including whole notes
and triplets and pitch (on the staff, according to the musical works studied)
```
```
c.
```
2. **Conventional nontraditional code**

```
Names the symbols related to duration (very short, short, long, very long,
rest), dynamics (loud, soft), pitch (high, low, ascending, descending) and the
qualities of sound (crisp, resonant)
```
```
a.
```
```
Differentiates among the symbols associated with duration, intensity, pitch (medium
register) and the qualities of sound (coarse, smooth)
```
```
b.
```
3. **Other codes**

```
a. Identifies a personal graphic representation in his/her creations
```
#### C. Sound sources 123456

1. **Voice and body**

```
a. Names sound sources: singing voice, vocal effects and body percussion
```
2. **Musical instruments**

```
a. Names some percussion instruments and a few other classroom instruments
```
```
Identifies some percussion instruments and a few other instruments
including the recorder
```
```
b.
```
3. **Sound-producing objects**

```
a. Identifies objects made from wood and metal that can be used to produce sounds
```
```
b. Lists objects made from paper and fabric that can be used to produce sounds
```
```
c. Distinguishes among different materials that can be used to produce sounds
```
4. **Information and communications technologies**

```
a. Identifies sounds produced using software, a sequencer or a synthesizer
```
#### D. Instrumental techniques^123456

1. **Voice**

```
Identifies the following elements of technique: opening of the mouth, breathing,
intonation and posture
```
```
a.
```
```
Lists the following elements of technique: pronunciation, opening of the mouth,
breathing, intonation and posture
```
```
b.
```
```
c. Distinguishes among the elements of technique, including tone
```
2. **Percussion instruments and other sound sources**

```
Identifies the following elements of technique: posture, form, means of production
and technique
```
```
a.
```
```
Differentiates among the following elements of technique: posture, form,
means of production and technique
```
```
b.
```
3. **Recorder**

```
Identifies the following elements of technique: posture, form, means of
production and technique
```
```
a.
```
```
Distinguishes the following elements of technique: posture, form, means of
production and technique
```
```
b.
```
#### E. Rules for group ensemble work 123456

```
Names directions indicating the beginning and ending of a piece and the
dynamics
```
```
a.
```
```
b. Identifies directions indicating beat
```

```
c. Distinguishes among directions indicating changes in tempo
```
```
d. Names sound or visual cues
```
#### F. Composition procedures^123456

```
Names the following composition procedures: question and answer, contrast and
reproduction of sound
```
```
a.
```
```
b. Names the composition procedures, including repetition
```
```
Distinguishes among the following composition procedures: reproduction of sound,
collage, ostinato and mirror
```
```
c.
```
#### G. Structures^123456

1. **Form**

```
a. Identifies personal form and A-B form
```
```
b. Identifies the A-B-A form
```
```
c. Identifies canon in two voices and rondo form
```
2. **Tempo**

```
a. Identifies slow and fast tempi
```
```
b. Identifies slow, moderate and fast tempi
```
```
Identifies tempi using the traditional code: lento , moderato , allegro ,
accelerando and rallentando
```
```
c.
```
3. **Rhythmic organization**

```
a. Names an element of rhythmic organization, for example beat, quarter note or rest
```
```
b. Identifies elements of rhythmic organization
```
```
c. Differentiates among the elements of rhythmic organization
```
4. **Melodic organization**

```
Identifies a musical phrase, a series of ascending sounds, a series of
descending sounds and a series of sounds repeated at a fixed pitch
```
```
a.
```
```
b. Identifies conjunct sounds, disjunct sounds and glissando
```
```
c. Differentiates among the elements of melodic organization
```
#### H. Musical appreciation repertoire 123456

1. **Types of excerpts**^1^
1. Since these elements are evident in action, they are included in the _Applications of Knowledge_ section_._


## Music

## Applications of Knowledge

### Competency 1 – To invent vocal or instrumental pieces

```
Student constructs knowledge with teacher guidance.
```
```
Student applies knowledge by the end of the school year.
```
```
Student reinvests knowledge.
```
```
The statements in bold type indicate knowledge that should be given priority if the program is not taught in all
cycles.
```
```
Elementary
```
```
Cycle
One
```
```
Cycle
Two
```
```
Cycle
Three
```
#### A. To use personal ideas inspired by the stimulus for creation 123456

```
Looks for an idea inspired by the stimulus for creation, paying attention to his/her
emotions and feelings
```
```
a.
```
```
b. Looks for ideas inspired by the stimulus for creation
```
```
c. Lists all of the ideas inspired by the stimulus for creation
```
```
d. Looks for a sound source to translate his/her idea or emotions
```
```
Tries out ideas using the voice, instruments and body and uses information
and communications technologies to translate his/her creative intention
```
```
e.
```
```
f. Combines trials and sound sources to develop his/her creation
```
```
g. Chooses an idea from among his/her experiments with sound sources
```
#### To use sound sources and elements of musical language and elements of

#### technique

#### B.

##### 123456

1. **Language of music and graphic representation**

```
Experiments with some of the following elements: intensity and dynamics, duration,
pitch, tone colour and quality of sound
```
```
a.
```
```
Uses different elements: intensity and dynamics, duration, pitch, tone colour
and quality of sound
```
```
b.
```
```
Chooses different elements: intensity and dynamics, duration, pitch, tone colour and
quality of sound
```
```
c.
```
```
Uses a personal representation and the conventional nontraditional code to keep a
record of his/her creation
```
```
d.
```
```
Uses the traditional code, a personal representation and the conventional
nontraditional code to keep a record of his/her creation
```
```
e.
```
```
Chooses the graphic representation best suited to the stimulus for creation to keep a
record of his/her creation
```
```
f.
```
2. **Sound sources and instrumental techniques**

```
Tries out ideas using the voice, the body, musical instruments and sound-producing
objects
```
```
a.
```
```
Uses some different sound sources: voice, body, musical instruments,
sound-producing objects, recorder and information and communications
technologies
```
```
b.
```
```
Selects some of the following sound sources: voice, body, musical instruments,
sound-producing objects, recorder and information and communications
technologies
```
```
c.
```
```
Experiment with voice techniques such as opening of the mouth, breathing,
intonation and posture, as well as instrumental techniques such as posture, form,
means of production and technique
```
```
d.
```
```
e. Uses voice techniques such as pronunciation and instrumental techniques
```
```
Adapts voice techniques, such as tone, and instrumental techniques to the
characteristics of his/her creation
```
```
f.
```

#### C. To organize the elements he/she has chosen^123456

1. **Composition procedures**

```
Experiments with the following composition procedures: question and answer,
contrast and reproduction of sound
```
```
a.
```
```
Uses the following composition procedures: repetition, question and
answer, contrast and reproduction of sound
```
```
b.
```
```
Uses the following composition procedures: reproduction of sound, collage, ostinato
and mirror
```
```
c.
```
2. **Structures**

```
a. Experiments with the following forms: personal and A-B
```
```
b. Uses the following forms: A-B-A, personal and A-B
```
```
c. Uses the following forms: canon in two voices, rondo, A-B-A, personal and A-B
```
```
d. Uses a slow tempo and a fast tempo
```
```
e. Uses a slow tempo, a moderate tempo and a fast tempo
```
```
Chooses an appropriate tempo for his/her creation: lento , moderato , allegro ,
accelerando or rallentando
```
```
f.
```
```
Adapts his/her creation rhythmically based on an unmeasured or definite
number of beats
```
```
g.
```
```
Structures his/her creation rhythmically based on an unmeasured or definite number
of beats
```
```
h.
```
```
Uses elements of melody: musical phrase, series of ascending or
descending sounds, series of sounds repeated at a fixed pitch
```
```
i.
```
```
j. Uses different elements of melody: conjunct sounds, disjoint sounds, glissando
```
```
k. Selects different elements of melody depending on his/her creative intention
```
```
l. Tries out sequences of musical elements
```
```
m. Tries out sequences by combining the selected elements
```
#### D. To finalize a production^123456

```
a. Makes some adjustments to his/her creation
```
```
b. Adjusts his/her production based on the stimulus for creation
```
```
c. Enhances some aspects of his/her production using musical elements
```
#### E. To share his/her creative experience^123456

```
Shares significant aspects of the use of sound sources, techniques and elements of
musical language
```
```
a.
```
```
Describes important aspects of the use of sound sources, techniques and
elements of musical language
```
```
b.
```
```
c. Uses subject-specific vocabulary
```

## Music

## Applications of Knowledge

### Competency 2 – To interpret musical pieces

```
Student constructs knowledge with teacher guidance.
```
```
Student applies knowledge by the end of the school year.
```
```
Student reinvests knowledge.
```
```
The statements in bold type indicate knowledge that should be given priority if the program is not taught in all
cycles.
```
```
Elementary
```
```
Cycle
One
```
```
Cycle
Two
```
```
Cycle
Three
```
#### A. To become familiar with the musical content of the piece 123456

**1. Language of music and graphic representation**

```
Looks for elements of musical language among the intensity and dynamics, duration,
pitch, tone colour and quality of sound^1
```
```
a.
```
```
Locates elements of musical language among the qualities of intensity,
dynamics, duration, pitch, tone colour and quality of sound
```
```
b.
```
```
c. Respects the elements of musical language depending on the repertoire used
```
```
Looks for elements of the conventional nontraditional code and elements of a
personal representation^2
```
```
d.
```
```
Locates elements of the traditional code, the conventional nontraditional
code and a personal representation
```
```
e.
```
```
Respects the elements of the graphic representation used: conventional
nontraditional, traditional and personal
```
```
f.
```
2. **Structures**

```
Looks for elements related to the structure of the piece: form, tempo, rhythmic
organization and melodic organization^3
```
```
a.
```
```
Locates elements related to the structure of the piece: form, tempo,
rhythmic organization and melodic organization
```
```
b.
```
```
c. Respects the elements related to the structure of the piece
```
3. **Sound sources**

```
Tries out ideas using the voice, the body, a musical instrument and a sound-
producing object
```
```
a.
```
```
Repeats the musical sequence using voice, body, a musical instrument, a sound-
producing object and information and communications technologies
```
```
b.
```
```
Uses the voice, body, musical instrument, sound-producing object and
information and communications technologies proposed in the piece
```
```
c.
```
```
Adapts one of the following musical elements of the piece: language of music,
graphic representation or structure
```
```
d.
```
```
e. Tries out musical ideas, taking into account musical elements
```
```
f. Combines the musical elements in the piece
```
#### B. To apply elements of technique 123456

1. **Instrumental techniques**

```
Experiments with voice techniques such as opening of the mouth, breathing,
intonation and posture
```
```
a.
```
```
Uses voice techniques related to the characteristics of the piece, including
pronunciation
```
```
b.
```
```
Integrates the voice techniques related to the characteristics of the piece,
such as tone
```
```
c.
```
```
Experiments with techniques related to the following percussion instruments and
other sound sources: posture, form, means of production and technique
```
```
d.
```

```
Uses some of the following techniques related to percussion instruments,
other sound sources and recorder: posture, form, means of production and
technique
```
```
e.
```
```
f. Integrates the instrumental techniques into the sound sources used
```
#### C. To bring out the expressive elements of the piece 123456

```
a. Adapts an expressive element of the piece
```
```
b. Repeats the piece taking some expressive elements into account
```
```
c. Integrates the expressive elements of the piece
```
#### D. To apply the rules for group ensemble work 123456

```
Takes into account directions indicating the beginning and ending of the
piece and the dynamics
```
```
a.
```
```
b. Takes into account directions indicating the beat
```
```
c. Takes into account directions indicating changes in tempo
```
```
d. Takes into account sound or visual cues
```
#### E. To share his/her interpretation experience 123456

```
Shares significant aspects of the use of sound sources, technique and elements of
musical language
```
```
a.
```
```
Describes important aspects of the use of sound sources, technique and
elements of musical language
```
```
b.
```
```
c. Uses subject-specific vocabulary
```
1. The complete list of elements of the language of music by cycle can be found in the _Essential Knowledges_ table in the
    Music program.
2. The complete list of graphic representation codes by cycle can be found in the _Essential Knowledges_ table in the Music
    program.
3. The complete list of elements of structures by cycle can be found in the _Essential Knowledges_ table in the Music
    program.


## Music

## Applications of Knowledge

### Competency 3 – To appreciate musical works, personal productions and those of classmates

```
Student constructs knowledge with teacher guidance.
```
```
Student applies knowledge by the end of the school year.
```
```
Student reinvests knowledge.
```
```
The statements in bold type indicate knowledge that should be given priority if the program is not taught in all
cycles.
```
```
Elementary
```
```
Cycle
One
```
```
Cycle
Two
```
```
Cycle
Three
```
#### A. To examine a musical work or excerpt for elements of content 123456

```
a. Pays attention to subject-specific elements while listening to classmates’ productions
```
```
Pays attention to subject-specific elements while listening to works past and present,
from here and elsewhere
```
```
b.
```
1. **Language of music and graphic representation**

```
Observes some of the following elements: intensity and dynamics, duration, pitch,
tone colour and quality of sound
```
```
a.
```
```
Locates some of the following elements: intensity and dynamics, duration,
pitch, tone colour and quality of sound
```
```
b.
```
```
Verifies the precision of the following elements used: intensity and dynamics,
duration, pitch, tone colour and quality of sound
```
```
c.
```
```
Observes elements of the conventional nontraditional code and elements of a
personal representation
```
```
d.
```
```
Locates different elements of the traditional code, the conventional
nontraditional code and a personal representation
```
```
e.
```
```
f. Verifies the precision of the elements of the graphic representation used
```
2. **Sound sources and instrumental techniques**

```
Observes some of the sound sources used: voice, body, musical instruments and
sound-producing objects
```
```
a.
```
```
Locates some of the sound sources used: voice, body, musical instruments,
sound-producing objects and information and communications technologies
```
```
b.
```
```
Verifies the use of several sound sources: voice, body, musical instruments, sound-
producing objects and information and communications technologies
```
```
c.
```
```
d. Observes a voice technique and an instrumental technique
```
```
e. Observes voice techniques and instrumental techniques
```
```
f. Locates voice techniques and instrumental techniques
```
3. **Composition procedures**

```
Observes the following composition procedures: question and answer, contrast and
reproduction of sound
```
```
a.
```
```
Locates the following composition procedures: repetition, question and
answer, contrast and reproduction of sound
```
```
b.
```
```
Verifies the precision of the composition procedures used: reproduction of sound,
collage, ostinato and mirror
```
```
c.
```
4. **Structures**

```
a. Observes the A-B and personal forms
```
```
b. Locates the A-B-A form
```
```
c. Locates a canon in two voices and rondo
```

```
d. Observes fast or slow tempo
```
```
e. Observes fast, moderate or slow tempo
```
```
Verifies one or more of the following changes in tempo: lento , moderato ,
allegro , rallentando and accelerando
```
```
f.
```
```
g. Locates the beat in a rhythmic organization
```
```
Observes elements of the melodic organization: musical phrase, series of
ascending or descending sounds and series of sounds repeated at a fixed
pitch
```
```
h.
```
```
Locates elements of the melodic organization: conjunct sounds, disjunct sounds and
glissando
```
```
i.
```
```
j. Verifies the precision of the use of elements of the melodic organization
```
#### To examine a musical work or excerpt for sociocultural references (Cycle

#### Two and Cycle Three)

#### B.

##### 123456

```
a. Brings out the musical elements indicating the artistic period of the work
```
```
Makes connections between the musical elements and the corresponding
sociocultural aspects
```
```
b.
```
```
c. Compares the main sociocultural aspects with his/her experience or surroundings
```
#### C. To make connections between what he/she has felt and examined 123456

```
a. Brings out the musical element that elicited an emotion
```
```
b. Associates the element of content with what he/she felt
```
```
c. Gives an example of the connections made between what he/she felt and examined
```
```
d. Uses subject-specific vocabulary
```
#### D. To make a critical or aesthetic judgment^123456

```
a. Shares a preference based on his/her observations
```
```
b. Discusses his/her musical observations, explaining his/her preferences
```
```
Shares his/her point of view based on his/her musical observations and the
proposed appreciation criteria
```
```
c.
```
```
d. Uses subject-specific vocabulary
```
#### E. To share his/her appreciation experience^123456

```
Shares significant facts related to the appreciation of sound sources, technique and
elements of musical language
```
```
a.
```
```
Describes important aspects of the appreciation of sound sources, technique
and elements of musical language
```
```
b.
```
```
c. Uses subject-specific vocabulary
```

